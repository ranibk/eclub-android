package com.ircaindia.eclub.view.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.ircaindia.eclub.BuildConfig;
import com.ircaindia.eclub.R;
import com.ircaindia.eclub.model.apis.RestAPICall;
import com.ircaindia.eclub.model.dto.ClubDetails;
import com.ircaindia.eclub.model.apis.PostData;
import com.ircaindia.eclub.model.dto.SendClubDetails;
import com.ircaindia.eclub.model.retrofit.response.ResponseModelArray;
import com.ircaindia.eclub.model.utils.Constants;
import com.ircaindia.eclub.model.utils.IMessages;
import com.ircaindia.eclub.model.utils.Messages;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import io.fabric.sdk.android.Fabric;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashActivity extends AppCompatActivity {

    IMessages messages;
    TextView versionTextView,titleTextView,subTextView;
    String[] appPermissions = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_PHONE_STATE
    };
    RelativeLayout mainRelativeLayout;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        try {
            Fabric.with(this, new Crashlytics());
            Objects.requireNonNull(this.getSupportActionBar()).hide();

            versionTextView = findViewById(R.id.text_viewVersion);
            titleTextView = findViewById(R.id.text_viewTitle);
            subTextView = findViewById(R.id.text_viewSub);
            mainRelativeLayout = findViewById(R.id.relative_layoutMain);
            versionTextView.setText("Version: " + BuildConfig.VERSION_NAME);


            messages = new Messages(this);
            messages.saveClubDetails(null);
            messages.saveDashBoardView(null);
            messages.saveCurrentLogin(SplashActivity.this,null);

            titleTextView.setText(messages.getCustomFontString(this.getResources().getString(R.string.app_name),"SirinStencil-Regular.ttf"));
//            subTextView.setText(messages.getCustomFontString("Manage things at your Finger Tips","SirinStencil-Regular.ttf"));

//            if (checkAndRequestPermissions()) {
                method();
//            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        try {
            if (requestCode == 1000) {
                HashMap<String, Integer> permissionResults = new HashMap<>();
                int deniedCount = 0;
                for (int i = 0; i < grantResults.length; i++) {
                    if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                        permissionResults.put(permissions[i], grantResults[i]);
                        deniedCount++;
                    }
                }
                if (deniedCount == 0) {
                    checkAndRequestPermissions();
                } else if (deniedCount == 3) {
                    method();
                }
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    @SuppressLint("HardwareIds")
    public void method() {
        try {
            if (messages.isInternetAvailable()) {
                PostData postData = new PostData();
                postData.setInterface("RestAPI");
                postData.setMethod("GetClubDetails");
                SendClubDetails sendClubDetails = new SendClubDetails();
                sendClubDetails.setClubName("");
                sendClubDetails.setAccountNo("");
                sendClubDetails.setAppId("0");
                sendClubDetails.setStatus("0");
                sendClubDetails.setAppName("eclub");
                sendClubDetails.setOs(messages.getDeviceOS());
                sendClubDetails.setImei(messages.getDeviceId(SplashActivity.this));
                sendClubDetails.setModel(messages.getDeviceModel(SplashActivity.this));
                postData.setParameters(sendClubDetails);
                callApiMethod(postData);
            } else {
                messages.toastInternet(SplashActivity.this, mainRelativeLayout);
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }

    }

    public boolean checkAndRequestPermissions() {
        try {
            List<String> listPermNeeded = new ArrayList<>();
            for (String perm : appPermissions) {
                if (ContextCompat.checkSelfPermission(this, perm) != PackageManager.PERMISSION_GRANTED) {
                    listPermNeeded.add(perm);
                }
            }
            if (!listPermNeeded.isEmpty()) {
                ActivityCompat.requestPermissions(this, listPermNeeded.toArray(new String[listPermNeeded.size()]), 1000);
                return false;
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return true;
    }


    private void callApiMethod(PostData postData) {
        try {
            RestAPICall restApi = messages.getAPIService(Constants.API_URL);
            messages.showProgressDialog(SplashActivity.this, "", "Loading...");
            restApi.getClubDetails("club.ashx",postData).enqueue(new Callback<ResponseModelArray<ClubDetails>>() {
                @Override
                public void onResponse(@NonNull Call<ResponseModelArray<ClubDetails>> call, @NonNull Response<ResponseModelArray<ClubDetails>> response) {
                    messages.hideDialog();
                    try {
                        if (response.isSuccessful()) {
                            if (response.body() != null) {
                                if (response.body().isStatus()) {
                                    List<ClubDetails> clubDetailsList = response.body().getData();
                                    if (clubDetailsList.size() > 0) {
                                        messages.saveClubDetails(clubDetailsList);
                                        Intent i = new Intent(SplashActivity.this, LoginActivity.class);
                                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        startActivity(i);
                                        finish();
                                    } else {
                                        messages.toast(SplashActivity.this, "Club details are not loaded");
                                    }
                                } else {
                                    messages.toast(SplashActivity.this, "Unable to find Club details");
                                }
                            } else {
                                messages.toast(SplashActivity.this, "Unable to find Club details");
                            }
                        } else {
                            if (response.code() == 500) {
                                messages.toast(SplashActivity.this, response.message());
                            }else   if (response.code() == 404) {
                                messages.toast(SplashActivity.this, "Internet connectivity issue could not connect to server");
                            } else {
                                assert response.errorBody() != null;
                                messages.toast(SplashActivity.this, response.errorBody().string());

                            }
                        }
                    } catch (Throwable e) {
                        e.printStackTrace();
                        messages.toast(SplashActivity.this, e.getMessage());
                    }
                }

                @Override
                public void onFailure(@NonNull Call<ResponseModelArray<ClubDetails>> call, @NonNull Throwable t) {
                    try {
                        messages.hideDialog();
                        if (Objects.requireNonNull(t.getMessage()).contains(getResources().getString(R.string.network_failed))) {
                            messages.toastInternet(SplashActivity.this, mainRelativeLayout);
                        } else {
                            messages.toast(SplashActivity.this, t.getMessage());
                        }
                        Log.d(Constants.TAG, t.getMessage());
                    } catch (Throwable e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (Throwable e) {
            e.printStackTrace();
        }

    }


}
