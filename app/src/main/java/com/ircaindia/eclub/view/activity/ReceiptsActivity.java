package com.ircaindia.eclub.view.activity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.graphics.Bitmap;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ircaindia.eclub.R;
import com.ircaindia.eclub.model.adapter.ReceiptInfoAdapter;
import com.ircaindia.eclub.model.apis.PostData;
import com.ircaindia.eclub.model.apis.RestAPICall;
import com.ircaindia.eclub.model.dto.ClubDetails;
import com.ircaindia.eclub.model.dto.Login;
import com.ircaindia.eclub.model.dto.ReceiptsInfo;
import com.ircaindia.eclub.model.dto.SendDto;
import com.ircaindia.eclub.model.retrofit.response.ResponseModelArray;
import com.ircaindia.eclub.model.sqlite.DataBaseLite;
import com.ircaindia.eclub.model.utils.Constants;
import com.ircaindia.eclub.model.utils.IMessages;
import com.ircaindia.eclub.model.utils.Messages;
import com.ircaindia.eclub.model.utils.OnRecyclerViewItemClickListener;
import com.ircaindia.eclub.model.utils.monthpicker.RackMonthPicker;
import com.ircaindia.eclub.model.utils.monthpicker.listener.DateMonthDialogListener;
import com.ircaindia.eclub.model.utils.monthpicker.listener.OnCancelMonthDialogListener;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReceiptsActivity extends AppCompatActivity implements View.OnClickListener {

    IMessages messages;
    DataBaseLite dataBaseLite;
    ClubDetails clubDetails;
    Login login;
    RackMonthPicker rackMonthPicker;

    LinearLayout monthLinearLayout, billingInfoLinearLayout, receiptLinearLayout;
    ExpandableListView billingInfoRecyclerView;
    RecyclerView receiptRecyclerView;
    TextView receiptInfoTextView, monthTextView, noBillingTextView, noReceiptTextView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receipts);
        try {
            if (activitySetup()) {
                monthLinearLayout = findViewById(R.id.linear_layoutMonth);
                monthTextView = findViewById(R.id.text_viewMonth);
                receiptInfoTextView = findViewById(R.id.text_viewReceiptInfo);

                billingInfoLinearLayout = findViewById(R.id.linear_layoutBillingInfo);
                billingInfoRecyclerView = findViewById(R.id.recycler_viewBillingInfo);
                receiptLinearLayout = findViewById(R.id.linear_layoutReceipt);
                receiptRecyclerView = findViewById(R.id.recycler_viewReceipt);
                noReceiptTextView = findViewById(R.id.text_viewNoReceipt);
                noBillingTextView = findViewById(R.id.text_viewNoBilling);

                monthLinearLayout.setOnClickListener(this);

                calenderView();

                monthTextView.setText(messages.getCurrentMonthYear());

                callApiReceiptInfo();
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    private boolean activitySetup() {
        messages = new Messages(this);
        Objects.requireNonNull(this.getSupportActionBar()).setTitle(messages.getToolbarTitle("Receipt Info"));
        this.getSupportActionBar().setElevation(0);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        dataBaseLite = new DataBaseLite(this);
        clubDetails = messages.getDashBoardView();
        login = messages.getCurrentLogin(this);
        if (clubDetails == null || login == null) {
            messages.showAlert(this);
            return false;
        }
        return true;
    }


    private void callApiReceiptInfo() {
        try {
            if (messages.isInternetAvailable()) {
                if (dataBaseLite.getReceiptInfoCount() > 0) {
                    dataBaseLite.truncateReceiptInfo();
                }
                PostData postData = new PostData();
                postData.setInterface("RestAPI");
                postData.setMethod("getReceiptno");
                SendDto sendDto = new SendDto();
                sendDto.setMemberId(login.getAccountNumber());
                String startDate = "", endDate = "";
                int month = messages.getMonthNumberFromName(monthTextView.getText().toString().trim().split(",")[0]);
                if (month < 10) {
                    startDate = "0" + messages.getMonthNumberFromName(monthTextView.getText().toString().trim().split(",")[0]) + "-01-" + monthTextView.getText().toString().trim().split(",")[1].trim();
                    endDate = "0" + messages.getMonthNumberFromName(monthTextView.getText().toString().trim().split(",")[0]) + "-31-" + monthTextView.getText().toString().trim().split(",")[1].trim();
                } else {
                    startDate = messages.getMonthNumberFromName(monthTextView.getText().toString().trim().split(",")[0]) + "-01-" + monthTextView.getText().toString().trim().split(",")[1].trim();
                    endDate = messages.getMonthNumberFromName(monthTextView.getText().toString().trim().split(",")[0]) + "-31-" + monthTextView.getText().toString().trim().split(",")[1].trim();
                }
                sendDto.setFdate(startDate);
                sendDto.setTodate(endDate);
                postData.setParameters(sendDto);
                RestAPICall restApi = messages.getAPIService(messages.getFirstStringURL(clubDetails.getURL(), messages.getLastStringURL(clubDetails.getURL())));
                messages.showProgressDialog(ReceiptsActivity.this, "", "Loading...");
                restApi.getReceiptInfo(messages.getLastStringURL(clubDetails.getURL()), postData).enqueue(new Callback<ResponseModelArray<ReceiptsInfo>>() {
                    @Override
                    public void onResponse(@NonNull Call<ResponseModelArray<ReceiptsInfo>> call, @NonNull Response<ResponseModelArray<ReceiptsInfo>> response) {
                        messages.hideDialog();
                        try {
                            if (response.isSuccessful()) {
                                if (response.body() != null) {
                                    if (response.body().isStatus()) {
                                        try {
                                            List<ReceiptsInfo> receiptsInfoList = response.body().getData();
                                            for (int i = 0; i < receiptsInfoList.size(); i++) {
                                                dataBaseLite.insertReceiptInfo(receiptsInfoList.get(i));
                                            }
                                            showReceiptInfo();
                                        } catch (Throwable e) {
                                            e.printStackTrace();
                                            messages.toast(ReceiptsActivity.this, e.getMessage());
                                        }
                                    } else {
                                        messages.toast(ReceiptsActivity.this, "Receipt Info not found");
                                    }
                                } else {
                                    messages.toast(ReceiptsActivity.this, "Receipt Info not found");
                                }
                            } else {
                                if (response.code() == 500) {
                                    messages.toast(ReceiptsActivity.this, response.message());
                                } else if (response.code() == 404) {
                                    messages.toast(ReceiptsActivity.this, "Internet connectivity issue could not connect to server");
                                } else {
                                    assert response.errorBody() != null;
                                    messages.toast(ReceiptsActivity.this, response.errorBody().string());
                                }
                            }
                        } catch (Throwable e) {
                            e.printStackTrace();
                            messages.toast(ReceiptsActivity.this, e.getMessage());
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<ResponseModelArray<ReceiptsInfo>> call, @NonNull Throwable t) {
                        try {
                            messages.hideDialog();
                            if (Objects.requireNonNull(t.getMessage()).contains(getResources().getString(R.string.network_failed))) {
                                messages.toastInternet(ReceiptsActivity.this, monthTextView);
                            } else {
                                messages.toast(ReceiptsActivity.this, t.getMessage());
                            }
                            Log.d(Constants.TAG, t.getMessage());
                        } catch (Throwable e) {
                            e.printStackTrace();
                        }
                    }
                });
            } else {
                messages.toastInternet(ReceiptsActivity.this, monthTextView);
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }


    private void showReceiptInfo() {
        try {
            List<ReceiptsInfo> receiptsInfoList = dataBaseLite.getReceiptInfoList();
            if (receiptsInfoList.size() > 0) {
                receiptRecyclerView.setVisibility(View.VISIBLE);
                noReceiptTextView.setVisibility(View.GONE);
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
                receiptRecyclerView.setLayoutManager(linearLayoutManager);

//                final CarouselLayoutManager layoutManager = new CarouselLayoutManager(CarouselLayoutManager.HORIZONTAL,false);
//                layoutManager.setPostLayoutListener(new CarouselZoomPostLayoutListener());
//                layoutManager.setMaxVisibleItems(2);
//                receiptRecyclerView.setHasFixedSize(true);
//                receiptRecyclerView.setLayoutManager(layoutManager);

                ReceiptInfoAdapter receiptInfoAdapter = new ReceiptInfoAdapter(receiptsInfoList, this);
                receiptRecyclerView.setAdapter(receiptInfoAdapter);
//                receiptRecyclerView.addOnScrollListener(new CenterScrollListener());
                receiptInfoAdapter.notifyDataSetChanged();
                receiptInfoAdapter.setOnRecyclerViewItemClickListener(new OnRecyclerViewItemClickListener<ReceiptInfoAdapter.MyViewHolder, ReceiptsInfo>() {
                    @Override
                    public void onRecyclerViewItemClick(@NonNull ReceiptInfoAdapter.MyViewHolder myViewHolder, @NonNull View view, @NonNull ReceiptsInfo receiptsInfo, int position) {
                        showReceiptInfoDetails(receiptsInfo);
                    }
                });

            } else {
                receiptRecyclerView.setVisibility(View.GONE);
                noReceiptTextView.setVisibility(View.VISIBLE);
            }
        } catch (Throwable e) {
            e.printStackTrace();
            receiptRecyclerView.setVisibility(View.GONE);
            noReceiptTextView.setVisibility(View.VISIBLE);
        }
    }

    public void calenderView() {
        rackMonthPicker = new RackMonthPicker(this)
                .setLocale(Locale.ENGLISH)
                .setPositiveButton(new DateMonthDialogListener() {
                    @Override
                    public void onDateMonth(int month, int startDate, int endDate, int year, String monthLabel) {
                        System.out.println(month);
                        System.out.println(startDate);
                        System.out.println(endDate);
                        System.out.println(year);
                        System.out.println(monthLabel);
                        if (messages.isFutureDate(messages.getCurrentMonthYear(), monthLabel)) {
                            Toast.makeText(ReceiptsActivity.this, "We can't generate the data for future date", Toast.LENGTH_SHORT).show();
                        } else {
                            monthTextView.setText(monthLabel);
                            callApiReceiptInfo();
                        }


                    }
                })
                .setNegativeButton(new OnCancelMonthDialogListener() {
                    @Override
                    public void onCancel(android.support.v7.app.AlertDialog dialog) {
                        dialog.dismiss();
                    }
                });

    }




    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.linear_layoutMonth:
                if (rackMonthPicker != null) {
                    rackMonthPicker.show();
                }
                break;
        }
    }


    @SuppressLint("SetTextI18n")
    private void showReceiptInfoDetails(final ReceiptsInfo receiptsInfo) {
        try {
            final Dialog dialog = new Dialog(this);
            dialog.getWindow();
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.dialog_receipt_info_details);
            Objects.requireNonNull(dialog.getWindow()).setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

            TextView gTotalTextVew = dialog.findViewById(R.id.text_viewGTotal);
            LinearLayout statusLinearLayout = dialog.findViewById(R.id.linear_layoutReceiptStatus);
            LinearLayout screenShotLinearLayout = dialog.findViewById(R.id.linear_layoutScreenShot);
            TextView billNoTextView = dialog.findViewById(R.id.text_viewBillNo);
            TextView dateTextView = dialog.findViewById(R.id.text_viewDate);
            TextView amountTextView = dialog.findViewById(R.id.text_viewAmount);
            TextView payStatus = dialog.findViewById(R.id.text_viewPayStatus);
            ImageView closeImageView = dialog.findViewById(R.id.image_viewClose);

            gTotalTextVew.setText(messages.getStringRupee(receiptsInfo.getAmount()));
            amountTextView.setText(messages.getStringRupee(receiptsInfo.getAmount()));
            billNoTextView.setText(receiptsInfo.getReceiptNo());
            dateTextView.setText(messages.getDateChange3Digit(receiptsInfo.getPaymentDate(),1));
            if (receiptsInfo.getPaymentMode().trim().toLowerCase().equals("onl")) {
                payStatus.setText("Successful payment via Online");
            } else {
                payStatus.setText("Successful payment via Cash");
            }
//            statusLinearLayout.setBackgroundColor(this.getResources().getColor(R.color.green));
            dialog.show();
            closeImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
            screenShotLinearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {

                        if (takeScreenshot(dialog) != null) {
                            String documents = "eClub";
                            String myFolder = Environment.getExternalStorageDirectory() + "/" + documents;
                            File f = new File(myFolder);
                            if (!f.exists()) {
                                if (!f.mkdir()) {
                                    Log.d(Constants.TAG, myFolder + " can't be created.");
                                } else {
                                    Log.d(Constants.TAG, myFolder + "folder exist");
                                }
                            }
                            File myFile = new File(f,   "screenshot-"+receiptsInfo.getReceiptNo()+".png");
                            FileOutputStream fos;
                            try {
                                fos = new FileOutputStream(myFile.getAbsoluteFile());
                                takeScreenshot(dialog).compress(Bitmap.CompressFormat.JPEG, 100, fos);
                                fos.flush();
                                fos.close();
                                Toast.makeText(ReceiptsActivity.this, "Image Saved", Toast.LENGTH_SHORT).show();
                            } catch (IOException e) {
                                Log.e(Constants.TAG, e.getMessage(), e);
                            }
                        }
                    } catch (Throwable e) {
                        e.printStackTrace();
                    }
                }
            });

        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    public Bitmap takeScreenshot(Dialog dialog) {
        try {
            View rootView = dialog.findViewById(android.R.id.content).getRootView();
            rootView.setDrawingCacheEnabled(true);
            return rootView.getDrawingCache();
        } catch (Throwable e) {
            e.printStackTrace();
            return null;
        }

    }

}
