package com.ircaindia.eclub.view.activity;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.ircaindia.eclub.R;
import com.ircaindia.eclub.model.adapter.AffiliatedClubsAdapter;
import com.ircaindia.eclub.model.apis.PostData;
import com.ircaindia.eclub.model.apis.RestAPICall;
import com.ircaindia.eclub.model.dto.AffiliatedClubs;
import com.ircaindia.eclub.model.dto.ClubDetails;
import com.ircaindia.eclub.model.dto.Login;
import com.ircaindia.eclub.model.dto.SendDto;
import com.ircaindia.eclub.model.retrofit.response.ResponseModelArray;
import com.ircaindia.eclub.model.sqlite.DataBaseLite;
import com.ircaindia.eclub.model.utils.Constants;
import com.ircaindia.eclub.model.utils.IMessages;
import com.ircaindia.eclub.model.utils.Messages;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AffiliatedClubsActivity extends AppCompatActivity {

    IMessages messages;
    DataBaseLite dataBaseLite;
    ClubDetails clubDetails;
    Login login;
    RecyclerView listRecyclerView;
    EditText searchEditText;
    TextView noClubsTextView;
    AffiliatedClubsAdapter affiliatedClubsAdapter;
    List<AffiliatedClubs> finalAffiliatedClubsList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_affiliated_clubs);
        if (activitySetup()) {
            listRecyclerView = findViewById(R.id.recycler_viewList);
            searchEditText = findViewById(R.id.edit_textSearch);
            noClubsTextView = findViewById(R.id.text_viewNoClubs);

            callApiAffiliatedClubs();

            searchEditText.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {

                    filter(editable.toString().trim());
                }
            });
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }

    private boolean activitySetup() {
        messages = new Messages(this);
        Objects.requireNonNull(this.getSupportActionBar()).setTitle(messages.getToolbarTitle("Affiliated Clubs"));
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.getSupportActionBar().setElevation(0);

        dataBaseLite = new DataBaseLite(this);
        clubDetails = messages.getDashBoardView();
        login = messages.getCurrentLogin(this);
        if (clubDetails == null || login == null) {
            messages.showAlert(this);
            return false;
        }
        return true;
    }

    private void filter(String text) {
        try {
            if (!text.isEmpty()) {
                List<AffiliatedClubs> affiliatedClubsList = new ArrayList<>();
                for (int i = 0; i < finalAffiliatedClubsList.size(); i++) {
                    if (finalAffiliatedClubsList.get(i).getAffiliationClubName().toLowerCase().contains(text.toLowerCase())) {
                        affiliatedClubsList.add(finalAffiliatedClubsList.get(i));
                    }
                }
                showAffiliatedClub(affiliatedClubsList, 1);
            } else {
                showAffiliatedClub(finalAffiliatedClubsList, 2);

            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    private void callApiAffiliatedClubs() {
        try {
            if (messages.isInternetAvailable()) {
                listRecyclerView.setVisibility(View.GONE);
                noClubsTextView.setVisibility(View.VISIBLE);
                finalAffiliatedClubsList.clear();
                PostData postData = new PostData();
                postData.setInterface("RestAPI");
                postData.setMethod("GetAffliatedClubDetails");
                SendDto sendDto = new SendDto();
                postData.setParameters(sendDto);
                RestAPICall restApi = messages.getAPIService(messages.getFirstStringURL(clubDetails.getURL(), messages.getLastStringURL(clubDetails.getURL())));
                messages.showProgressDialog(AffiliatedClubsActivity.this, "", "Loading...");
                restApi.getAffiliatedClubs(messages.getLastStringURL(clubDetails.getURL()), postData).enqueue(new Callback<ResponseModelArray<AffiliatedClubs>>() {
                    @Override
                    public void onResponse(@NonNull Call<ResponseModelArray<AffiliatedClubs>> call, @NonNull Response<ResponseModelArray<AffiliatedClubs>> response) {
                        messages.hideDialog();
                        try {
                            if (response.isSuccessful()) {
                                if (response.body() != null) {
                                    if (response.body().isStatus()) {
                                        try {
                                            List<AffiliatedClubs> affiliatedClubsList = response.body().getData();
//                                            for (int i = 0; i < affiliatedClubsList.size(); i++) {
//                                                dataBaseLite.insertBillingInfo(billingInfoList.get(i));
//                                            }
                                            finalAffiliatedClubsList = affiliatedClubsList;
                                            showAffiliatedClub(affiliatedClubsList, 1);
                                        } catch (Throwable e) {
                                            e.printStackTrace();
                                            messages.toast(AffiliatedClubsActivity.this, e.getMessage());
                                        }
                                    } else {
                                        messages.toast(AffiliatedClubsActivity.this, "Affiliated Clubs not found");
                                    }
                                } else {
                                    messages.toast(AffiliatedClubsActivity.this, "Affiliated Clubs not found");
                                }
                            } else {
                                if (response.code() == 500) {
                                    messages.toast(AffiliatedClubsActivity.this, response.message());
                                } else if (response.code() == 404) {
                                    messages.toast(AffiliatedClubsActivity.this, "Internet connectivity issue could not connect to server");
                                } else {
                                    assert response.errorBody() != null;
                                    messages.toast(AffiliatedClubsActivity.this, response.errorBody().string());
                                }
                            }
                        } catch (Throwable e) {
                            e.printStackTrace();
                            messages.toast(AffiliatedClubsActivity.this, e.getMessage());
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<ResponseModelArray<AffiliatedClubs>> call, @NonNull Throwable t) {
                        try {
                            messages.hideDialog();
                            if (Objects.requireNonNull(t.getMessage()).contains(getResources().getString(R.string.network_failed))) {
                                messages.toastInternet(AffiliatedClubsActivity.this, listRecyclerView);
                            } else {
                                messages.toast(AffiliatedClubsActivity.this, t.getMessage());
                            }
                            Log.d(Constants.TAG, t.getMessage());
                        } catch (Throwable e) {
                            e.printStackTrace();
                        }
                    }
                });
            } else {
                messages.toastInternet(AffiliatedClubsActivity.this, listRecyclerView);
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    private void showAffiliatedClub(List<AffiliatedClubs> affiliatedClubsList, int key) {
        try {
            if (affiliatedClubsList.size() > 0) {
                List<String> stringList = new ArrayList<>();
                List<AffiliatedClubs> affiliatedClubsList1 = new ArrayList<>();
                for (int i = 0; i < affiliatedClubsList.size(); i++) {
                    stringList.add(affiliatedClubsList.get(i).getAffiliationClubName().toUpperCase().charAt(0) + "");
                }
                HashSet<String> hashSet = new HashSet<>(stringList);
                stringList.clear();
                stringList.addAll(hashSet);

                Collections.sort(stringList);

                for (int i = 0; i < stringList.size(); i++) {
                    AffiliatedClubs affiliatedClubs = new AffiliatedClubs();
                    affiliatedClubs.setAlpha(stringList.get(i));
                    List<AffiliatedClubs> affiliatedClubsList2 = new ArrayList<>();
                    for (int j = 0; j < affiliatedClubsList.size(); j++) {
                        if (stringList.get(i).toUpperCase().equals(affiliatedClubsList.get(j).getAffiliationClubName().toUpperCase().charAt(0) + "")) {
                            affiliatedClubsList2.add(affiliatedClubsList.get(j));
                        }
                    }
                    affiliatedClubs.setAffiliatedClubsList(affiliatedClubsList2);
                    affiliatedClubsList1.add(affiliatedClubs);
                }

                if (affiliatedClubsList1.size() > 0) {
                    listRecyclerView.setVisibility(View.VISIBLE);
                    noClubsTextView.setVisibility(View.GONE);
                    if (key == 1) {
                        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
                        listRecyclerView.setLayoutManager(linearLayoutManager);
                        affiliatedClubsAdapter = new AffiliatedClubsAdapter(affiliatedClubsList1, this);
                        listRecyclerView.setAdapter(affiliatedClubsAdapter);
                        affiliatedClubsAdapter.notifyDataSetChanged();
                    } else {
                        if (affiliatedClubsAdapter != null)
                            affiliatedClubsAdapter.filterList(affiliatedClubsList1, AffiliatedClubsActivity.this);
                    }

                } else {
                    listRecyclerView.setVisibility(View.GONE);
                    noClubsTextView.setVisibility(View.VISIBLE);
                }
            } else {
                listRecyclerView.setVisibility(View.GONE);
                noClubsTextView.setVisibility(View.VISIBLE);
            }
        } catch (Throwable e) {
            e.printStackTrace();
            listRecyclerView.setVisibility(View.GONE);
            noClubsTextView.setVisibility(View.VISIBLE);
        }
    }

}
