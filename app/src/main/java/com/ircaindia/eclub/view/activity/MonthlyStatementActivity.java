package com.ircaindia.eclub.view.activity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.ircaindia.eclub.R;
import com.ircaindia.eclub.model.adapter.StatementAdapter;
import com.ircaindia.eclub.model.adapter.StatementDetailsAdapter;
import com.ircaindia.eclub.model.adapter.StatementGraphAdapter;
import com.ircaindia.eclub.model.apis.PostData;
import com.ircaindia.eclub.model.apis.RestAPICall;
import com.ircaindia.eclub.model.dto.ClubDetails;
import com.ircaindia.eclub.model.dto.Login;
import com.ircaindia.eclub.model.dto.MonthlyStatement;
import com.ircaindia.eclub.model.dto.Profile;
import com.ircaindia.eclub.model.dto.SendDto;
import com.ircaindia.eclub.model.dto.StatementMain;
import com.ircaindia.eclub.model.retrofit.response.ResponseModelArray;
import com.ircaindia.eclub.model.sqlite.DataBaseLite;
import com.ircaindia.eclub.model.utils.Constants;
import com.ircaindia.eclub.model.utils.IMessages;
import com.ircaindia.eclub.model.utils.Messages;
import com.ircaindia.eclub.model.utils.OnRecyclerViewItemClickListener;
import com.ircaindia.eclub.model.utils.monthpicker.RackMonthPicker;
import com.ircaindia.eclub.model.utils.monthpicker.listener.DateMonthDialogListener;
import com.ircaindia.eclub.model.utils.monthpicker.listener.OnCancelMonthDialogListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MonthlyStatementActivity extends AppCompatActivity implements View.OnClickListener {

    IMessages messages;
    DataBaseLite dataBaseLite;
    ClubDetails clubDetails;
    Profile profile;
    Login login;

    LinearLayout monthLinearLayout;
    TextView monthTextView;
    RackMonthPicker rackMonthPicker;
    PieChart statementPieChart;
    RecyclerView itemsRecyclerView, statementRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_monthly_statement);
        try {
            if (activitySetup()) {
                monthLinearLayout = findViewById(R.id.linear_layoutMonth);
                monthTextView = findViewById(R.id.text_viewMonth);
                statementPieChart = findViewById(R.id.pie_chartStatement);
                itemsRecyclerView = findViewById(R.id.recycler_viewItems);
                statementRecyclerView = findViewById(R.id.recycler_viewStatement);

                calenderView();
                monthTextView.setText(messages.getCurrentMonthYear());

                monthLinearLayout.setOnClickListener(this);

                callApiStatement();

            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    private void callApiStatement() {
        try {
            if (messages.isInternetAvailable()) {
                PostData postData = new PostData();
                postData.setInterface("RestAPI");
                postData.setMethod("GetMonthlyBill");
                SendDto sendDto = new SendDto();
                sendDto.setA1(login.getAccountNumber());
                sendDto.setA2(login.getAccountNumber());
                String startDate = monthTextView.getText().toString().trim().split(",")[1].trim() + "-" + messages.getMonthNumberFromName(monthTextView.getText().toString().trim().split(",")[0]) + "-01";
                String endDate = monthTextView.getText().toString().trim().split(",")[1].trim() + "-" + messages.getMonthNumberFromName(monthTextView.getText().toString().trim().split(",")[0]) + "-31";
                sendDto.setD1(startDate);
                sendDto.setD2(endDate);
                sendDto.setF("0");
                postData.setParameters(sendDto);
                RestAPICall restApi = messages.getAPIService(messages.getFirstStringURL(clubDetails.getURL(), messages.getLastStringURL(clubDetails.getURL())));
                messages.showProgressDialog(MonthlyStatementActivity.this, "", "Loading...");
                restApi.getMonthlyStatement(messages.getLastStringURL(clubDetails.getURL()), postData).enqueue(new Callback<ResponseModelArray<MonthlyStatement>>() {
                    @Override
                    public void onResponse(@NonNull Call<ResponseModelArray<MonthlyStatement>> call, @NonNull Response<ResponseModelArray<MonthlyStatement>> response) {
                        messages.hideDialog();
                        try {
                            if (response.isSuccessful()) {
                                if (response.body() != null) {
                                    if (response.body().isStatus()) {
                                        try {
                                            List<MonthlyStatement> monthlyStatements = response.body().getData();
                                            showMonthlyStatement(monthlyStatements);
                                        } catch (Throwable e) {
                                            e.printStackTrace();
                                            messages.toast(MonthlyStatementActivity.this, e.getMessage());
                                        }
                                    } else {
                                        messages.toast(MonthlyStatementActivity.this, "Monthly Statement not found");
                                    }
                                } else {
                                    messages.toast(MonthlyStatementActivity.this, "Monthly Statement not found");
                                }
                            } else {
                                if (response.code() == 500) {
                                    messages.toast(MonthlyStatementActivity.this, response.message());
                                } else if (response.code() == 404) {
                                    messages.toast(MonthlyStatementActivity.this, "Internet connectivity issue could not connect to server");
                                } else {
                                    assert response.errorBody() != null;
                                    messages.toast(MonthlyStatementActivity.this, response.errorBody().string());
                                }
                            }
                        } catch (Throwable e) {
                            e.printStackTrace();
                            messages.toast(MonthlyStatementActivity.this, e.getMessage());
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<ResponseModelArray<MonthlyStatement>> call, @NonNull Throwable t) {
                        try {
                            messages.hideDialog();
                            if (Objects.requireNonNull(t.getMessage()).contains(getResources().getString(R.string.network_failed))) {
                                messages.toastInternet(MonthlyStatementActivity.this, monthTextView);
                            } else {
                                messages.toast(MonthlyStatementActivity.this, t.getMessage());
                            }
                            Log.d(Constants.TAG, t.getMessage());
                        } catch (Throwable e) {
                            e.printStackTrace();
                        }
                    }
                });
            } else {
                messages.toastInternet(MonthlyStatementActivity.this, monthTextView);
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    public void calenderView() {
        rackMonthPicker = new RackMonthPicker(this)
                .setLocale(Locale.ENGLISH)
                .setPositiveButton(new DateMonthDialogListener() {
                    @Override
                    public void onDateMonth(int month, int startDate, int endDate, int year, String monthLabel) {
                        System.out.println(month);
                        System.out.println(startDate);
                        System.out.println(endDate);
                        System.out.println(year);
                        System.out.println(monthLabel);
                        if (messages.isFutureDate(messages.getCurrentMonthYear(), monthLabel)) {
                            Toast.makeText(MonthlyStatementActivity.this, "We can't generate the data for future date", Toast.LENGTH_SHORT).show();
                        } else {
                            monthTextView.setText(monthLabel);
                            callApiStatement();
                        }


                    }
                })
                .setNegativeButton(new OnCancelMonthDialogListener() {
                    @Override
                    public void onCancel(android.support.v7.app.AlertDialog dialog) {
                        dialog.dismiss();
                    }
                });

    }

    private void showMonthlyStatement(List<MonthlyStatement> monthlyStatementList) {
        List<StatementMain> statementMainList = new ArrayList<>();
        // BAR
        Double barTotal = 0.0;
        StatementMain barStatementMain = new StatementMain();
        barStatementMain.setName("Bar");
        List<MonthlyStatement> barMonthlyStatementList = new ArrayList<>();

        // KITCHEN
        Double kitchenTotal = 0.0;
        StatementMain kitchenStatementMain = new StatementMain();
        kitchenStatementMain.setName("Kitchen");
        List<MonthlyStatement> kitchenStatementList = new ArrayList<>();

        //FOR EPGC CLUB
        // GOLF
        Double golfTotal = 0.0;
        StatementMain golfStatementMain = new StatementMain();
        golfStatementMain.setName("Golf");
        List<MonthlyStatement> golfStatementList = new ArrayList<>();


        // OTHERS
        Double otherTotal = 0.0, particularAmount = 0.0, balance = 0.0;
        StatementMain otherStatementMain = new StatementMain();
        if (messages.getDashBoardView().getId().equals("66")) {
            otherStatementMain.setName("Sports &amp; Others");
        } else {
            otherStatementMain.setName("Others");
        }
        List<MonthlyStatement> otherStatementList = new ArrayList<>();

        // TOTAL
        Double totalAmount = 0.0, grandTotalAmount = 0.0, receiptAmount = 0.0, netAmount = 0.0;
        StatementMain totalStatementMain = new StatementMain();
        totalStatementMain.setName("Net Due");
        List<MonthlyStatement> totalStatementList = new ArrayList<>();

        for (int i = 0; i < monthlyStatementList.size(); i++) {
            MonthlyStatement monthlyStatementMain = monthlyStatementList.get(i);
            if (monthlyStatementMain.getType().toLowerCase().equals("m")) {
                //BAR
                if (!(monthlyStatementMain.getBaramount().equals("0") || monthlyStatementMain.getBaramount().isEmpty())) {
                    MonthlyStatement monthlyStatement = new MonthlyStatement();
                    monthlyStatement.setAmount(monthlyStatementMain.getBaramount());
                    monthlyStatement.setTdate(monthlyStatementMain.getTdate());
                    barMonthlyStatementList.add(monthlyStatement);
                    barTotal = barTotal + Double.parseDouble(monthlyStatementMain.getBaramount());
                }
                //KITCHEN
                if (!(monthlyStatementMain.getKamount().equals("0") || monthlyStatementMain.getKamount().isEmpty())) {
                    MonthlyStatement monthlyStatement = new MonthlyStatement();
                    monthlyStatement.setAmount(monthlyStatementMain.getKamount());
                    monthlyStatement.setTdate(monthlyStatementMain.getTdate());
                    kitchenStatementList.add(monthlyStatement);
                    kitchenTotal = kitchenTotal + Double.parseDouble(monthlyStatementMain.getKamount());
                }
            }

            if (monthlyStatementMain.getType().toLowerCase().equals("o")) {

                if (monthlyStatementMain.getItemcatname() != null) {
                    if (monthlyStatementMain.getItemcatname().toLowerCase().equals("golf")) {
                        //GOLF
                        if (!monthlyStatementMain.getAmount().equals("0") || !monthlyStatementMain.getAmount().isEmpty()) {
                            MonthlyStatement monthlyStatement = new MonthlyStatement();
                            monthlyStatement.setAmount(monthlyStatementMain.getAmount());
                            monthlyStatement.setTdate(monthlyStatementMain.getTdate());
                            monthlyStatement.setParticulars(monthlyStatementMain.getParticulars());
                            golfStatementList.add(monthlyStatement);
                            golfTotal = golfTotal + Double.parseDouble(monthlyStatementMain.getAmount());
                        }
                    } else {
                        //OTHERS
                        if (!monthlyStatementMain.getAmount().equals("0") || !monthlyStatementMain.getAmount().isEmpty()) {
                            MonthlyStatement monthlyStatement = new MonthlyStatement();
                            monthlyStatement.setAmount(monthlyStatementMain.getAmount());
                            monthlyStatement.setTdate(monthlyStatementMain.getTdate());
                            monthlyStatement.setParticulars(monthlyStatementMain.getParticulars());
                            otherStatementList.add(monthlyStatement);
                            otherTotal = otherTotal + Double.parseDouble(monthlyStatementMain.getAmount());
                        }
                    }
                } else {
                    //OTHERS
                    if (!monthlyStatementMain.getAmount().equals("0") || !monthlyStatementMain.getAmount().isEmpty()) {
                        MonthlyStatement monthlyStatement = new MonthlyStatement();
                        monthlyStatement.setAmount(monthlyStatementMain.getAmount());
                        monthlyStatement.setTdate(monthlyStatementMain.getTdate());
                        monthlyStatement.setParticulars(monthlyStatementMain.getParticulars());
                        otherStatementList.add(monthlyStatement);
                        otherTotal = otherTotal + Double.parseDouble(monthlyStatementMain.getAmount());
                    }
                }
            }

            //NET DUE
            if (!(monthlyStatementMain.getType().equals("") || monthlyStatementMain.getType().toLowerCase().equals("m") || monthlyStatementMain.getType().toLowerCase().equals("a") || monthlyStatementMain.getType().toLowerCase().equals("o"))) {
                if (!monthlyStatementMain.getType().toUpperCase().equals("L")) {
                    particularAmount = particularAmount + Double.parseDouble(monthlyStatementMain.getAmount());
                    if (!(monthlyStatementMain.getParticulars().equalsIgnoreCase("KITCHEN USAGE")) || !(monthlyStatementMain.getParticulars().equals("BAR USAGE"))) {
                        MonthlyStatement monthlyStatement = new MonthlyStatement();
                        monthlyStatement.setAmount(monthlyStatementMain.getAmount());
                        monthlyStatement.setTdate(monthlyStatementMain.getTdate());
                        monthlyStatement.setParticulars(monthlyStatementMain.getParticulars());
                        totalStatementList.add(monthlyStatement);
                    }
                } else {
                    try {
                        balance = Double.parseDouble(monthlyStatementMain.getAmount());
                    } catch (Throwable e) {
                        e.printStackTrace();
                    }
                }
            }

            if (!monthlyStatementMain.getRamount().equals("0")) {
                receiptAmount = receiptAmount + Double.parseDouble(monthlyStatementMain.getRamount());
            }
        }
        totalAmount = particularAmount + otherTotal + golfTotal;
        grandTotalAmount = totalAmount + balance;
        netAmount = grandTotalAmount - receiptAmount;

        barStatementMain.setAmount(messages.round(barTotal, 2) + "");
        barStatementMain.setColor(getResources().getColor(R.color.lightIndigo));
        kitchenStatementMain.setAmount(messages.round(kitchenTotal, 2) + "");
        kitchenStatementMain.setColor(getResources().getColor(R.color.lightDeepOrange));
        golfStatementMain.setAmount(messages.round(golfTotal, 2) + "");
        golfStatementMain.setColor(getResources().getColor(R.color.lightAmber));
        otherStatementMain.setAmount(messages.round(otherTotal, 2) + "");
        otherStatementMain.setColor(getResources().getColor(R.color.lightTeal));
        totalStatementMain.setAmount(messages.round(netAmount, 2) + "");
        totalStatementMain.setColor(getResources().getColor(R.color.lightDarkRed));


        if (barTotal == 0) {
            MonthlyStatement monthlyStatement = new MonthlyStatement();
            monthlyStatement.setAmount("0");
            monthlyStatement.setParticulars("Bar Amount");
            barMonthlyStatementList.add(monthlyStatement);
        }
        if (kitchenTotal == 0) {
            MonthlyStatement monthlyStatement = new MonthlyStatement();
            monthlyStatement.setAmount("0");
            monthlyStatement.setParticulars("Kitchen Amount");
            kitchenStatementList.add(monthlyStatement);
        }
        if (golfTotal == 0) {
            MonthlyStatement monthlyStatement = new MonthlyStatement();
            monthlyStatement.setAmount("0");
            monthlyStatement.setParticulars("Golf Amount");
            golfStatementList.add(monthlyStatement);
        }
        if (otherTotal == 0) {
            MonthlyStatement monthlyStatement = new MonthlyStatement();
            monthlyStatement.setAmount("0");
            monthlyStatement.setParticulars("Others Amount");
            otherStatementList.add(monthlyStatement);
        }

        if (particularAmount == 0) {
            MonthlyStatement monthlyStatement = new MonthlyStatement();
            monthlyStatement.setAmount("0");
            monthlyStatement.setParticulars("Total Amount");
            totalStatementList.add(monthlyStatement);
        }

        MonthlyStatement monthlyStatement;
        monthlyStatement = new MonthlyStatement();
        monthlyStatement.setAmount(messages.round(totalAmount, 2) + "");
        monthlyStatement.setParticulars("Total For Month");
        totalStatementList.add(monthlyStatement);

        monthlyStatement = new MonthlyStatement();
        monthlyStatement.setAmount(messages.round(balance, 2) + "");
        monthlyStatement.setParticulars("Balance B/F");
        totalStatementList.add(monthlyStatement);

        monthlyStatement = new MonthlyStatement();
        monthlyStatement.setAmount(messages.round(grandTotalAmount, 2) + "");
        monthlyStatement.setParticulars("Grand Total");
        totalStatementList.add(monthlyStatement);

        monthlyStatement = new MonthlyStatement();
        monthlyStatement.setAmount(messages.round(receiptAmount, 2) + "");
        monthlyStatement.setParticulars("Amount Paid");
        totalStatementList.add(monthlyStatement);

        monthlyStatement = new MonthlyStatement();
        monthlyStatement.setAmount(messages.round(netAmount, 2) + "");
        monthlyStatement.setParticulars("Net Due Amount");
        totalStatementList.add(monthlyStatement);

        barStatementMain.setMonthlyStatementList(barMonthlyStatementList);
        kitchenStatementMain.setMonthlyStatementList(kitchenStatementList);
        golfStatementMain.setMonthlyStatementList(golfStatementList);
        otherStatementMain.setMonthlyStatementList(otherStatementList);
        totalStatementMain.setMonthlyStatementList(totalStatementList);

        statementMainList.add(barStatementMain);
        statementMainList.add(kitchenStatementMain);
        if (messages.getDashBoardView().getId().equals("74"))
            statementMainList.add(golfStatementMain);
        statementMainList.add(otherStatementMain);
        statementMainList.add(totalStatementMain);


        setPieChart(statementMainList);


        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        statementRecyclerView.setLayoutManager(linearLayoutManager);
        StatementAdapter statementAdapter = new StatementAdapter(statementMainList, this);
        statementRecyclerView.setAdapter(statementAdapter);
        statementAdapter.notifyDataSetChanged();

        statementAdapter.setOnRecyclerViewItemClickListener(new OnRecyclerViewItemClickListener<StatementAdapter.MyViewHolder, StatementMain>() {
            @Override
            public void onRecyclerViewItemClick(@NonNull StatementAdapter.MyViewHolder myViewHolder, @NonNull View view, @NonNull StatementMain statementMain, int position) {
                Toast.makeText(MonthlyStatementActivity.this, "Clicked", Toast.LENGTH_SHORT).show();
                showStatementDetails(statementMain);
            }
        });

//        Double finalTotalAmount = 0.0;
//        for (int i = 0; i < statementMainList.size(); i++) {
//            finalTotalAmount = finalTotalAmount + Double.parseDouble(statementMainList.get(i).getAmount());
//        }
//
//        StatementMain finalStatementMain = new StatementMain();
//        finalStatementMain.setName("Total");
//        finalStatementMain.setAmount(messages.round(finalTotalAmount, 2) + "");
//        List<StatementMain> statementMainList1 = new ArrayList<>(statementMainList);
//        statementMainList1.add(finalStatementMain);

        LinearLayoutManager linearLayoutManager1 = new LinearLayoutManager(this);
        itemsRecyclerView.setLayoutManager(linearLayoutManager1);
        StatementGraphAdapter statementGraphAdapter = new StatementGraphAdapter(statementMainList, this);
        itemsRecyclerView.setAdapter(statementGraphAdapter);
        statementGraphAdapter.notifyDataSetChanged();

    }

    public void setPieChart(List<StatementMain> statementMainList) {
        try {
            statementPieChart.getDescription().setEnabled(false);
            //usagesPieChart.setDescription("");
            statementPieChart.setExtraOffsets(5, 5, 5, 5);
            statementPieChart.setDragDecelerationFrictionCoef(0.9f);
            statementPieChart.setTransparentCircleRadius(10);
            statementPieChart.setHoleColor(Color.WHITE);
            statementPieChart.setHoleRadius(40f);
            statementPieChart.animateY(1000, Easing.EasingOption.EaseInOutCubic);
            statementPieChart.setDrawHoleEnabled(true);

//            usagesPieChart.setCenterText("Total \n" + tot);
            statementPieChart.setDrawEntryLabels(false);
            statementPieChart.getLegend().setEnabled(false);


//            usagesPieChart.getData().setDrawValues(false);
            ArrayList<PieEntry> yValues = new ArrayList<>();
            for (int i = 0; i < statementMainList.size(); i++) {
                if (Float.parseFloat(statementMainList.get(i).getAmount()) >= 0) {
                    yValues.add(new PieEntry(Float.parseFloat(statementMainList.get(i).getAmount()), ""));
                } else {
                    yValues.add(new PieEntry(0, ""));
                }
            }

            PieDataSet dataSet = new PieDataSet(yValues, "");
            dataSet.setSliceSpace(0f);
            dataSet.setSelectionShift(5f);
//            dataSet.setDrawValues(false);
            if (messages.getDashBoardView().getId().equals("74")) {
                int[] color = {getResources().getColor(R.color.lightIndigo), getResources().getColor(R.color.lightDeepOrange), getResources().getColor(R.color.lightAmber), getResources().getColor(R.color.lightTeal), getResources().getColor(R.color.lightDarkRed)};
                dataSet.setColors(color);
            } else {
                int[] color = {getResources().getColor(R.color.lightIndigo), getResources().getColor(R.color.lightDeepOrange), getResources().getColor(R.color.lightTeal), getResources().getColor(R.color.lightDarkRed)};
                dataSet.setColors(color);
            }
            PieData pieData = new PieData((dataSet));
            pieData.setValueTextSize(8f);
            pieData.setValueTextColor(Color.WHITE);
            statementPieChart.setData(pieData);
            //PieChart Ends Here
        } catch (Throwable e) {
            e.printStackTrace();
        }


    }

    private boolean activitySetup() {
        messages = new Messages(this);
        Objects.requireNonNull(this.getSupportActionBar()).setTitle(messages.getToolbarTitle("Monthly Statement"));
        this.getSupportActionBar().setElevation(0);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        dataBaseLite = new DataBaseLite(this);
        clubDetails = messages.getDashBoardView();
        profile = messages.getProfile();
        login = messages.getCurrentLogin(this);
        if (clubDetails == null || login == null || profile == null) {
            messages.showAlert(this);
            return false;
        }
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }


    @SuppressLint("SetTextI18n")
    private void showStatementDetails(StatementMain statementMain) {
        try {
            final Dialog dialog = new Dialog(MonthlyStatementActivity.this);
            dialog.getWindow();
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.dialog_statement);
            Objects.requireNonNull(dialog.getWindow()).setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

            TextView gTotalTextVew = dialog.findViewById(R.id.text_viewGTotal);
            TextView gTotalMainTextVew = dialog.findViewById(R.id.text_viewGTotalMain);
            RecyclerView itemsRecyclerView = dialog.findViewById(R.id.recycler_viewItems);
            TextView mainTypeTextView = dialog.findViewById(R.id.text_viewMainType);
            TextView typeTextView = dialog.findViewById(R.id.text_viewType);
            ImageView closeImageView = dialog.findViewById(R.id.image_viewClose);

            gTotalMainTextVew.setText("₹ " + statementMain.getAmount());
            gTotalTextVew.setText("₹ " + statementMain.getAmount());
            mainTypeTextView.setText(statementMain.getName());
            try {
                if (statementMain.getName().equals("Net Due")) {
                    typeTextView.setText("Type");
                } else {
                    if (Double.parseDouble(statementMain.getAmount()) == 0) {
                        typeTextView.setText("Type");
                    } else {
                        typeTextView.setText("Date");
                    }
                }
            } catch (Throwable e) {
                e.printStackTrace();
            }

            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
            itemsRecyclerView.setLayoutManager(linearLayoutManager);
            StatementDetailsAdapter statementDetailsAdapter = new StatementDetailsAdapter(statementMain.getMonthlyStatementList(), MonthlyStatementActivity.this, typeTextView.getText().toString().trim());
            itemsRecyclerView.setAdapter(statementDetailsAdapter);
            statementDetailsAdapter.notifyDataSetChanged();
            dialog.show();
            closeImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });

        } catch (Throwable e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.linear_layoutMonth:
                if (rackMonthPicker != null) {
                    rackMonthPicker.show();
                }
                break;
        }
    }
}
