package com.ircaindia.eclub.view.activity;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.ircaindia.eclub.R;
import com.ircaindia.eclub.model.apis.PostData;
import com.ircaindia.eclub.model.apis.RestAPICall;
import com.ircaindia.eclub.model.dto.ChangePassword;
import com.ircaindia.eclub.model.dto.ClubDetails;
import com.ircaindia.eclub.model.dto.Login;
import com.ircaindia.eclub.model.dto.Profile;
import com.ircaindia.eclub.model.dto.RememberMe;
import com.ircaindia.eclub.model.dto.SendDto;
import com.ircaindia.eclub.model.retrofit.response.ResponseModelArray;
import com.ircaindia.eclub.model.sqlite.DataBaseLite;
import com.ircaindia.eclub.model.utils.Constants;
import com.ircaindia.eclub.model.utils.IMessages;
import com.ircaindia.eclub.model.utils.Messages;

import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangePasswordActivity extends AppCompatActivity implements View.OnClickListener {


    IMessages messages;
    DataBaseLite dataBaseLite;
    ClubDetails clubDetails;
    Profile profile;
    Login login;

    Button submitButton;

    EditText newPasswordEditText, passwordEditText, noEditText;
    boolean isNew, isCurrent;
    ImageView passwordImageView, newPasswordImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        try {
            if (activitySetup()) {
                newPasswordEditText = findViewById(R.id.edit_textNewPassword);
                newPasswordImageView = findViewById(R.id.image_viewNewPassword);
                passwordEditText = findViewById(R.id.edit_textPassword);
                passwordImageView = findViewById(R.id.image_viewPassword);
                noEditText = findViewById(R.id.edit_textNo);
                submitButton = findViewById(R.id.buttonSubmit);

                newPasswordImageView.setOnClickListener(this);
                submitButton.setOnClickListener(this);
                passwordImageView.setOnClickListener(this);


                noEditText.setText(login.getAccountNumber());

            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    private boolean activitySetup() {
        messages = new Messages(this);
        Objects.requireNonNull(this.getSupportActionBar()).setTitle(messages.getToolbarTitle("Change Password"));
        this.getSupportActionBar().setElevation(0);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.getSupportActionBar().hide();

        dataBaseLite = new DataBaseLite(this);
        clubDetails = messages.getDashBoardView();
        profile = messages.getProfile();
        login = messages.getCurrentLogin(this);
        if (clubDetails == null || login == null || profile == null) {
            messages.showAlert(this);
            return false;
        }
        return true;
    }


    private boolean validations() {
        if (noEditText.getText().toString().trim().isEmpty()) {
            Toast.makeText(this, "Membership number not found, Please login again", Toast.LENGTH_SHORT).show();
            messages.showAlert(this);
            return false;
        } else if (passwordEditText.getText().toString().trim().isEmpty()) {
            Toast.makeText(this, "Please enter current password", Toast.LENGTH_SHORT).show();
            return false;
        } else if (!passwordEditText.getText().toString().trim().equals(messages.getRememberMe(ChangePasswordActivity.this).getPassWord())) {
            Toast.makeText(this, "Please enter valid current password", Toast.LENGTH_SHORT).show();
            return false;
        } else if (passwordEditText.getText().toString().trim().equals(newPasswordEditText.getText().toString().trim())) {
            Toast.makeText(this, "Please enter a New Password", Toast.LENGTH_SHORT).show();
            return false;
        } else if (newPasswordEditText.getText().toString().trim().isEmpty()) {
            Toast.makeText(this, "Please enter new password", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }

    private void callApiChangePassword() {
        try {
            if (messages.isInternetAvailable()) {
                PostData postData = new PostData();
                postData.setInterface("RestAPI");
                postData.setMethod("changepassword");
                SendDto sendDto = new SendDto();
                sendDto.setAccountno(login.getAccountNumber());
                sendDto.setNewpwd(newPasswordEditText.getText().toString().trim());
                postData.setParameters(sendDto);
                RestAPICall restApi = messages.getAPIService(messages.getFirstStringURL(clubDetails.getURL(), messages.getLastStringURL(clubDetails.getURL())));
                messages.showProgressDialog(ChangePasswordActivity.this, "", "Loading...");
                restApi.changePassword(messages.getLastStringURL(clubDetails.getURL()), postData).enqueue(new Callback<ResponseModelArray<ChangePassword>>() {
                    @Override
                    public void onResponse(@NonNull Call<ResponseModelArray<ChangePassword>> call, @NonNull Response<ResponseModelArray<ChangePassword>> response) {
                        messages.hideDialog();
                        try {
                            if (response.isSuccessful()) {
                                if (response.body() != null) {
                                    if (response.body().isStatus()) {
                                        try {
                                            String status = response.body().getData().get(0).getChange();
                                            if (status.toLowerCase().equals("true")) {
                                                RememberMe rememberMe = new RememberMe();
                                                rememberMe.setUserName(messages.getRememberMe(ChangePasswordActivity.this).getUserName());
                                                rememberMe.setPassWord(newPasswordEditText.getText().toString().trim());
                                                rememberMe.setSave(false);
                                                messages.rememberMe(ChangePasswordActivity.this, rememberMe);
                                                passwordEditText.setText("");
                                                newPasswordEditText.setText("");
                                                messages.showAlertPassword(ChangePasswordActivity.this);
                                            } else {
                                                messages.toast(ChangePasswordActivity.this, status);
                                            }
                                        } catch (Throwable e) {
                                            e.printStackTrace();
                                            messages.toast(ChangePasswordActivity.this, e.getMessage());
                                        }
                                    } else {
                                        messages.toast(ChangePasswordActivity.this, "failed to update the password");
                                    }
                                } else {
                                    messages.toast(ChangePasswordActivity.this, "failed to update the password");
                                }
                            } else {
                                if (response.code() == 500) {
                                    messages.toast(ChangePasswordActivity.this, response.message());
                                } else if (response.code() == 404) {
                                    messages.toast(ChangePasswordActivity.this, "Internet connectivity issue could not connect to server");
                                } else {
                                    assert response.errorBody() != null;
                                    messages.toast(ChangePasswordActivity.this, response.errorBody().string());
                                }
                            }
                        } catch (Throwable e) {
                            e.printStackTrace();
                            messages.toast(ChangePasswordActivity.this, e.getMessage());
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<ResponseModelArray<ChangePassword>> call, @NonNull Throwable t) {
                        try {
                            messages.hideDialog();
                            if (Objects.requireNonNull(t.getMessage()).contains(getResources().getString(R.string.network_failed))) {
                                messages.toastInternet(ChangePasswordActivity.this, noEditText);
                            } else {
                                messages.toast(ChangePasswordActivity.this, t.getMessage());
                            }
                            Log.d(Constants.TAG, t.getMessage());
                        } catch (Throwable e) {
                            e.printStackTrace();
                        }
                    }
                });
            } else {
                messages.toastInternet(ChangePasswordActivity.this, noEditText);
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.image_viewNewPassword:
                if (isNew) {
                    newPasswordEditText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    isNew = false;
                    newPasswordImageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_visibility_off));
                } else {
                    newPasswordEditText.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    isNew = true;
                    newPasswordImageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_visibility_on));
                }
                newPasswordEditText.setSelection(newPasswordEditText.length());
                break;
            case R.id.image_viewPassword:
                if (isCurrent) {
                    passwordEditText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    passwordImageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_visibility_off));
                    isCurrent = false;
                } else {
                    passwordEditText.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    isCurrent = true;
                    passwordImageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_visibility_on));
                }
                passwordEditText.setSelection(passwordEditText.length());
                break;
            case R.id.buttonSubmit:
                if (validations()) {
                    callApiChangePassword();
                }
                break;
        }
    }

}
