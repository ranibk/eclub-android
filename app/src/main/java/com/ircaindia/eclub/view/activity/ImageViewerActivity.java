package com.ircaindia.eclub.view.activity;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ImageView;

import com.ircaindia.eclub.R;

import java.util.Objects;

public class ImageViewerActivity extends AppCompatActivity {

    ImageView pictureImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_viewer);
        try {
            Objects.requireNonNull(this.getSupportActionBar()).setTitle("Picture");
            this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);

            pictureImageView = findViewById(R.id.image_viewPicture);

            Bitmap bitmap = getIntent().getParcelableExtra("Bitmap");
            pictureImageView.setImageBitmap(bitmap);

        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }
}
