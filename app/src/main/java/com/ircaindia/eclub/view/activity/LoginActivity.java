package com.ircaindia.eclub.view.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ircaindia.eclub.R;
import com.ircaindia.eclub.model.apis.PostData;
import com.ircaindia.eclub.model.apis.RestAPICall;
import com.ircaindia.eclub.model.dto.ClubDetails;
import com.ircaindia.eclub.model.dto.Login;
import com.ircaindia.eclub.model.dto.RememberMe;
import com.ircaindia.eclub.model.dto.SendLoginAcc;
import com.ircaindia.eclub.model.dto.SendLoginMobile;
import com.ircaindia.eclub.model.retrofit.response.ResponseModelArray;
import com.ircaindia.eclub.model.utils.ConnectionDetector;
import com.ircaindia.eclub.model.utils.Constants;
import com.ircaindia.eclub.model.utils.IMessages;
import com.ircaindia.eclub.model.utils.Messages;


import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    EditText userNameEditText, passwordEditText;
    Button loginButton;
    TextView clubNameTextView;
    ImageView clubLogoImageView, logoImageView;
    LinearLayout clubLinearLayout;
    IMessages messages;
    ConnectionDetector cd;
    public boolean isInternetPresent;
    CheckBox checkBox;
    List<ClubDetails> clubDetailsList = new ArrayList<>();
    ClubDetails clubDetails;
    Button registerButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Objects.requireNonNull(this.getSupportActionBar()).hide();

        //IDS
        userNameEditText = findViewById(R.id.edit_textUserName);
        passwordEditText = findViewById(R.id.edit_textPassword);
        loginButton = findViewById(R.id.buttonLogin);
        logoImageView = findViewById(R.id.image_viewLogo);
        clubLinearLayout = findViewById(R.id.linear_layoutClub);
        clubLogoImageView = findViewById(R.id.image_viewClubLogo);
        clubNameTextView = findViewById(R.id.text_viewClubName);
        checkBox = findViewById(R.id.checkBox);
        registerButton = findViewById(R.id.buttonRegister);

        //Obj Declare
        messages = new Messages(this);
        cd = new ConnectionDetector(LoginActivity.this);
        isInternetPresent = cd.isConnectingToInternet();
        checkBox.setText(messages.getToolbarTitle("Remember Me"));
        if (messages.getClubDetails() != null) {
            clubDetailsList = messages.getClubDetails();
            messages.saveCurrentLogin(this, null);
            messages.saveDashBoardView(null);
        } else {
            Toast.makeText(this, "Club Details are not loaded", Toast.LENGTH_SHORT).show();
            finishAffinity();
        }

        //PERMISSIONS
        if (Build.VERSION.SDK_INT >= 23) {
            this.requestPermissions();
        }

        //RememberMe
        RememberMe rememberMe = messages.getRememberMe(this);
        if (rememberMe != null) {
            if (rememberMe.isSave()) {
                userNameEditText.setText(rememberMe.getUserName());
                passwordEditText.setText(rememberMe.getPassWord());
                checkBox.setChecked(true);
                usernameChangeListener(userNameEditText.getText().toString().trim());

            }
        }

        //OnClicks
        loginButton.setOnClickListener(this);
        registerButton.setOnClickListener(this);


        //onTxtChange
        userNameEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence s, int sa, int i1, int i2) {

            }

            @SuppressLint("SetTextI18n")
            @Override
            public void afterTextChanged(Editable s) {
                usernameChangeListener(s.toString());
            }
        });


    }


    private void usernameChangeListener(String s) {
        if (s.contains(".") && s.length() >= 2) {
            clubLinearLayout.setVisibility(View.VISIBLE);
            logoImageView.setVisibility(View.GONE);
            String[] clubCode = s.split("\\.");
            for (int i = 0; i < clubDetailsList.size(); i++) {
                if (clubDetailsList.get(i).getClubCode().toLowerCase().trim().equals(clubCode[0].toLowerCase().trim())) {
                    clubDetails = clubDetailsList.get(i);
                    clubNameTextView.setText(messages.getCustomFontString(clubDetails.getClubName(), "SirinStencil-Regular.ttf"));
                    messages.loadImagePicasso(clubLogoImageView, clubDetails.getImageURL(), getResources().getDrawable(R.mipmap.ic_launcher));
                    return;
                }
            }
        } else {
            clubLinearLayout.setVisibility(View.GONE);
            logoImageView.setVisibility(View.VISIBLE);
            clubNameTextView.setText(messages.getCustomFontString("Club Name", "SirinStencil-Regular.ttf"));
            clubLogoImageView.setImageDrawable(getResources().getDrawable(R.mipmap.ic_launcher));

        }
    }

    @TargetApi(23)
    private void requestPermissions() {
        try {
            int permissionCheck0 = ContextCompat.checkSelfPermission(LoginActivity.this, ACCESS_FINE_LOCATION);
            int permissionCheck2 = ContextCompat.checkSelfPermission(LoginActivity.this, Manifest.permission.ACCESS_NETWORK_STATE);
            int permissionCheck3 = ContextCompat.checkSelfPermission(LoginActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
            int permissionCheck4 = ContextCompat.checkSelfPermission(LoginActivity.this, Manifest.permission.READ_SYNC_SETTINGS);
            int permissionCheck5 = ContextCompat.checkSelfPermission(LoginActivity.this, Manifest.permission.WRITE_SYNC_SETTINGS);
            int permissionCheck6 = ContextCompat.checkSelfPermission(LoginActivity.this, Manifest.permission.CAMERA);
            int permissionCheck7 = ContextCompat.checkSelfPermission(LoginActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE);
            int permissionCheck8 = ContextCompat.checkSelfPermission(LoginActivity.this, Manifest.permission.READ_PHONE_STATE);
            int permissionCheck9 = ContextCompat.checkSelfPermission(LoginActivity.this, Manifest.permission.CALL_PHONE);

            if (permissionCheck0 == -1 || permissionCheck2 == -1 || permissionCheck3 == -1 || permissionCheck4 == -1 || permissionCheck5 == -1 || permissionCheck6 == -1 || permissionCheck7 == -1 || permissionCheck8 == -1 || permissionCheck9 == -1) {
                requestPermissionsAlert();
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    private void requestPermissionsAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.need_storage_permission));
        builder.setMessage(getString(R.string.app_needs_permission));
        builder.setCancelable(false);
        builder.setPositiveButton(getString(R.string.grant), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ActivityCompat.requestPermissions(LoginActivity.this, new String[]{ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_NETWORK_STATE, Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_SYNC_SETTINGS, Manifest.permission.WRITE_SYNC_SETTINGS, Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.READ_PHONE_STATE, Manifest.permission.CALL_PHONE}, 225);
                dialog.cancel();
            }
        });
        builder.show();
    }

    @TargetApi(23)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        boolean isPermissionGranted = true;
        for (int p : grantResults) {
            if (p == -1) {
                isPermissionGranted = false;
                break;
            }
        }
        if (!isPermissionGranted) {
            requestPermissionsAlert();
        }

    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.buttonLogin:
                messages.hideKeyBoard(LoginActivity.this, loginButton);
                if (validations()) {
                    if (userNameEditText.getText().toString().trim().contains(".")) {
                        methodAccLogin();
                    } else {
                        methodMobileLogin();
                    }
                }
                break;
            case R.id.buttonRegister:
                startActivity(new Intent(LoginActivity.this, SignupActivity.class));
                break;
        }
    }


    //validations
    private boolean validations() {
        if (userNameEditText.getText().toString().trim().isEmpty()) {
            Toast.makeText(this, "Please enter username", Toast.LENGTH_SHORT).show();
            return false;
        } else if (passwordEditText.getText().toString().trim().isEmpty()) {
            Toast.makeText(this, "Please enter password", Toast.LENGTH_SHORT).show();
            return false;
        } else {
            if (userNameEditText.getText().toString().trim().contains(".")) {
                if (clubDetails == null) {
                    Toast.makeText(this, "Club details not found", Toast.LENGTH_SHORT).show();
                    return false;
                } else if (clubDetails.getURL() == null) {
                    Toast.makeText(this, "Club URL not found", Toast.LENGTH_SHORT).show();
                    return false;
                } else if (clubDetails.getURL().isEmpty()) {
                    Toast.makeText(this, "Club URL not found", Toast.LENGTH_SHORT).show();
                    return false;
                }
            } else {
                if (userNameEditText.getText().toString().trim().length() != 10) {
                    Toast.makeText(this, "Please enter valid username", Toast.LENGTH_SHORT).show();
                    return false;
                }
            }
        }
        return true;
    }

    //Login with mobile
    @SuppressLint("HardwareIds")
    public void methodMobileLogin() {
        try {
            if (messages.isInternetAvailable()) {
                PostData postData = new PostData();
                postData.setInterface("RestAPI");
                postData.setMethod("eclub_ICMSLogin");
                SendLoginMobile sendLoginMobile = new SendLoginMobile();
                sendLoginMobile.setClubId("0");
                sendLoginMobile.setUserName(userNameEditText.getText().toString().trim());
                sendLoginMobile.setPassword(passwordEditText.getText().toString().trim());
                postData.setParameters(sendLoginMobile);
                callApiMobileLogin(postData);
            } else {
                messages.toastInternet(LoginActivity.this, loginButton);
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }

    }


    private void callApiMobileLogin(PostData postData) {
        try {
            RestAPICall restApi = messages.getAPIService(Constants.API_URL);
            messages.showProgressDialog(LoginActivity.this, "", "Loading...");
            restApi.loginWithMobileNumber(postData).enqueue(new Callback<ResponseModelArray<ClubDetails>>() {
                @Override
                public void onResponse(@NonNull Call<ResponseModelArray<ClubDetails>> call, @NonNull Response<ResponseModelArray<ClubDetails>> response) {
                    messages.hideDialog();
                    try {
                        if (response.isSuccessful()) {
                            if (response.body() != null) {
                                if (response.body().isStatus()) {
//                                    ClubDetails clubDetails = response.body().getData().get(0);
//                                    messages.saveClubDetails(clubDetailsList);
//                                    Intent i = new Intent(LoginActivity.this, LoginActivity.class);
//                                    i.putExtra("mode", 0);
//                                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                                    startActivity(i);
//                                    finish();

                                } else {
                                    messages.toast(LoginActivity.this, "Unable to login, Try again");
                                }
                            } else {
                                messages.toast(LoginActivity.this, "Unable to login, Try again");
                            }
                        } else {
                            if (response.code() == 500) {
                                messages.toast(LoginActivity.this, response.message());
                            } else if (response.code() == 404) {
                                messages.toast(LoginActivity.this, "Internet connectivity issue could not connect to server");
                            } else {
                                assert response.errorBody() != null;
                                messages.toast(LoginActivity.this, response.errorBody().string());

                            }
                        }
                    } catch (Throwable e) {
                        e.printStackTrace();
                        messages.toast(LoginActivity.this, e.getMessage());
                    }
                }

                @Override
                public void onFailure(@NonNull Call<ResponseModelArray<ClubDetails>> call, @NonNull Throwable t) {
                    try {
                        messages.hideDialog();
                        if (Objects.requireNonNull(t.getMessage()).contains(getResources().getString(R.string.network_failed))) {
                            messages.toastInternet(LoginActivity.this, loginButton);
                        } else {
                            messages.toast(LoginActivity.this, t.getMessage());
                        }
                        Log.d(Constants.TAG, t.getMessage());
                    } catch (Throwable e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (Throwable e) {
            e.printStackTrace();
        }

    }

    //Login with ACC
    @SuppressLint("HardwareIds")
    public void methodAccLogin() {
        try {
            if (messages.isInternetAvailable()) {
                PostData postData = new PostData();
                postData.setInterface("RestAPI");
                postData.setMethod("MemberAuthentication");
                SendLoginAcc sendLoginAcc = new SendLoginAcc();
                if (clubDetails.getClubCode().trim().equals("kgc") && !userNameEditText.getText().toString().trim().contains(" 01")) {
                    sendLoginAcc.setUserName((userNameEditText.getText().toString().trim() + " 01").split("\\.")[1]);
                } else {
                    sendLoginAcc.setUserName(userNameEditText.getText().toString().trim().split("\\.")[1]);
                }
                sendLoginAcc.setPassword(passwordEditText.getText().toString().trim());
                postData.setParameters(sendLoginAcc);
                callApiAccNumber(postData);
            } else {
                messages.toastInternet(LoginActivity.this, loginButton);
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }

    }

    private void callApiAccNumber(PostData postData) {
        try {
            RestAPICall restApi = messages.getAPIService(messages.getFirstStringURL(clubDetails.getURL(), messages.getLastStringURL(clubDetails.getURL())));
            messages.showProgressDialog(LoginActivity.this, "", "Loading...");
            restApi.loginWithAccNumber(messages.getLastStringURL(clubDetails.getURL()), postData).enqueue(new Callback<ResponseModelArray<Login>>() {
                @Override
                public void onResponse(@NonNull Call<ResponseModelArray<Login>> call, @NonNull Response<ResponseModelArray<Login>> response) {
                    messages.hideDialog();
                    try {
                        if (response.isSuccessful()) {
                            if (response.body() != null) {
                                if (response.body().isStatus()) {
                                    try {
                                        Login login = response.body().getData().get(0);
                                        if (login.getAuth() != null) {
                                            if (login.getAuth().trim().toLowerCase().equals("true")) {

                                                //Save login details
                                                if (clubDetails.getClubCode().trim().equals("kgc")) {
                                                    if (!login.getAccountNumber().contains(" 01")) {
                                                        login.setAccountNumber(login.getAccountNumber().concat(" 01"));
                                                    }
                                                }
                                                messages.saveCurrentLogin(LoginActivity.this, login);
                                                messages.saveDashBoardView(clubDetails);

                                                //Remember ME
                                                RememberMe rememberMe = new RememberMe();
                                                if (clubDetails.getClubCode().trim().equals("kgc") && !userNameEditText.getText().toString().trim().contains(" 01")) {
                                                    rememberMe.setUserName(userNameEditText.getText().toString().trim() + " 01");
                                                } else {
                                                    rememberMe.setUserName(userNameEditText.getText().toString().trim());
                                                }
                                                rememberMe.setPassWord(passwordEditText.getText().toString().trim());
                                                if (checkBox.isChecked()) {
                                                    rememberMe.setSave(true);
                                                } else {
                                                    rememberMe.setSave(false);
                                                }
                                                messages.rememberMe(LoginActivity.this, rememberMe);

                                                //Intents
                                                if (login.getLoginType() != null) {
                                                    if (login.getLoginType().toLowerCase().equals("st")) {
                                                        //INTENT TO SPORTS TRAINER LOGIN
                                                    } else if (login.getLoginType().toLowerCase().equals("admin")) {
                                                        //INTENT TO TEE ADMIN LOGIN
                                                    } else if (login.getLoginType().toLowerCase().equals("guest")) {
                                                        //INTENT TO MENU GUEST LOGIN
                                                    } else {
                                                        startActivity(new Intent(LoginActivity.this, DashBoardActivity.class));
                                                    }

                                                } else {
                                                    startActivity(new Intent(LoginActivity.this, DashBoardActivity.class));
                                                }


                                            } else {
                                                Toast.makeText(LoginActivity.this, "Invalid login details", Toast.LENGTH_SHORT).show();
                                            }
                                        } else {
                                            Toast.makeText(LoginActivity.this, "Invalid login details", Toast.LENGTH_SHORT).show();
                                        }
                                    } catch (Throwable e) {
                                        e.printStackTrace();
                                        messages.toast(LoginActivity.this, "Unable to login, Try again");
                                    }
                                } else {
                                    messages.toast(LoginActivity.this, "Unable to login, Try again");
                                }
                            } else {
                                messages.toast(LoginActivity.this, "Unable to login, Try again");
                            }
                        } else {
                            if (response.code() == 500) {
                                messages.toast(LoginActivity.this, response.message());
                            } else if (response.code() == 404) {
                                messages.toast(LoginActivity.this, "Internet connectivity issue could not connect to server");
                            } else {
                                assert response.errorBody() != null;
                                messages.toast(LoginActivity.this, response.errorBody().string());
                            }
                        }
                    } catch (Throwable e) {
                        e.printStackTrace();
                        messages.toast(LoginActivity.this, e.getMessage());
                    }
                }

                @Override
                public void onFailure
                        (@NonNull Call<ResponseModelArray<Login>> call, @NonNull Throwable t) {
                    try {
                        messages.hideDialog();
                        if (Objects.requireNonNull(t.getMessage()).contains(getResources().getString(R.string.network_failed))) {
                            messages.toastInternet(LoginActivity.this, loginButton);
                        } else {
                            messages.toast(LoginActivity.this, t.getMessage());
                        }
                        Log.d(Constants.TAG, t.getMessage());
                    } catch (Throwable e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (Throwable e) {
            e.printStackTrace();
        }

    }

}
