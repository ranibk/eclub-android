package com.ircaindia.eclub.view.activity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.azoft.carousellayoutmanager.CarouselLayoutManager;
import com.azoft.carousellayoutmanager.CarouselZoomPostLayoutListener;
import com.azoft.carousellayoutmanager.CenterScrollListener;
import com.ircaindia.eclub.R;
import com.ircaindia.eclub.model.adapter.DependentAdapter;
import com.ircaindia.eclub.model.apis.PostData;
import com.ircaindia.eclub.model.apis.RestAPICall;
import com.ircaindia.eclub.model.dto.ClubDetails;
import com.ircaindia.eclub.model.dto.Dependent;
import com.ircaindia.eclub.model.dto.Login;
import com.ircaindia.eclub.model.dto.Profile;
import com.ircaindia.eclub.model.dto.SendDto;
import com.ircaindia.eclub.model.retrofit.response.ResponseModelArray;
import com.ircaindia.eclub.model.sqlite.DataBaseLite;
import com.ircaindia.eclub.model.utils.Constants;
import com.ircaindia.eclub.model.utils.IMessages;
import com.ircaindia.eclub.model.utils.Messages;
import com.ircaindia.eclub.model.utils.OnRecyclerViewItemClickListener;

import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileActivity extends AppCompatActivity implements View.OnClickListener {

    IMessages messages;
    DataBaseLite dataBaseLite;
    ClubDetails clubDetails;
    Profile profile;
    Login login;

    ImageView personImageView;
    TextView nameTextView, clubNameTextView, noDependentTextView, membershipNoTextView, addressTextView, contactTextView, emailTextView, dobTextView, dojTextView;

    RecyclerView dependentRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        try {
            if (activitySetup()) {
                personImageView = findViewById(R.id.image_viewPerson);
                nameTextView = findViewById(R.id.text_viewName);
                clubNameTextView = findViewById(R.id.text_viewClubName);
                membershipNoTextView = findViewById(R.id.text_viewMembershipNo);
                addressTextView = findViewById(R.id.text_viewAddress);
                contactTextView = findViewById(R.id.text_viewContact);
                emailTextView = findViewById(R.id.text_viewEmail);
                dobTextView = findViewById(R.id.text_viewDob);
                dojTextView = findViewById(R.id.text_viewDoj);
                dependentRecyclerView = findViewById(R.id.recycler_viewDependent);
                noDependentTextView = findViewById(R.id.text_viewNoDependent);

                personImageView.setOnClickListener(this);

                showProfile(profile);

                callApiDependents();

            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    private void callApiDependents() {
        try {
            if (messages.isInternetAvailable()) {
                dependentRecyclerView.setVisibility(View.GONE);
                noDependentTextView.setVisibility(View.VISIBLE);
                PostData postData = new PostData();
                postData.setInterface("RestAPI");
                postData.setMethod("getDependantMemeberdetails");
                SendDto sendDto = new SendDto();
                sendDto.setMemberid(login.getMemberid());
                postData.setParameters(sendDto);
                RestAPICall restApi = messages.getAPIService(messages.getFirstStringURL(clubDetails.getURL(), messages.getLastStringURL(clubDetails.getURL())));
                messages.showProgressDialog(ProfileActivity.this, "", "Loading...");
                restApi.getDependents(messages.getLastStringURL(clubDetails.getURL()), postData).enqueue(new Callback<ResponseModelArray<Dependent>>() {
                    @Override
                    public void onResponse(@NonNull Call<ResponseModelArray<Dependent>> call, @NonNull Response<ResponseModelArray<Dependent>> response) {
                        messages.hideDialog();
                        try {
                            if (response.isSuccessful()) {
                                if (response.body() != null) {
                                    if (response.body().isStatus()) {
                                        try {
                                            List<Dependent> dependentList = response.body().getData();
                                            showDependents(dependentList);
                                        } catch (Throwable e) {
                                            e.printStackTrace();
                                            messages.toast(ProfileActivity.this, e.getMessage());
                                        }
                                    } else {
                                        messages.toast(ProfileActivity.this, "Dependents not found");
                                    }
                                } else {
                                    messages.toast(ProfileActivity.this, "Dependents not found");
                                }
                            } else {
                                if (response.code() == 500) {
                                    messages.toast(ProfileActivity.this, response.message());
                                } else if (response.code() == 404) {
                                    messages.toast(ProfileActivity.this, "Internet connectivity issue could not connect to server");
                                } else {
                                    assert response.errorBody() != null;
                                    messages.toast(ProfileActivity.this, response.errorBody().string());
                                }
                            }
                        } catch (Throwable e) {
                            e.printStackTrace();
                            messages.toast(ProfileActivity.this, e.getMessage());
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<ResponseModelArray<Dependent>> call, @NonNull Throwable t) {
                        try {
                            messages.hideDialog();
                            if (Objects.requireNonNull(t.getMessage()).contains(getResources().getString(R.string.network_failed))) {
                                messages.toastInternet(ProfileActivity.this, dependentRecyclerView);
                            } else {
                                messages.toast(ProfileActivity.this, t.getMessage());
                            }
                            Log.d(Constants.TAG, t.getMessage());
                        } catch (Throwable e) {
                            e.printStackTrace();
                        }
                    }
                });
            } else {
                messages.toastInternet(ProfileActivity.this, dependentRecyclerView);
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.profile_menu, menu);
        return true;
    }


    @SuppressLint("SetTextI18n")
    void showProfile(Profile profile) {
        try {
            nameTextView.setText(profile.getMembername().toUpperCase());
            emailTextView.setText(profile.getEmail());
            contactTextView.setText(profile.getMobileNo());
            clubNameTextView.setText(clubDetails.getClubName());
            addressTextView.setText(profile.getResiAddress()+profile.getCityR()+profile.getPinCodeR());
            emailTextView.setText(profile.getEmail());
            membershipNoTextView.setText(profile.getAccountNumber());
            if (!profile.getPhoto().isEmpty())
                personImageView.setImageBitmap(messages.getBitmapFromB64(profile.getPhoto()));

            dobTextView.setText(messages.getDateChange3Digit(profile.getDateOfBirth(), 2));
            dojTextView.setText(messages.getDateChange3Digit(profile.getConfirmationDate(), 2));
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.item_qrCode:
                startActivity(new Intent(ProfileActivity.this, QrCodeMemberActivity.class));
                break;
        }
        return true;
    }


    private boolean activitySetup() {
        messages = new Messages(this);
        Objects.requireNonNull(this.getSupportActionBar()).setTitle(messages.getToolbarTitle("Profile"));
        this.getSupportActionBar().setElevation(0);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        dataBaseLite = new DataBaseLite(this);
        clubDetails = messages.getDashBoardView();
        profile = messages.getProfile();
        login = messages.getCurrentLogin(this);
        if (clubDetails == null || login == null || profile == null) {
            messages.showAlert(this);
            return false;
        }
        return true;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.image_viewPerson:
                try {
                    personImageView.setDrawingCacheEnabled(true);
                    Bitmap bitmap = personImageView.getDrawingCache();
                    Intent i = new Intent(this, ImageViewerActivity.class);
                    i.putExtra("Bitmap", bitmap);
                    startActivity(i);
                } catch (Throwable e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    private void showDependents(List<Dependent> dependentList) {
        try {
            if (dependentList.size() > 0) {
                dependentRecyclerView.setVisibility(View.VISIBLE);
                noDependentTextView.setVisibility(View.GONE);

                final CarouselLayoutManager layoutManager = new CarouselLayoutManager(CarouselLayoutManager.HORIZONTAL, false);
                layoutManager.setPostLayoutListener(new CarouselZoomPostLayoutListener());
                layoutManager.setMaxVisibleItems(2);
                dependentRecyclerView.setHasFixedSize(true);
                dependentRecyclerView.setLayoutManager(layoutManager);

                DependentAdapter dependentAdapter = new DependentAdapter(dependentList, this);
                dependentRecyclerView.setAdapter(dependentAdapter);
                dependentRecyclerView.addOnScrollListener(new CenterScrollListener());
                dependentAdapter.notifyDataSetChanged();

                dependentAdapter.setOnRecyclerViewItemClickListener(new OnRecyclerViewItemClickListener<DependentAdapter.MyViewHolder, Dependent>() {
                    @Override
                    public void onRecyclerViewItemClick(@NonNull DependentAdapter.MyViewHolder myViewHolder, @NonNull View view, @NonNull Dependent dependent, int position) {
                        showDependentDetails(dependent);
                    }
                });


            } else {
                dependentRecyclerView.setVisibility(View.GONE);
                noDependentTextView.setVisibility(View.VISIBLE);
            }
        } catch (Throwable e) {
            e.printStackTrace();
            dependentRecyclerView.setVisibility(View.GONE);
            noDependentTextView.setVisibility(View.VISIBLE);
        }
    }


    @SuppressLint("SetTextI18n")
    private void showDependentDetails(final Dependent dependent) {
        try {
            final Dialog dialog = new Dialog(this);
            dialog.getWindow();
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.dialog_dependent);
            Objects.requireNonNull(dialog.getWindow()).setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

            TextView nameTextView = dialog.findViewById(R.id.text_viewName);
            TextView relationTextView = dialog.findViewById(R.id.text_viewRelation);
            TextView memberShipNoTextView = dialog.findViewById(R.id.text_viewMembershipNo);
            TextView contactTextView = dialog.findViewById(R.id.text_viewContact);
            ImageView closeImageView = dialog.findViewById(R.id.image_viewClose);
            final ImageView personImageView = dialog.findViewById(R.id.image_viewPerson);

            nameTextView.setText(dependent.getDependantName());
            relationTextView.setText(dependent.getRelation());
            contactTextView.setText(dependent.getMobileNo());
            memberShipNoTextView.setText(dependent.getDependantAccountnumber());
            if (!dependent.getPhoto().isEmpty())
                personImageView.setImageBitmap(messages.getBitmapFromB64(dependent.getPhoto()));

            dialog.show();
            closeImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
            personImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    try {
                        personImageView.setDrawingCacheEnabled(true);
                        Bitmap bitmap = personImageView.getDrawingCache();
                        Intent i = new Intent(ProfileActivity.this, ImageViewerActivity.class);
                        i.putExtra("Bitmap", bitmap);
                        startActivity(i);
                    } catch (Throwable e) {
                        e.printStackTrace();
                    }
                }
            });

        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

}
