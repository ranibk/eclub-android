package com.ircaindia.eclub.view.activity;

import android.annotation.SuppressLint;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.ircaindia.eclub.R;
import com.ircaindia.eclub.model.adapter.DashBoardAdapter;
import com.ircaindia.eclub.model.apis.PostData;
import com.ircaindia.eclub.model.apis.RestAPICall;
import com.ircaindia.eclub.model.dto.ClubDetails;
import com.ircaindia.eclub.model.dto.DashboardDto;
import com.ircaindia.eclub.model.dto.Login;
import com.ircaindia.eclub.model.dto.Profile;
import com.ircaindia.eclub.model.dto.SendDto;
import com.ircaindia.eclub.model.dto.SendProfile;
import com.ircaindia.eclub.model.retrofit.response.ResponseModel;
import com.ircaindia.eclub.model.retrofit.response.ResponseModelArray;
import com.ircaindia.eclub.model.utils.Constants;
import com.ircaindia.eclub.model.utils.IMessages;
import com.ircaindia.eclub.model.utils.Messages;
import com.ircaindia.eclub.model.utils.OnRecyclerViewItemClickListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DashBoardActivity extends AppCompatActivity implements View.OnClickListener {


    RecyclerView dashboardRecyclerView;
    List<DashboardDto> dashboardList = new ArrayList<>();
    DashBoardAdapter dashboardAdapter;
    IMessages messages;
    PieChart usagesPieChart;
    ClubDetails clubDetails;
    Login login;
    TextView emailTextView, phoneTextView, accNoTextView, nameTextView, outstandingTextView;
    ImageView profileImageView, profileViewImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dash_board);
        try {

            messages = new Messages(this);

            Objects.requireNonNull(this.getSupportActionBar()).setTitle(messages.getToolbarTitle("DashBoard"));
            this.getSupportActionBar().setElevation(0);

            dashboardRecyclerView = findViewById(R.id.recycler_viewDashboard);
            usagesPieChart = findViewById(R.id.pie_chartUsages);
            profileImageView = findViewById(R.id.image_viewPerson);
            emailTextView = findViewById(R.id.text_viewEmail);
            phoneTextView = findViewById(R.id.text_viewPhone);
            accNoTextView = findViewById(R.id.text_viewAccNo);
            profileViewImageView = findViewById(R.id.image_viewProfile);
            nameTextView = findViewById(R.id.text_viewName);
            outstandingTextView = findViewById(R.id.text_viewOutstanding);


            profileImageView.setOnClickListener(this);
            outstandingTextView.setOnClickListener(this);
            profileViewImageView.setOnClickListener(this);

            clubDetails = messages.getDashBoardView();
            login = messages.getCurrentLogin(this);
            if (clubDetails == null || login == null) {
                messages.showAlert(this);
                return;
            }


            GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 4);
            dashboardRecyclerView.setLayoutManager(gridLayoutManager);
            dashboardAdapter = new DashBoardAdapter(dashboardList, this);
            dashboardRecyclerView.setAdapter(dashboardAdapter);
            dashboardAdapter.notifyDataSetChanged();

            getData();

            setPieChart();

            methodProfile();
            methodOutstanding();


            dashboardAdapter.setOnRecyclerViewItemClickListener(new OnRecyclerViewItemClickListener<DashBoardAdapter.MyViewHolder, DashboardDto>() {
                @Override
                public void onRecyclerViewItemClick(@NonNull DashBoardAdapter.MyViewHolder myViewHolder, @NonNull View view, @NonNull DashboardDto dashboardDto, int position) {
                    switch (dashboardDto.getId()) {
                        case "11":  //Billing Info
                            startActivity(new Intent(DashBoardActivity.this, BillingDetailsActivity.class));
                            break;
                        case "12":  //Monthly Statement
                            startActivity(new Intent(DashBoardActivity.this, MonthlyStatementActivity.class));
                            break;
                        case "13":  //Receipts
                            startActivity(new Intent(DashBoardActivity.this, ReceiptsActivity.class));
                            break;
                        case "7":  //Affiliated clubs
                            startActivity(new Intent(DashBoardActivity.this, AffiliatedClubsActivity.class));
                            break;
                        case "3":
                            startActivity(new Intent(DashBoardActivity.this, ChangePasswordActivity.class));
                            break;
                        case "9":
                            try {
                                messages.shareOption();
                            } catch (Throwable e) {
                                e.printStackTrace();
                            }
                            break;
                        case "10":
                            try {
                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + getPackageName())));
                            } catch (ActivityNotFoundException e) {
                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + getPackageName())));
                            }
                            break;
                    }
                }
            });
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    public void setPieChart() {

        try {
            usagesPieChart.getDescription().setEnabled(false);
            //usagesPieChart.setDescription("");
//            usagesPieChart.setExtraOffsets(5, 5, 5, 5);
//            usagesPieChart.setDragDecelerationFrictionCoef(0.9f);
//            usagesPieChart.setTransparentCircleRadius(61f);
//            usagesPieChart.setHoleColor(Color.TRANSPARENT);
//            usagesPieChart.setHoleRadius(0f);
            usagesPieChart.animateY(1000, Easing.EasingOption.EaseInOutCubic);
            usagesPieChart.setDrawHoleEnabled(false);

//            usagesPieChart.setCenterText("Total \n" + tot);
            usagesPieChart.setDrawEntryLabels(false);
            usagesPieChart.getLegend().setEnabled(false);


//            usagesPieChart.getData().setDrawValues(false);
            ArrayList<PieEntry> yValues = new ArrayList<>();
            for (int i = 0; i < 5; i++) {
                if (i == 0) {
                    yValues.add(new PieEntry(Float.parseFloat("1222"), ""));
                } else if (i == 1) {
                    yValues.add(new PieEntry(Float.parseFloat("2022"), ""));
                } else if (i == 2) {
                    yValues.add(new PieEntry(Float.parseFloat("2312"), ""));
                } else if (i == 3) {
                    yValues.add(new PieEntry(Float.parseFloat("1230"), ""));
                } else {
                    yValues.add(new PieEntry(Float.parseFloat("3433"), ""));
                }
            }

            PieDataSet dataSet = new PieDataSet(yValues, "");
            dataSet.setSliceSpace(0f);
            dataSet.setSelectionShift(5f);
//            dataSet.setDrawValues(false);
            dataSet.setColors(ColorTemplate.COLORFUL_COLORS);
            PieData pieData = new PieData((dataSet));
            pieData.setValueTextSize(8f);
            pieData.setValueTextColor(Color.WHITE);
            usagesPieChart.setData(pieData);
            //PieChart Ends Here
        } catch (Throwable e) {
            e.printStackTrace();
        }


    }

//    @Override
//    public void onResume() {
//        try {
//            super.onResume();
//            if (dashboardShimmerFrameLayout != null)
//                dashboardShimmerFrameLayout.startShimmerAnimation();
//        } catch (Throwable e) {
//            e.printStackTrace();
//        }
//    }
//
//    @Override
//    public void onPause() {
//        try {
//            if (dashboardShimmerFrameLayout != null)
//                dashboardShimmerFrameLayout.stopShimmerAnimation();
//            super.onPause();
//        } catch (Throwable e) {
//            e.printStackTrace();
//        }
//    }

    public void getData() {

        try {
            DashboardDto dashboardDealer;

            dashboardDealer = new DashboardDto("11", "Billing Info", R.drawable.dashboard_billing_info);
            dashboardList.add(dashboardDealer);

            dashboardDealer = new DashboardDto("12", "Monthly Statement", R.drawable.dashboard_monthly_statement);
            dashboardList.add(dashboardDealer);

            dashboardDealer = new DashboardDto("13", "Receipts", R.drawable.receipts);
            dashboardList.add(dashboardDealer);

            dashboardDealer = new DashboardDto("1", "TeeTime Booking", R.drawable.dashboard_teetime_booking);
            dashboardList.add(dashboardDealer);

            dashboardDealer = new DashboardDto("2", "Sports Booking", R.drawable.dashbord_sports_booking);
            dashboardList.add(dashboardDealer);

            dashboardDealer = new DashboardDto("3", "Movie Booking", R.drawable.dashboard_movie_booking);
            dashboardList.add(dashboardDealer);

            dashboardDealer = new DashboardDto("4", "Room Booking", R.drawable.dashboard_room_booking);
            dashboardList.add(dashboardDealer);

//            dashboardDealer = new DashboardDto("5", "Party Hall Booking", R.drawable.dashboard_party_hall);
//            dashboardList.add(dashboardDealer);

            dashboardDealer = new DashboardDto("6", "Events", R.drawable.dashboard_events);
            dashboardList.add(dashboardDealer);

            dashboardDealer = new DashboardDto("7", "Affiliations", R.drawable.dashboard_affiliations);
            dashboardList.add(dashboardDealer);

            dashboardDealer = new DashboardDto("8", "Website", R.drawable.dashboard_website);
            dashboardList.add(dashboardDealer);

            dashboardDealer = new DashboardDto("9", "Share", R.drawable.dashboard_share);
            dashboardList.add(dashboardDealer);

            dashboardDealer = new DashboardDto("10", "Rate Us", R.drawable.dashboard_rate_us);
            dashboardList.add(dashboardDealer);


            dashboardAdapter.notifyDataSetChanged();


        } catch (Throwable e) {
            e.printStackTrace();
        }
    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        messages.logOut(DashBoardActivity.this);
    }

    //Profile
    @SuppressLint("HardwareIds")
    public void methodProfile() {
        try {
            if (messages.isInternetAvailable()) {
                PostData postData = new PostData();
                postData.setInterface("RestAPI");
                postData.setMethod("getMemeberdetails_profile");
                SendProfile sendProfile = new SendProfile();
                sendProfile.setAccountNo(login.getAccountNumber());
                postData.setParameters(sendProfile);
                callApiProfile(postData);
            } else {
                messages.toastInternet(DashBoardActivity.this, dashboardRecyclerView);
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    private void callApiProfile(PostData postData) {
        try {
            RestAPICall restApi = messages.getAPIService(messages.getFirstStringURL(clubDetails.getURL(), messages.getLastStringURL(clubDetails.getURL())));
            messages.showProgressDialog(DashBoardActivity.this, "", "Loading...");
            restApi.getProfileDetails(messages.getLastStringURL(clubDetails.getURL()), postData).enqueue(new Callback<ResponseModelArray<Profile>>() {
                @Override
                public void onResponse(@NonNull Call<ResponseModelArray<Profile>> call, @NonNull Response<ResponseModelArray<Profile>> response) {
                    messages.hideDialog();
                    try {
                        if (response.isSuccessful()) {
                            if (response.body() != null) {
                                if (response.body().isStatus()) {
                                    try {
                                        Profile profile = response.body().getData().get(0);
                                        messages.saveProfile(profile);
                                        showProfile(profile);
                                    } catch (Throwable e) {
                                        e.printStackTrace();
                                        messages.toast(DashBoardActivity.this, e.getMessage());
                                    }
                                } else {
                                    messages.toast(DashBoardActivity.this, "profile not found");
                                }
                            } else {
                                messages.toast(DashBoardActivity.this, "profile not found");
                            }
                        } else {
                            if (response.code() == 500) {
                                messages.toast(DashBoardActivity.this, response.message());
                            } else if (response.code() == 404) {
                                messages.toast(DashBoardActivity.this, "Internet connectivity issue could not connect to server");
                            } else {
                                assert response.errorBody() != null;
                                messages.toast(DashBoardActivity.this, response.errorBody().string());
                            }
                        }
                    } catch (Throwable e) {
                        e.printStackTrace();
                        messages.toast(DashBoardActivity.this, e.getMessage());
                    }
                }

                @Override
                public void onFailure(@NonNull Call<ResponseModelArray<Profile>> call, @NonNull Throwable t) {
                    try {
                        messages.hideDialog();
                        if (Objects.requireNonNull(t.getMessage()).contains(getResources().getString(R.string.network_failed))) {
                            messages.toastInternet(DashBoardActivity.this, dashboardRecyclerView);
                        } else {
                            messages.toast(DashBoardActivity.this, t.getMessage());
                        }
                        Log.d(Constants.TAG, t.getMessage());
                    } catch (Throwable e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    @SuppressLint("SetTextI18n")
    void showProfile(Profile profile) {
        try {
            nameTextView.setText("Welcome " + profile.getMembername().toUpperCase());
            emailTextView.setText(profile.getEmail());
            phoneTextView.setText(profile.getMobileNo());
            accNoTextView.setText(profile.getAccountNumber());
            if (!profile.getPhoto().isEmpty())
                profileImageView.setImageBitmap(messages.getBitmapFromB64(profile.getPhoto()));
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }


    //Outstanding
    @SuppressLint("HardwareIds")
    public void methodOutstanding() {
        try {
            if (messages.isInternetAvailable()) {
                PostData postData = new PostData();
                postData.setInterface("RestAPI");
                postData.setMethod("GetOpeningBalace");
                SendDto sendDto = new SendDto();
                sendDto.setMemAccNo(login.getAccountNumber());
                postData.setParameters(sendDto);
                callApiOutstanding(postData);
            } else {
                messages.toastInternet(DashBoardActivity.this, dashboardRecyclerView);
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }

    }

    private void callApiOutstanding(PostData postData) {
        try {
            RestAPICall restApi = messages.getAPIService(messages.getFirstStringURL(clubDetails.getURL(), messages.getLastStringURL(clubDetails.getURL())));
//            messages.showProgressDialog(DashBoardActivity.this, "", "Loading...");
            restApi.getOutstanding(messages.getLastStringURL(clubDetails.getURL()), postData).enqueue(new Callback<ResponseModel>() {
                @Override
                public void onResponse(@NonNull Call<ResponseModel> call, @NonNull Response<ResponseModel> response) {
//                    messages.hideDialog();
                    try {
                        if (response.isSuccessful()) {
                            if (response.body() != null) {
                                if (response.body().isStatus()) {
                                    try {
                                        String outstanding = response.body().getData();
                                        outstandingTextView.setText(outstanding);
                                    } catch (Throwable e) {
                                        e.printStackTrace();
                                        messages.toast(DashBoardActivity.this, e.getMessage());
                                    }
                                } else {
                                    messages.toast(DashBoardActivity.this, "profile not found");
                                }
                            } else {
                                messages.toast(DashBoardActivity.this, "profile not found");
                            }
                        } else {
                            if (response.code() == 500) {
                                messages.toast(DashBoardActivity.this, response.message());
                            } else if (response.code() == 404) {
                                messages.toast(DashBoardActivity.this, "Internet connectivity issue could not connect to server");
                            } else {
                                assert response.errorBody() != null;
                                messages.toast(DashBoardActivity.this, response.errorBody().string());
                            }
                        }
                    } catch (Throwable e) {
                        e.printStackTrace();
                        messages.toast(DashBoardActivity.this, e.getMessage());
                    }
                }

                @Override
                public void onFailure(@NonNull Call<ResponseModel> call, @NonNull Throwable t) {
                    try {
//                        messages.hideDialog();
                        if (Objects.requireNonNull(t.getMessage()).contains(getResources().getString(R.string.network_failed))) {
                            messages.toastInternet(DashBoardActivity.this, dashboardRecyclerView);
                        } else {
                            messages.toast(DashBoardActivity.this, t.getMessage());
                        }
                        Log.d(Constants.TAG, t.getMessage());
                    } catch (Throwable e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.image_viewPerson:
                try {
                    profileImageView.setDrawingCacheEnabled(true);
                    Bitmap bitmap = profileImageView.getDrawingCache();
                    Intent i = new Intent(this, ImageViewerActivity.class);
                    i.putExtra("Bitmap", bitmap);
                    startActivity(i);
                } catch (Throwable e) {
                    e.printStackTrace();
                }
                break;
            case R.id.text_viewOutstanding:
                methodOutstanding();
                break;
            case R.id.image_viewProfile:
                startActivity(new Intent(DashBoardActivity.this, ProfileActivity.class));
                break;
        }

    }
}
