package com.ircaindia.eclub.view.activity;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ircaindia.eclub.R;
import com.ircaindia.eclub.model.apis.PostData;
import com.ircaindia.eclub.model.apis.RestAPICall;
import com.ircaindia.eclub.model.dto.ClubDetails;
import com.ircaindia.eclub.model.dto.SendDto;
import com.ircaindia.eclub.model.dto.SignUp;
import com.ircaindia.eclub.model.retrofit.response.ResponseModelArray;
import com.ircaindia.eclub.model.sqlite.DataBaseLite;
import com.ircaindia.eclub.model.utils.Constants;
import com.ircaindia.eclub.model.utils.IMessages;
import com.ircaindia.eclub.model.utils.Messages;

import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignupActivity extends AppCompatActivity implements View.OnClickListener {


    IMessages messages;
    DataBaseLite dataBaseLite;
    ClubDetails clubDetails;


    Button submitButton, next1Button, next2Button;

    LinearLayout oneLinearLayout, twoLinearLayout, threeLinearLayout;

    EditText confirmPasswordEditText, passwordEditText, accNoEditText, clubCodeEditText, otpEditText;
    boolean isConfirm, isCurrent;
    ImageView passwordImageView, confirmPasswordImageView;
    TextView signInTextView;
    SignUp signUp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        try {
            if (activitySetup()) {
                clubCodeEditText = findViewById(R.id.edit_textClubCode);
                accNoEditText = findViewById(R.id.edit_textAccNo);
                next1Button = findViewById(R.id.buttonNext1);
                oneLinearLayout = findViewById(R.id.linear_layoutOne);

                twoLinearLayout = findViewById(R.id.linear_layout2);
                otpEditText = findViewById(R.id.edit_textOTP);
                next2Button = findViewById(R.id.buttonNext2);

                signInTextView = findViewById(R.id.text_viewSignIn);

                threeLinearLayout = findViewById(R.id.linear_layout3);
                passwordEditText = findViewById(R.id.edit_textPassword);
                passwordImageView = findViewById(R.id.image_viewPassword);
                submitButton = findViewById(R.id.buttonSubmit);
                confirmPasswordEditText = findViewById(R.id.edit_textCPassword);
                confirmPasswordImageView = findViewById(R.id.image_viewCPassword);


                next1Button.setOnClickListener(this);
                next2Button.setOnClickListener(this);
                submitButton.setOnClickListener(this);
                passwordImageView.setOnClickListener(this);
                confirmPasswordImageView.setOnClickListener(this);
                signInTextView.setOnClickListener(this);
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }


    private boolean activitySetup() {
        messages = new Messages(this);
        Objects.requireNonNull(this.getSupportActionBar()).setTitle(messages.getToolbarTitle("SignUp"));
        this.getSupportActionBar().setElevation(0);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.getSupportActionBar().hide();

        dataBaseLite = new DataBaseLite(this);

        return true;
    }

    private boolean validations1() {
        if (clubCodeEditText.getText().toString().trim().isEmpty()) {
            Toast.makeText(this, "Please enter Club code", Toast.LENGTH_SHORT).show();
            return false;
        } else if (accNoEditText.getText().toString().trim().isEmpty()) {
            Toast.makeText(this, "Please enter Membership number", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private boolean validations2() {
        if (otpEditText.getText().toString().trim().isEmpty()) {
            Toast.makeText(this, "Please enter OTP", Toast.LENGTH_SHORT).show();
            return false;
        } else if (otpEditText.getText().toString().trim().length() < 4) {
            Toast.makeText(this, "Please enter valid OTP", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private boolean validations3() {
        if (passwordEditText.getText().toString().trim().isEmpty()) {
            Toast.makeText(this, "Please enter password", Toast.LENGTH_SHORT).show();
            return false;
        } else if (confirmPasswordEditText.getText().toString().trim().isEmpty()) {
            Toast.makeText(this, "Please enter confirm password", Toast.LENGTH_SHORT).show();
            return false;
        } else if (!passwordEditText.getText().toString().trim().equals(confirmPasswordEditText.getText().toString().trim())) {
            Toast.makeText(this, "Password mismatch", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }


    private void callApiChangePassword() {
        try {
            if (messages.isInternetAvailable()) {
                PostData postData = new PostData();
                postData.setInterface("RestAPI");
                postData.setMethod("getLoginOTP");
                SendDto sendDto = new SendDto();
                sendDto.setAccno(accNoEditText.getText().toString().trim());
                postData.setParameters(sendDto);
                RestAPICall restApi = messages.getAPIService(messages.getFirstStringURL(clubDetails.getURL(), messages.getLastStringURL(clubDetails.getURL())));
                messages.showProgressDialog(SignupActivity.this, "", "Loading...");
                restApi.getSignUpOTP(messages.getLastStringURL(clubDetails.getURL()), postData).enqueue(new Callback<ResponseModelArray<SignUp>>() {
                    @Override
                    public void onResponse(@NonNull Call<ResponseModelArray<SignUp>> call, @NonNull Response<ResponseModelArray<SignUp>> response) {
                        messages.hideDialog();
                        try {
                            if (response.isSuccessful()) {
                                if (response.body() != null) {
                                    if (response.body().isStatus()) {
                                        try {
                                            SignUp signUp1 = response.body().getData().get(0);
                                            if (signUp1.getStatus().toLowerCase().equals("success")) {
                                                oneLinearLayout.setVisibility(View.GONE);
                                                twoLinearLayout.setVisibility(View.VISIBLE);
                                                threeLinearLayout.setVisibility(View.GONE);
                                                signUp = signUp1;
                                                messages.toast(SignupActivity.this, "OTP sent successfully");
                                            } else {
                                                messages.toast(SignupActivity.this, signUp1.getSMSerror());
                                            }
                                        } catch (Throwable e) {
                                            e.printStackTrace();
                                            messages.toast(SignupActivity.this, e.getMessage());
                                        }
                                    } else {
                                        messages.toast(SignupActivity.this, "failed to send otp");
                                    }
                                } else {
                                    messages.toast(SignupActivity.this, "failed to send otp");
                                }
                            } else {
                                if (response.code() == 500) {
                                    messages.toast(SignupActivity.this, response.message());
                                } else if (response.code() == 404) {
                                    messages.toast(SignupActivity.this, "Internet connectivity issue could not connect to server");
                                } else {
                                    assert response.errorBody() != null;
                                    messages.toast(SignupActivity.this, response.errorBody().string());
                                }
                            }
                        } catch (Throwable e) {
                            e.printStackTrace();
                            messages.toast(SignupActivity.this, e.getMessage());
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<ResponseModelArray<SignUp>> call, @NonNull Throwable t) {
                        try {
                            messages.hideDialog();
                            if (Objects.requireNonNull(t.getMessage()).contains(getResources().getString(R.string.network_failed))) {
                                messages.toastInternet(SignupActivity.this, next1Button);
                            } else {
                                messages.toast(SignupActivity.this, t.getMessage());
                            }
                            Log.d(Constants.TAG, t.getMessage());
                        } catch (Throwable e) {
                            e.printStackTrace();
                        }
                    }
                });
            } else {
                messages.toastInternet(SignupActivity.this, next1Button);
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.buttonNext1:
                if (validations1()) {
                    for (int i = 0; i < messages.getClubDetails().size(); i++) {
                        if (messages.getClubDetails().get(i).getClubCode().toLowerCase().equals(clubCodeEditText.getText().toString().trim().toLowerCase())) {
                            clubDetails = messages.getClubDetails().get(i);
                            break;
                        }
                    }
                    if (clubDetails != null) {
                        callApiChangePassword();
                    } else {
                        Toast.makeText(this, "Please enter valid club code", Toast.LENGTH_SHORT).show();
                    }
                }
                break;
            case R.id.text_viewSignIn:
                startActivity(new Intent(SignupActivity.this, SplashActivity.class));
                break;
            case R.id.buttonNext2:
                if (validations2()) {
                    if (signUp != null) {
                        if (otpEditText.getText().toString().trim().equals(signUp.getoTP())) {
                            messages.toast(SignupActivity.this, "OTP verified successfully");
                            oneLinearLayout.setVisibility(View.GONE);
                            threeLinearLayout.setVisibility(View.VISIBLE);
                            twoLinearLayout.setVisibility(View.GONE);
                        } else {
                            messages.toast(SignupActivity.this, "invalid OTP, Please enter valid OTP");
                        }
                    } else {
                        messages.toast(SignupActivity.this, "Unable verify OTP, Please try again");
                        oneLinearLayout.setVisibility(View.VISIBLE);
                        threeLinearLayout.setVisibility(View.GONE);
                        twoLinearLayout.setVisibility(View.GONE);
                    }
                }
                break;
            case R.id.buttonSubmit:
                if (validations3())
                break;

            case R.id.image_viewCPassword:
                if (isConfirm) {
                    confirmPasswordEditText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    isConfirm = false;
                    confirmPasswordImageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_visibility_off));
                } else {
                    confirmPasswordEditText.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    isConfirm = true;
                    confirmPasswordImageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_visibility_on));
                }
                confirmPasswordEditText.setSelection(confirmPasswordEditText.length());
                break;
            case R.id.image_viewPassword:
                if (isCurrent) {
                    passwordEditText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    passwordImageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_visibility_off));
                    isCurrent = false;
                } else {
                    passwordEditText.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    isCurrent = true;
                    passwordImageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_visibility_on));
                }
                passwordEditText.setSelection(passwordEditText.length());
                break;
        }
    }
}
