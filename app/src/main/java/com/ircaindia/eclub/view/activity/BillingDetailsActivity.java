package com.ircaindia.eclub.view.activity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.graphics.Bitmap;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ircaindia.eclub.R;
import com.ircaindia.eclub.model.adapter.BillingInfoExpandableAdapter;
import com.ircaindia.eclub.model.adapter.ReceiptInfoAdapter;
import com.ircaindia.eclub.model.apis.PostData;
import com.ircaindia.eclub.model.apis.RestAPICall;
import com.ircaindia.eclub.model.dto.BillingInfo;
import com.ircaindia.eclub.model.dto.ClubDetails;
import com.ircaindia.eclub.model.dto.Login;
import com.ircaindia.eclub.model.dto.ReceiptsInfo;
import com.ircaindia.eclub.model.dto.SendDto;
import com.ircaindia.eclub.model.retrofit.response.ResponseModelArray;
import com.ircaindia.eclub.model.sqlite.DataBaseLite;
import com.ircaindia.eclub.model.utils.Constants;
import com.ircaindia.eclub.model.utils.IMessages;
import com.ircaindia.eclub.model.utils.Messages;
import com.ircaindia.eclub.model.utils.OnRecyclerViewItemClickListener;
import com.ircaindia.eclub.model.utils.monthpicker.RackMonthPicker;
import com.ircaindia.eclub.model.utils.monthpicker.listener.DateMonthDialogListener;
import com.ircaindia.eclub.model.utils.monthpicker.listener.OnCancelMonthDialogListener;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BillingDetailsActivity extends AppCompatActivity implements View.OnClickListener {

    IMessages messages;
    DataBaseLite dataBaseLite;
    ClubDetails clubDetails;
    Login login;
    RackMonthPicker rackMonthPicker;
    boolean isBilling = true;

    LinearLayout monthLinearLayout, billingInfoLinearLayout, receiptLinearLayout;
    ExpandableListView billingInfoRecyclerView;
    RecyclerView receiptRecyclerView;
    TextView receiptInfoTextView, monthTextView, billingTextView, receiptTextView, noBillingTextView, noReceiptTextView;

    BillingInfoExpandableAdapter billingInfoExpandableAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_billing_details);
        try {
            if (activitySetup()) {
                monthLinearLayout = findViewById(R.id.linear_layoutMonth);
                monthTextView = findViewById(R.id.text_viewMonth);
                receiptInfoTextView = findViewById(R.id.text_viewReceiptInfo);
                billingTextView = findViewById(R.id.text_viewBilling);
                receiptTextView = findViewById(R.id.text_viewReceipt);
                billingInfoLinearLayout = findViewById(R.id.linear_layoutBillingInfo);
                billingInfoRecyclerView = findViewById(R.id.recycler_viewBillingInfo);
                receiptLinearLayout = findViewById(R.id.linear_layoutReceipt);
                receiptRecyclerView = findViewById(R.id.recycler_viewReceipt);
                noReceiptTextView = findViewById(R.id.text_viewNoReceipt);
                noBillingTextView = findViewById(R.id.text_viewNoBilling);

                billingTextView.setOnClickListener(this);
                receiptTextView.setOnClickListener(this);
                monthLinearLayout.setOnClickListener(this);

                calenderView();

                monthTextView.setText(messages.getCurrentMonthYear());

                callApiBillingInfo();
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    private boolean activitySetup() {
        messages = new Messages(this);
        Objects.requireNonNull(this.getSupportActionBar()).setTitle(messages.getToolbarTitle("Billing Info"));
        this.getSupportActionBar().setElevation(0);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        dataBaseLite = new DataBaseLite(this);
        clubDetails = messages.getDashBoardView();
        login = messages.getCurrentLogin(this);
        if (clubDetails == null || login == null) {
            messages.showAlert(this);
            return false;
        }
        return true;
    }

    private void callApiBillingInfo() {
        try {
            if (messages.isInternetAvailable()) {
                receiptRecyclerView.setVisibility(View.GONE);
                noReceiptTextView.setVisibility(View.VISIBLE);

                if (dataBaseLite.getBillingInfoCount() > 0) {
                    dataBaseLite.truncateBillingInfo();
                }
                PostData postData = new PostData();
                postData.setInterface("RestAPI");
                postData.setMethod("GetBilling");
                SendDto sendDto = new SendDto();
                sendDto.setAccno(login.getAccountNumber());
                sendDto.setMonth(messages.getMonthNumberFromName(monthTextView.getText().toString().trim().split(",")[0]) + "");
                sendDto.setYear(monthTextView.getText().toString().trim().split(",")[1]);
                postData.setParameters(sendDto);
                RestAPICall restApi = messages.getAPIService(messages.getFirstStringURL(clubDetails.getURL(), messages.getLastStringURL(clubDetails.getURL())));
                messages.showProgressDialog(BillingDetailsActivity.this, "", "Loading...");
                restApi.getBillingInfo(messages.getLastStringURL(clubDetails.getURL()), postData).enqueue(new Callback<ResponseModelArray<BillingInfo>>() {
                    @Override
                    public void onResponse(@NonNull Call<ResponseModelArray<BillingInfo>> call, @NonNull Response<ResponseModelArray<BillingInfo>> response) {
                        messages.hideDialog();
                        try {
                            if (response.isSuccessful()) {
                                if (response.body() != null) {
                                    if (response.body().isStatus()) {
                                        try {
                                            List<BillingInfo> billingInfoList = response.body().getData();
                                            for (int i = 0; i < billingInfoList.size(); i++) {
                                                dataBaseLite.insertBillingInfo(billingInfoList.get(i));
                                            }
                                            showBillingInfo();
                                        } catch (Throwable e) {
                                            e.printStackTrace();
                                            messages.toast(BillingDetailsActivity.this, e.getMessage());
                                        }
                                    } else {
                                        messages.toast(BillingDetailsActivity.this, "Billing Info not found");
                                    }
                                } else {
                                    messages.toast(BillingDetailsActivity.this, "Billing Info not found");
                                }
                            } else {
                                if (response.code() == 500) {
                                    messages.toast(BillingDetailsActivity.this, response.message());
                                } else if (response.code() == 404) {
                                    messages.toast(BillingDetailsActivity.this, "Internet connectivity issue could not connect to server");
                                } else {
                                    assert response.errorBody() != null;
                                    messages.toast(BillingDetailsActivity.this, response.errorBody().string());
                                }
                            }
                        } catch (Throwable e) {
                            e.printStackTrace();
                            messages.toast(BillingDetailsActivity.this, e.getMessage());
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<ResponseModelArray<BillingInfo>> call, @NonNull Throwable t) {
                        try {
                            messages.hideDialog();
                            if (Objects.requireNonNull(t.getMessage()).contains(getResources().getString(R.string.network_failed))) {
                                messages.toastInternet(BillingDetailsActivity.this, monthTextView);
                            } else {
                                messages.toast(BillingDetailsActivity.this, t.getMessage());
                            }
                            Log.d(Constants.TAG, t.getMessage());
                        } catch (Throwable e) {
                            e.printStackTrace();
                        }
                    }
                });
//                callApiReceiptInfo();
            } else {
                messages.toastInternet(BillingDetailsActivity.this, monthTextView);
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    private void callApiReceiptInfo() {
        try {
            if (messages.isInternetAvailable()) {
                if (dataBaseLite.getReceiptInfoCount() > 0) {
                    dataBaseLite.truncateReceiptInfo();
                }
                billingInfoRecyclerView.setVisibility(View.GONE);
                noBillingTextView.setVisibility(View.VISIBLE);
                PostData postData = new PostData();
                postData.setInterface("RestAPI");
                postData.setMethod("getReceiptno");
                SendDto sendDto = new SendDto();
                sendDto.setMemberId(login.getAccountNumber());
                String startDate = "", endDate = "";
                int month = messages.getMonthNumberFromName(monthTextView.getText().toString().trim().split(",")[0]);
                if (month < 10) {
                    startDate = "0" + messages.getMonthNumberFromName(monthTextView.getText().toString().trim().split(",")[0]) + "-01-" + monthTextView.getText().toString().trim().split(",")[1].trim();
                    endDate = "0" + messages.getMonthNumberFromName(monthTextView.getText().toString().trim().split(",")[0]) + "-31-" + monthTextView.getText().toString().trim().split(",")[1].trim();
                } else {
                    startDate = messages.getMonthNumberFromName(monthTextView.getText().toString().trim().split(",")[0]) + "-01-" + monthTextView.getText().toString().trim().split(",")[1].trim();
                    endDate = messages.getMonthNumberFromName(monthTextView.getText().toString().trim().split(",")[0]) + "-31-" + monthTextView.getText().toString().trim().split(",")[1].trim();
                }
                sendDto.setFdate(startDate);
                sendDto.setTodate(endDate);
                postData.setParameters(sendDto);
                RestAPICall restApi = messages.getAPIService(messages.getFirstStringURL(clubDetails.getURL(), messages.getLastStringURL(clubDetails.getURL())));
//                messages.showProgressDialog(BillingDetailsActivity.this, "", "Loading...");
                restApi.getReceiptInfo(messages.getLastStringURL(clubDetails.getURL()), postData).enqueue(new Callback<ResponseModelArray<ReceiptsInfo>>() {
                    @Override
                    public void onResponse(@NonNull Call<ResponseModelArray<ReceiptsInfo>> call, @NonNull Response<ResponseModelArray<ReceiptsInfo>> response) {
//                        messages.hideDialog();
                        try {
                            if (response.isSuccessful()) {
                                if (response.body() != null) {
                                    if (response.body().isStatus()) {
                                        try {
                                            List<ReceiptsInfo> receiptsInfoList = response.body().getData();
                                            for (int i = 0; i < receiptsInfoList.size(); i++) {
                                                dataBaseLite.insertReceiptInfo(receiptsInfoList.get(i));
                                            }
                                            showReceiptInfo();
                                        } catch (Throwable e) {
                                            e.printStackTrace();
                                            messages.toast(BillingDetailsActivity.this, e.getMessage());
                                        }
                                    } else {
                                        messages.toast(BillingDetailsActivity.this, "Receipt Info not found");
                                    }
                                } else {
                                    messages.toast(BillingDetailsActivity.this, "Receipt Info not found");
                                }
                            } else {
                                if (response.code() == 500) {
                                    messages.toast(BillingDetailsActivity.this, response.message());
                                } else if (response.code() == 404) {
                                    messages.toast(BillingDetailsActivity.this, "Internet connectivity issue could not connect to server");
                                } else {
                                    assert response.errorBody() != null;
                                    messages.toast(BillingDetailsActivity.this, response.errorBody().string());
                                }
                            }
                        } catch (Throwable e) {
                            e.printStackTrace();
                            messages.toast(BillingDetailsActivity.this, e.getMessage());
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<ResponseModelArray<ReceiptsInfo>> call, @NonNull Throwable t) {
                        try {
//                            messages.hideDialog();
                            if (Objects.requireNonNull(t.getMessage()).contains(getResources().getString(R.string.network_failed))) {
                                messages.toastInternet(BillingDetailsActivity.this, monthTextView);
                            } else {
                                messages.toast(BillingDetailsActivity.this, t.getMessage());
                            }
                            Log.d(Constants.TAG, t.getMessage());
                        } catch (Throwable e) {
                            e.printStackTrace();
                        }
                    }
                });
            } else {
                messages.toastInternet(BillingDetailsActivity.this, monthTextView);
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    private void showBillingInfo() {
        try {
            final HashMap<String, List<BillingInfo>> finalBillingInfoList = new HashMap<>();
            final List<String> dateList = dataBaseLite.getBillingDates();
            for (int i = 0; i < dateList.size(); i++) {
                List<String> billList = dataBaseLite.getBillingNumbers(dateList.get(i));
                List<BillingInfo> billingInfoListList = new ArrayList<>();
                for (int j = 0; j < billList.size(); j++) {
                    List<BillingInfo> billingInfoList = dataBaseLite.getBillingInfoList(billList.get(j));
                    int qty = 0;
                    Double rate = 0.0, amount = 0.0, billAmount = 0.0, tax = 0.0;
                    for (int k = 0; k < billingInfoList.size(); k++) {
                        qty = qty + Integer.parseInt(billingInfoList.get(k).getQuantity());
                        rate = rate + Double.parseDouble(billingInfoList.get(k).getRate());
                        amount = amount + Double.parseDouble(billingInfoList.get(k).getAmount());
                        billAmount = billAmount + Double.parseDouble(billingInfoList.get(k).getBillAmount());
                        tax = tax + Double.parseDouble(billingInfoList.get(k).getTax());
                    }
                    BillingInfo billingInfo = new BillingInfo();
                    billingInfo.setTax(messages.round(tax, 2) + "");
                    billingInfo.setRate(messages.round(rate, 2) + "");
                    billingInfo.setAmount(messages.round(amount, 2) + "");
                    billingInfo.setBillAmount(messages.round(billAmount, 2) + "");
                    billingInfo.setQuantity(qty + "");
                    billingInfo.setItemName("Total");
                    billingInfo.setBillnumber(billList.get(j));
                    billingInfoList.add(billingInfo);

                    BillingInfo billingInfo1 = new BillingInfo();
                    billingInfo1.setBillnumber(billList.get(j));
                    billingInfo1.setBilldate(dateList.get(i));
                    billingInfo1.setAmount(messages.round(amount, 2) + "");
                    billingInfo1.setTax(messages.round(tax, 2) + "");
                    billingInfo1.setBillAmount(messages.round(billAmount, 2) + "");
                    billingInfo1.setBillingInfoList(billingInfoList);
                    billingInfoListList.add(billingInfo1);
                }
                BillingInfo billingInfo = new BillingInfo();
                billingInfo.setBilldate(dateList.get(i));
                billingInfo.setBillingInfoList(billingInfoListList);
                finalBillingInfoList.put(dateList.get(i), billingInfoListList);
            }

            if (finalBillingInfoList.size() > 0) {
                billingInfoRecyclerView.setVisibility(View.VISIBLE);
                noBillingTextView.setVisibility(View.GONE);
                billingInfoExpandableAdapter = new BillingInfoExpandableAdapter(this, dateList, finalBillingInfoList);
                // setting list adapter
                billingInfoRecyclerView.setAdapter(billingInfoExpandableAdapter);
                billingInfoRecyclerView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
                    @Override
                    public boolean onGroupClick(ExpandableListView parent, View v,
                                                int groupPosition, long id) {
                        return true; // This way the expander cannot be collapsed
                    }
                });
                billingInfoRecyclerView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
                    @Override
                    public boolean onChildClick(ExpandableListView parent, View v,
                                                int groupPosition, int childPosition, long id) {
//                        List<BillingInfo> billingInfoList = finalBillingInfoList.get(dateList.get(groupPosition));

//                        List<BillingInfo> billingInfoList = finalBillingInfoList.get(finalBillingInfoList.get(groupPosition)).get(childPosition);

                        return false;
                    }
                });
            } else {
                billingInfoRecyclerView.setVisibility(View.GONE);
                noBillingTextView.setVisibility(View.VISIBLE);
            }
        } catch (Throwable e) {
            e.printStackTrace();
            billingInfoRecyclerView.setVisibility(View.GONE);
            noBillingTextView.setVisibility(View.VISIBLE);
        }
    }

    private void showReceiptInfo() {
        try {
            List<ReceiptsInfo> receiptsInfoList = dataBaseLite.getReceiptInfoList();
            if (receiptsInfoList.size() > 0) {
                receiptRecyclerView.setVisibility(View.VISIBLE);
                noReceiptTextView.setVisibility(View.GONE);
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
                receiptRecyclerView.setLayoutManager(linearLayoutManager);

//                final CarouselLayoutManager layoutManager = new CarouselLayoutManager(CarouselLayoutManager.HORIZONTAL,false);
//                layoutManager.setPostLayoutListener(new CarouselZoomPostLayoutListener());
//                layoutManager.setMaxVisibleItems(2);
//                receiptRecyclerView.setHasFixedSize(true);
//                receiptRecyclerView.setLayoutManager(layoutManager);

                ReceiptInfoAdapter receiptInfoAdapter = new ReceiptInfoAdapter(receiptsInfoList, this);
                receiptRecyclerView.setAdapter(receiptInfoAdapter);
//                receiptRecyclerView.addOnScrollListener(new CenterScrollListener());
                receiptInfoAdapter.notifyDataSetChanged();
                receiptInfoAdapter.setOnRecyclerViewItemClickListener(new OnRecyclerViewItemClickListener<ReceiptInfoAdapter.MyViewHolder, ReceiptsInfo>() {
                    @Override
                    public void onRecyclerViewItemClick(@NonNull ReceiptInfoAdapter.MyViewHolder myViewHolder, @NonNull View view, @NonNull ReceiptsInfo receiptsInfo, int position) {
                        showReceiptInfoDetails(receiptsInfo);
                    }
                });

            } else {
                receiptRecyclerView.setVisibility(View.GONE);
                noReceiptTextView.setVisibility(View.VISIBLE);
            }
        } catch (Throwable e) {
            e.printStackTrace();
            receiptRecyclerView.setVisibility(View.GONE);
            noReceiptTextView.setVisibility(View.VISIBLE);
        }
    }

    public void calenderView() {
        rackMonthPicker = new RackMonthPicker(this)
                .setLocale(Locale.ENGLISH)
                .setPositiveButton(new DateMonthDialogListener() {
                    @Override
                    public void onDateMonth(int month, int startDate, int endDate, int year, String monthLabel) {
                        System.out.println(month);
                        System.out.println(startDate);
                        System.out.println(endDate);
                        System.out.println(year);
                        System.out.println(monthLabel);
                        if (messages.isFutureDate(messages.getCurrentMonthYear(), monthLabel)) {
                            Toast.makeText(BillingDetailsActivity.this, "We can't generate the data for future date", Toast.LENGTH_SHORT).show();
                        } else {
                            monthTextView.setText(monthLabel);
                            callApiBillingInfo();
                        }


                    }
                })
                .setNegativeButton(new OnCancelMonthDialogListener() {
                    @Override
                    public void onCancel(android.support.v7.app.AlertDialog dialog) {
                        dialog.dismiss();
                    }
                });

    }


    public void setTitleName(String name) {
        Objects.requireNonNull(this.getSupportActionBar()).setTitle(messages.getToolbarTitle(name));
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.text_viewBilling:
                setTitleName("Billing Info");
                isBilling = true;
                billingTextView.setBackground(getResources().getDrawable(R.drawable.bg_left_dark_primary));
                receiptTextView.setBackground(getResources().getDrawable(R.drawable.bg_right_primary));
//                receiptInfoTextView.setVisibility(View.GONE);
                receiptLinearLayout.setVisibility(View.GONE);
                billingInfoLinearLayout.setVisibility(View.VISIBLE);
                break;
            case R.id.text_viewReceipt:
                setTitleName("Receipt Info");
                isBilling = false;
                billingTextView.setBackground(getResources().getDrawable(R.drawable.bg_left_primary));
                receiptTextView.setBackground(getResources().getDrawable(R.drawable.bg_right_dark_primary));
                receiptInfoTextView.setVisibility(View.VISIBLE);
                receiptLinearLayout.setVisibility(View.VISIBLE);
                billingInfoLinearLayout.setVisibility(View.GONE);
                break;
            case R.id.linear_layoutMonth:
                if (rackMonthPicker != null) {
                    rackMonthPicker.show();
                }
                break;
        }
    }


    @SuppressLint("SetTextI18n")
    private void showReceiptInfoDetails(final ReceiptsInfo receiptsInfo) {
        try {
            final Dialog dialog = new Dialog(this);
            dialog.getWindow();
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.dialog_receipt_info_details);
            Objects.requireNonNull(dialog.getWindow()).setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

            TextView gTotalTextVew = dialog.findViewById(R.id.text_viewGTotal);
            LinearLayout statusLinearLayout = dialog.findViewById(R.id.linear_layoutReceiptStatus);
            LinearLayout screenShotLinearLayout = dialog.findViewById(R.id.linear_layoutScreenShot);
            TextView billNoTextView = dialog.findViewById(R.id.text_viewBillNo);
            TextView dateTextView = dialog.findViewById(R.id.text_viewDate);
            TextView amountTextView = dialog.findViewById(R.id.text_viewAmount);
            TextView payStatus = dialog.findViewById(R.id.text_viewPayStatus);
            ImageView closeImageView = dialog.findViewById(R.id.image_viewClose);

            gTotalTextVew.setText(messages.getStringRupee(receiptsInfo.getAmount()));
            amountTextView.setText(messages.getStringRupee(receiptsInfo.getAmount()));
            billNoTextView.setText(receiptsInfo.getReceiptNo());
            dateTextView.setText(messages.getDateChange3Digit(receiptsInfo.getPaymentDate(),1));
            if (receiptsInfo.getPaymentMode().trim().toLowerCase().equals("onl")) {
                payStatus.setText("Successful payment via Online");
            } else {
                payStatus.setText("Successful payment via Cash");
            }
            statusLinearLayout.setBackgroundColor(this.getResources().getColor(R.color.green));
            dialog.show();
            closeImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
            screenShotLinearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {

                        if (takeScreenshot(dialog) != null) {
                            String documents = "eClub";
                            String myFolder = Environment.getExternalStorageDirectory() + "/" + documents;
                            File f = new File(myFolder);
                            if (!f.exists()) {
                                if (!f.mkdir()) {
                                    Log.d(Constants.TAG, myFolder + " can't be created.");
                                } else {
                                    Log.d(Constants.TAG, myFolder + "folder exist");
                                }
                            }
                            File myFile = new File(f,   "screenshot-"+receiptsInfo.getReceiptNo()+".png");
                            FileOutputStream fos;
                            try {
                                fos = new FileOutputStream(myFile.getAbsoluteFile());
                                takeScreenshot(dialog).compress(Bitmap.CompressFormat.JPEG, 100, fos);
                                fos.flush();
                                fos.close();
                                Toast.makeText(BillingDetailsActivity.this, "Image Saved", Toast.LENGTH_SHORT).show();
                            } catch (IOException e) {
                                Log.e(Constants.TAG, e.getMessage(), e);
                            }
                        }
                    } catch (Throwable e) {
                        e.printStackTrace();
                    }
                }
            });

        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    public Bitmap takeScreenshot(Dialog dialog) {
        try {
            View rootView = dialog.findViewById(android.R.id.content).getRootView();
            rootView.setDrawingCacheEnabled(true);
            return rootView.getDrawingCache();
        } catch (Throwable e) {
            e.printStackTrace();
            return null;
        }

    }

}
