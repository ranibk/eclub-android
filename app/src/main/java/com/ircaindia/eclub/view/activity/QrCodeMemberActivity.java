package com.ircaindia.eclub.view.activity;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.ircaindia.eclub.R;
import com.ircaindia.eclub.model.dto.ClubDetails;
import com.ircaindia.eclub.model.dto.Login;
import com.ircaindia.eclub.model.dto.Profile;
import com.ircaindia.eclub.model.sqlite.DataBaseLite;
import com.ircaindia.eclub.model.utils.IMessages;
import com.ircaindia.eclub.model.utils.Messages;

import java.util.Objects;

public class QrCodeMemberActivity extends AppCompatActivity {


    IMessages messages;
    DataBaseLite dataBaseLite;
    ClubDetails clubDetails;
    Profile profile;
    Login login;

    ImageView personImageView, qrCodeImageView;
    TextView nameTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qr_code_member);
        try {
            if (activitySetup()) {
                personImageView = findViewById(R.id.image_viewPerson);
                qrCodeImageView = findViewById(R.id.image_viewQRCode);
                nameTextView = findViewById(R.id.text_viewName);

                if (!profile.getPhoto().isEmpty()) {
                    personImageView.setImageBitmap(messages.getBitmapFromB64(profile.getPhoto()));
                }
                nameTextView.setText(profile.getMembername());
                new getQRCode().execute(loadQRCode());
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class getQRCode extends AsyncTask<String, Void, Bitmap> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            messages.showProgressDialog(QrCodeMemberActivity.this, "", "Loading...");
        }

        protected Bitmap doInBackground(String... urls) {
            String Value = urls[0];
            Bitmap bitmap = null;
            try {
                BitMatrix bitMatrix;
                try {
                    bitMatrix = new MultiFormatWriter().encode(
                            Value,
                            BarcodeFormat.QR_CODE,
                            400, 400, null
                    );

                } catch (IllegalArgumentException Illegalargumentexception) {

                    return null;
                }
                int bitMatrixWidth = bitMatrix.getWidth();

                int bitMatrixHeight = bitMatrix.getHeight();

                int[] pixels = new int[bitMatrixWidth * bitMatrixHeight];

                for (int y = 0; y < bitMatrixHeight; y++) {
                    int offset = y * bitMatrixWidth;

                    for (int x = 0; x < bitMatrixWidth; x++) {

                        pixels[offset + x] = bitMatrix.get(x, y) ?
                                getResources().getColor(R.color.black) : getResources().getColor(R.color.white);
                    }
                }
                bitmap = Bitmap.createBitmap(bitMatrixWidth, bitMatrixHeight, Bitmap.Config.ARGB_4444);

                bitmap.setPixels(pixels, 0, 400, 0, 0, bitMatrixWidth, bitMatrixHeight);
            } catch (WriterException e) {
                e.printStackTrace();
            }
            return bitmap;
        }

        protected void onPostExecute(Bitmap result) {
            try {
                messages.hideDialog();
                if (result != null) {
                    qrCodeImageView.setImageBitmap(result);
                } else {
                    Toast.makeText(QrCodeMemberActivity.this, "Not able to load QRCode", Toast.LENGTH_SHORT).show();
                }
            } catch (Throwable e) {
                e.printStackTrace();
            }
        }

    }

    private void showQRCode(String qrCode) {
        Bitmap bitmap = null;
        try {
            BitMatrix bitMatrix = null;
            try {
                bitMatrix = new MultiFormatWriter().encode(qrCode, BarcodeFormat.QR_CODE, 400, 400, null);
            } catch (Throwable throwable) {
                throwable.printStackTrace();
            }
            assert bitMatrix != null;
            int bitMatrixWidth = bitMatrix.getWidth();

            int bitMatrixHeight = bitMatrix.getHeight();

            int[] pixels = new int[bitMatrixWidth * bitMatrixHeight];

            for (int y = 0; y < bitMatrixHeight; y++) {
                int offset = y * bitMatrixWidth;

                for (int x = 0; x < bitMatrixWidth; x++) {
                    pixels[offset + x] = bitMatrix.get(x, y) ?
                            getResources().getColor(R.color.black) : getResources().getColor(R.color.white);
                }
            }
            bitmap = Bitmap.createBitmap(bitMatrixWidth, bitMatrixHeight, Bitmap.Config.ARGB_4444);

            bitmap.setPixels(pixels, 0, 400, 0, 0, bitMatrixWidth, bitMatrixHeight);

            qrCodeImageView.setImageBitmap(bitmap);
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }
    private String loadQRCode() {
        String qrCode = "";
        qrCode = "Account Number: " + profile.getAccountNumber() + "\n" + "Member Name: " + profile.getMembername() + "\n" + "Date Of Birth: " + profile.getDateOfBirth() + "\n" + "Mobile Number: " + profile.getMobileNo() + "\n" + "Email: " + profile.getEmail();
        return qrCode;
    }

    private boolean activitySetup() {
        messages = new Messages(this);
        Objects.requireNonNull(this.getSupportActionBar()).setTitle(messages.getToolbarTitle("QR Code"));
        this.getSupportActionBar().setElevation(0);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        dataBaseLite = new DataBaseLite(this);
        clubDetails = messages.getDashBoardView();
        profile = messages.getProfile();
        login = messages.getCurrentLogin(this);
        if (clubDetails == null || login == null || profile == null) {
            messages.showAlert(this);
            return false;
        }
        return true;
    }

}
