package com.ircaindia.eclub.model.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ircaindia.eclub.R;
import com.ircaindia.eclub.model.dto.AffiliatedClubs;
import com.ircaindia.eclub.model.utils.IMessages;
import com.ircaindia.eclub.model.utils.Messages;
import com.ircaindia.eclub.model.utils.OnRecyclerViewItemClickListener;

import java.util.List;

public class AffiliatedClubsAdapter extends RecyclerView.Adapter<AffiliatedClubsAdapter.MyViewHolder> {

    private Context context;
    private List<AffiliatedClubs> AffiliatedClubsList;
    private IMessages messages;
    private OnRecyclerViewItemClickListener<AffiliatedClubsAdapter.MyViewHolder, AffiliatedClubs> onRecyclerViewItemClickListener;

    public AffiliatedClubsAdapter(List<AffiliatedClubs> stringList, Context context) {
        this.AffiliatedClubsList = stringList;
        this.context = context;
        messages = new Messages(context);
    }


    public void filterList(List<AffiliatedClubs> dataList, Context context) {
        this.AffiliatedClubsList = dataList;
        this.context = context;
        messages = new Messages(context);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public AffiliatedClubsAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = null;
        try {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_affiliated_clubs_item, parent, false);
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return new AffiliatedClubsAdapter.MyViewHolder(view);
    }

    public void setOnRecyclerViewItemClickListener(@NonNull OnRecyclerViewItemClickListener<AffiliatedClubsAdapter.MyViewHolder, AffiliatedClubs> onRecyclerViewItemClickListener) {
        this.onRecyclerViewItemClickListener = onRecyclerViewItemClickListener;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull AffiliatedClubsAdapter.MyViewHolder holder, int position) {
        try {
            AffiliatedClubs affiliatedClubs = AffiliatedClubsList.get(position);

            holder.alphaTextView.setText(affiliatedClubs.getAlpha());
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
            holder.listRecyclerView.setLayoutManager(linearLayoutManager);
            AffiliatedClubListAdapter affiliatedClubListAdapter = new AffiliatedClubListAdapter(affiliatedClubs.getAffiliatedClubsList(), context);
            holder.listRecyclerView.setAdapter(affiliatedClubListAdapter);
            affiliatedClubListAdapter.notifyDataSetChanged();


        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        try {
            return AffiliatedClubsList.size();
        } catch (Throwable e) {
            e.printStackTrace();
            return 0;
        }
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView alphaTextView;
        RecyclerView listRecyclerView;

        MyViewHolder(View itemView) {
            super(itemView);
            try {
                alphaTextView = itemView.findViewById(R.id.text_viewAlpha);
                listRecyclerView = itemView.findViewById(R.id.recycler_viewList);
            } catch (Throwable t) {
                t.printStackTrace();
            }
        }


        @Override
        public void onClick(View view) {
            try {
                if (onRecyclerViewItemClickListener != null)
                    onRecyclerViewItemClickListener.onRecyclerViewItemClick(this, view, AffiliatedClubsList.get(getAdapterPosition()), getAdapterPosition());
            } catch (Throwable e) {
                e.printStackTrace();
            }
        }
    }
}




