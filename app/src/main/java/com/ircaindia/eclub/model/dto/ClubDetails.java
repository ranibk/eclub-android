package com.ircaindia.eclub.model.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ClubDetails implements Serializable {

    @SerializedName("Id")
    @Expose
    private String id;
    @SerializedName("ClubCode")
    @Expose
    private String clubCode;
    @SerializedName("ClubName")
    @Expose
    private String clubName;
    @SerializedName("URL")
    @Expose
    private String uRL;
    @SerializedName("ImageURL")
    @Expose
    private String imageURL;
    @SerializedName("isPaymentAvailable")
    @Expose
    private String isPaymentAvailable;
    @SerializedName("versionId")
    @Expose
    private String versionId;
    @SerializedName("appname")
    @Expose
    private String appname;
    @SerializedName("isRoomavailable")
    @Expose
    private String isRoomavailable;
    @SerializedName("isAffiliated")
    @Expose
    private String isAffiliated;
    @SerializedName("isWebsiteAvailable")
    @Expose
    private String isWebsiteAvailable;
    @SerializedName("isProfile")
    @Expose
    private String isProfile;
    @SerializedName("isReceipt")
    @Expose
    private String isReceipt;
    @SerializedName("isAdvertisment")
    @Expose
    private String isAdvertisment;
    @SerializedName("IsTheaterBooking")
    @Expose
    private String isTheaterBooking;
    @SerializedName("IsMonthlyPrint")
    @Expose
    private String isMonthlyPrint;
    @SerializedName("State")
    @Expose
    private String state;
    @SerializedName("City")
    @Expose
    private String city;
    @SerializedName("Area")
    @Expose
    private String area;
    @SerializedName("Pincode")
    @Expose
    private String pincode;
    @SerializedName("isLoginOtp")
    @Expose
    private String isLoginOtp;
    @SerializedName("isnewroomtype")
    @Expose
    private String isNewRoomType;

    @SerializedName("paymenturl")
    @Expose
    private String paymenturl;
    @SerializedName("isteetime")
    @Expose
    private String isTeeTime;
    @SerializedName("iscoachingclass")
    @Expose
    private String isCoachingClass;
    @SerializedName("ismenureq")
    @Expose
    private String isMenu;

    @SerializedName("dasboard")
    @Expose
    private String dashBoard;

    @SerializedName("issocialmedia")
    @Expose
    private String socialMedia;

    public String getSocialMedia() {
        return socialMedia;
    }

    public void setSocialMedia(String socialMedia) {
        this.socialMedia = socialMedia;
    }

    public String getDashBoard() {
        return dashBoard;
    }

    public void setDashBoard(String dashBoard) {
        this.dashBoard = dashBoard;
    }

    public String getIsMenu() {
        return isMenu;
    }

    public void setIsMenu(String isMenu) {
        this.isMenu = isMenu;
    }

    public String getIsTeeTime() {
        return isTeeTime;
    }

    public void setIsTeeTime(String isTeeTime) {
        this.isTeeTime = isTeeTime;
    }

    public String getIsCoachingClass() {
        return isCoachingClass;
    }

    public void setIsCoachingClass(String isCoachingClass) {
        this.isCoachingClass = isCoachingClass;
    }

    public String getPaymenturl() {
        return paymenturl;
    }

    public void setPaymenturl(String paymenturl) {
        this.paymenturl = paymenturl;
    }


    public String getIsNewRoomType() {
        return isNewRoomType;
    }

    public void setIsNewRoomType(String isNewRoomType) {
        this.isNewRoomType = isNewRoomType;
    }

    public String getIsLoginOtp() {
        return isLoginOtp;
    }

    public void setIsLoginOtp(String isLoginOtp) {
        this.isLoginOtp = isLoginOtp;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getClubCode() {
        return clubCode;
    }

    public void setClubCode(String clubCode) {
        this.clubCode = clubCode;
    }

    public String getClubName() {
        return clubName;
    }

    public void setClubName(String clubName) {
        this.clubName = clubName;
    }

    public String getURL() {
        return uRL;
    }

    public void setURL(String uRL) {
        this.uRL = uRL;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public String getIsPaymentAvailable() {
        return isPaymentAvailable;
    }

    public void setIsPaymentAvailable(String isPaymentAvailable) {
        this.isPaymentAvailable = isPaymentAvailable;
    }

    public String getVersionId() {
        return versionId;
    }

    public void setVersionId(String versionId) {
        this.versionId = versionId;
    }

    public String getAppname() {
        return appname;
    }

    public void setAppname(String appname) {
        this.appname = appname;
    }

    public String getIsRoomavailable() {
        return isRoomavailable;
    }

    public void setIsRoomavailable(String isRoomavailable) {
        this.isRoomavailable = isRoomavailable;
    }

    public String getIsAffiliated() {
        return isAffiliated;
    }

    public void setIsAffiliated(String isAffiliated) {
        this.isAffiliated = isAffiliated;
    }

    public String getIsWebsiteAvailable() {
        return isWebsiteAvailable;
    }

    public void setIsWebsiteAvailable(String isWebsiteAvailable) {
        this.isWebsiteAvailable = isWebsiteAvailable;
    }

    public String getIsProfile() {
        return isProfile;
    }

    public void setIsProfile(String isProfile) {
        this.isProfile = isProfile;
    }

    public String getIsReceipt() {
        return isReceipt;
    }

    public void setIsReceipt(String isReceipt) {
        this.isReceipt = isReceipt;
    }

    public String getIsAdvertisment() {
        return isAdvertisment;
    }

    public void setIsAdvertisment(String isAdvertisment) {
        this.isAdvertisment = isAdvertisment;
    }

    public String getIsTheaterBooking() {
        return isTheaterBooking;
    }

    public void setIsTheaterBooking(String isTheaterBooking) {
        this.isTheaterBooking = isTheaterBooking;
    }

    public String getIsMonthlyPrint() {
        return isMonthlyPrint;
    }

    public void setIsMonthlyPrint(String isMonthlyPrint) {
        this.isMonthlyPrint = isMonthlyPrint;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

}

