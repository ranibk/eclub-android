package com.ircaindia.eclub.model.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SignUp implements Serializable {

    @SerializedName("AccountNumber")
    @Expose
    private String accountNumber;
    @SerializedName("MobileNo")
    @Expose
    private String mobileNo;
    @SerializedName("ID")
    @Expose
    private Integer iD;
    @SerializedName("OTP")
    @Expose
    private String oTP;
    @SerializedName("MemberName")
    @Expose
    private String memberName;
    @SerializedName("SMSerror")
    @Expose
    private String sMSerror;
    @SerializedName("status")
    @Expose
    private String status;

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public Integer getID() {
        return iD;
    }

    public void setID(Integer iD) {
        this.iD = iD;
    }

    public String getoTP() {
        return oTP;
    }

    public void setoTP(String oTP) {
        this.oTP = oTP;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getSMSerror() {
        return sMSerror;
    }

    public void setSMSerror(String sMSerror) {
        this.sMSerror = sMSerror;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
