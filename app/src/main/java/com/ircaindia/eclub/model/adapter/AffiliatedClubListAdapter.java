package com.ircaindia.eclub.model.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ircaindia.eclub.R;
import com.ircaindia.eclub.model.dto.AffiliatedClubs;
import com.ircaindia.eclub.model.utils.IMessages;
import com.ircaindia.eclub.model.utils.Messages;
import com.ircaindia.eclub.model.utils.OnRecyclerViewItemClickListener;

import java.util.List;

public class AffiliatedClubListAdapter extends RecyclerView.Adapter<AffiliatedClubListAdapter.MyViewHolder> {

    private Context context;
    private List<AffiliatedClubs> affiliatedClubList;
    private IMessages messages;
    private OnRecyclerViewItemClickListener<AffiliatedClubListAdapter.MyViewHolder, AffiliatedClubs> onRecyclerViewItemClickListener;

    public AffiliatedClubListAdapter(List<AffiliatedClubs> stringList, Context context) {
        this.affiliatedClubList = stringList;
        this.context = context;
        messages = new Messages(context);
    }

    @NonNull
    @Override
    public AffiliatedClubListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = null;
        try {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_affiliated_club_list_item, parent, false);
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return new AffiliatedClubListAdapter.MyViewHolder(view);
    }

    public void setOnRecyclerViewItemClickListener(@NonNull OnRecyclerViewItemClickListener<AffiliatedClubListAdapter.MyViewHolder, AffiliatedClubs> onRecyclerViewItemClickListener) {
        this.onRecyclerViewItemClickListener = onRecyclerViewItemClickListener;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull AffiliatedClubListAdapter.MyViewHolder holder, int position) {
        try {
            AffiliatedClubs affiliatedClubs = affiliatedClubList.get(position);

            holder.clubNameTextView.setText(affiliatedClubs.getAffiliationClubName());
            holder.numberTextView.setText(affiliatedClubs.getPhoneNumber());
            holder.addressTextView.setText(affiliatedClubs.getClubAddress() + "," + affiliatedClubs.getCity() + "," + affiliatedClubs.getState() + "," + affiliatedClubs.getCountry() + "," + affiliatedClubs.getPinCode());

        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        try {
            return affiliatedClubList.size();
        } catch (Throwable e) {
            e.printStackTrace();
            return 0;
        }
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView clubNameTextView, addressTextView, numberTextView;
        ImageView infoImageView;

        MyViewHolder(View itemView) {
            super(itemView);
            try {
                clubNameTextView = itemView.findViewById(R.id.text_viewClubName);
                addressTextView = itemView.findViewById(R.id.text_viewAddress);
                numberTextView = itemView.findViewById(R.id.text_viewNumber);
                infoImageView = itemView.findViewById(R.id.image_viewInfo);
                infoImageView.setOnClickListener(this);
            } catch (Throwable t) {
                t.printStackTrace();
            }
        }

        @Override
        public void onClick(View view) {
            try {
                AffiliatedClubs affiliatedClubs=affiliatedClubList.get(getAdapterPosition());
                String uri = "";
                if (affiliatedClubs.getLongitude().isEmpty() || affiliatedClubs.getLongitude().equals("0")) {
                     uri = "http://maps.google.co.in/maps?q=" + affiliatedClubs.getClubAddress()+","+affiliatedClubs.getCity()+","+affiliatedClubs.getState()+","+affiliatedClubs.getCountry();
                } else {
                    uri = "http://maps.google.com/maps?saddr=" + affiliatedClubs.getLattitude() + "," + affiliatedClubs.getLongitude() + "&daddr=" + affiliatedClubs.getLattitude() + "," + affiliatedClubs.getLongitude();
                }
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                context.startActivity(intent);
            } catch (Throwable e) {
                e.printStackTrace();
            }
        }
    }
}




