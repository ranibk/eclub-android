package com.ircaindia.eclub.model.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class RememberMe implements Serializable {
    @SerializedName("isSave")
    @Expose
    private boolean isSave;

    @SerializedName("user")
    @Expose
    private String userName;
    @SerializedName("pass")
    @Expose
    private String passWord;

    public boolean isSave() {
        return isSave;
    }

    public void setSave(boolean save) {
        isSave = save;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassWord() {
        return passWord;
    }

    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }
}

