package com.ircaindia.eclub.model.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.ircaindia.eclub.R;
import com.ircaindia.eclub.model.dto.Dependent;
import com.ircaindia.eclub.model.utils.IMessages;
import com.ircaindia.eclub.model.utils.Messages;
import com.ircaindia.eclub.model.utils.OnRecyclerViewItemClickListener;

import java.util.List;

public class DependentAdapter extends RecyclerView.Adapter<DependentAdapter.MyViewHolder> {

    private Context context;
    private List<Dependent> dependentList;
    private IMessages messages;
    private OnRecyclerViewItemClickListener<DependentAdapter.MyViewHolder, Dependent> onRecyclerViewItemClickListener;

    public DependentAdapter(List<Dependent> stringList, Context context) {
        this.dependentList = stringList;
        this.context = context;
        messages = new Messages(context);
    }

    @NonNull
    @Override
    public DependentAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = null;
        try {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_member_dependent_item, parent, false);
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return new DependentAdapter.MyViewHolder(view);
    }

    public void setOnRecyclerViewItemClickListener(@NonNull OnRecyclerViewItemClickListener<DependentAdapter.MyViewHolder, Dependent> onRecyclerViewItemClickListener) {
        this.onRecyclerViewItemClickListener = onRecyclerViewItemClickListener;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull DependentAdapter.MyViewHolder holder, int position) {
        try {
            Dependent dependent = dependentList.get(position);
            holder.nameTextView.setText(dependent.getDependantName());
            holder.relativeTextView.setText(dependent.getRelation());
            if (!dependent.getPhoto().isEmpty())
                holder.personImageView.setImageBitmap(messages.getBitmapFromB64(dependent.getPhoto()));

        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        try {
            return dependentList.size();
        } catch (Throwable e) {
            e.printStackTrace();
            return 0;
        }
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView nameTextView, relativeTextView;
        ImageView personImageView;
        Button moreButton;

        MyViewHolder(View itemView) {
            super(itemView);
            try {
                personImageView = itemView.findViewById(R.id.image_viewPerson);
                nameTextView = itemView.findViewById(R.id.text_viewName);
                relativeTextView = itemView.findViewById(R.id.text_viewType);
                moreButton = itemView.findViewById(R.id.buttonMore);

                moreButton.setOnClickListener(this);
            } catch (Throwable t) {
                t.printStackTrace();
            }
        }


        @Override
        public void onClick(View view) {
            try {
                if (onRecyclerViewItemClickListener != null)
                    onRecyclerViewItemClickListener.onRecyclerViewItemClick(this, view, dependentList.get(getAdapterPosition()), getAdapterPosition());
            } catch (Throwable e) {
                e.printStackTrace();
            }
        }
    }
}




