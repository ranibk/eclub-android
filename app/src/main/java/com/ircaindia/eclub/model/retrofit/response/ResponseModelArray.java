package com.ircaindia.eclub.model.retrofit.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;


public class ResponseModelArray<T> {


    @SerializedName("Value")
    @Expose
    private List<T> data = null;
    @SerializedName("Successful")
    @Expose
    private boolean status;


    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }




}
