package com.ircaindia.eclub.model.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ircaindia.eclub.R;
import com.ircaindia.eclub.model.dto.ReceiptsInfo;
import com.ircaindia.eclub.model.utils.IMessages;
import com.ircaindia.eclub.model.utils.Messages;
import com.ircaindia.eclub.model.utils.OnRecyclerViewItemClickListener;

import java.util.List;

public class ReceiptInfoAdapter extends RecyclerView.Adapter<ReceiptInfoAdapter.MyViewHolder> {

    private Context context;
    private List<ReceiptsInfo> receiptsInfoList;
    private IMessages messages;
    private OnRecyclerViewItemClickListener<ReceiptInfoAdapter.MyViewHolder, ReceiptsInfo> onRecyclerViewItemClickListener;

    public ReceiptInfoAdapter(List<ReceiptsInfo> stringList, Context context) {
        this.receiptsInfoList = stringList;
        this.context = context;
        messages = new Messages(context);
    }

    @NonNull
    @Override
    public ReceiptInfoAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = null;
        try {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_receipt_info_item, parent, false);
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return new ReceiptInfoAdapter.MyViewHolder(view);
    }

    public void setOnRecyclerViewItemClickListener(@NonNull OnRecyclerViewItemClickListener<ReceiptInfoAdapter.MyViewHolder, ReceiptsInfo> onRecyclerViewItemClickListener) {
        this.onRecyclerViewItemClickListener = onRecyclerViewItemClickListener;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ReceiptInfoAdapter.MyViewHolder holder, int position) {
        try {
            ReceiptsInfo receiptsInfo = receiptsInfoList.get(position);
            holder.billNoTextView.setText(receiptsInfo.getReceiptNo());
            holder.amountTextView.setText(messages.getStringRupee(receiptsInfo.getAmount()));
            holder.dateTextView.setText(receiptsInfo.getPaymentDate());

            holder.infoImageView.setImageDrawable(context.getResources().getDrawable(R.drawable.success));
            if (receiptsInfo.getPaymentMode().toLowerCase().trim().equals("onl")) {
                holder.payTypeImageView.setImageDrawable(context.getResources().getDrawable(R.drawable.card_payment));
            } else {
                holder.payTypeImageView.setImageDrawable(context.getResources().getDrawable(R.drawable.cash_payment));
            }

        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        try {
            return receiptsInfoList.size();
        } catch (Throwable e) {
            e.printStackTrace();
            return 0;
        }
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView billNoTextView, amountTextView, dateTextView;
        ImageView payTypeImageView, infoImageView;

        MyViewHolder(View itemView) {
            super(itemView);
            try {
                infoImageView = itemView.findViewById(R.id.image_viewInfo);
                billNoTextView = itemView.findViewById(R.id.text_viewBillNo);
                amountTextView = itemView.findViewById(R.id.text_viewAmount);
                payTypeImageView = itemView.findViewById(R.id.image_viewPayType);
                dateTextView = itemView.findViewById(R.id.text_viewDate);
                infoImageView.setOnClickListener(this);
            } catch (Throwable t) {
                t.printStackTrace();
            }
        }


        @Override
        public void onClick(View view) {
            try {
                if (onRecyclerViewItemClickListener != null)
                    onRecyclerViewItemClickListener.onRecyclerViewItemClick(this, view, receiptsInfoList.get(getAdapterPosition()), getAdapterPosition());
            } catch (Throwable e) {
                e.printStackTrace();
            }
        }
    }
}



