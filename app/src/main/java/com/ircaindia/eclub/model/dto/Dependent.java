package com.ircaindia.eclub.model.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Dependent implements Serializable {

    @SerializedName("DependantID")
    @Expose
    private String dependantID;
    @SerializedName("DependantName")
    @Expose
    private String dependantName;
    @SerializedName("DependantAccountnumber")
    @Expose
    private String dependantAccountnumber;
    @SerializedName("Relation")
    @Expose
    private String relation;
    @SerializedName("MobileNo")
    @Expose
    private String mobileNo;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("Photo")
    @Expose
    private String photo;
    @SerializedName("ConfirmationDate")
    @Expose
    private String confirmationDate;
    @SerializedName("DateOfBirth")
    @Expose
    private String dateOfBirth;
    @SerializedName("ResiAddress")
    @Expose
    private String resiAddress;
    @SerializedName("CityR")
    @Expose
    private String cityR;
    @SerializedName("PinCodeR")
    @Expose
    private String pinCodeR;
    @SerializedName("isE")
    @Expose
    private String isE;
    @SerializedName("isPE")
    @Expose
    private String isPE;

    public String getDependantID() {
        return dependantID;
    }

    public void setDependantID(String dependantID) {
        this.dependantID = dependantID;
    }

    public String getDependantName() {
        return dependantName;
    }

    public void setDependantName(String dependantName) {
        this.dependantName = dependantName;
    }

    public String getDependantAccountnumber() {
        return dependantAccountnumber;
    }

    public void setDependantAccountnumber(String dependantAccountnumber) {
        this.dependantAccountnumber = dependantAccountnumber;
    }

    public String getRelation() {
        return relation;
    }

    public void setRelation(String relation) {
        this.relation = relation;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getConfirmationDate() {
        return confirmationDate;
    }

    public void setConfirmationDate(String confirmationDate) {
        this.confirmationDate = confirmationDate;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getResiAddress() {
        return resiAddress;
    }

    public void setResiAddress(String resiAddress) {
        this.resiAddress = resiAddress;
    }

    public String getCityR() {
        return cityR;
    }

    public void setCityR(String cityR) {
        this.cityR = cityR;
    }

    public String getPinCodeR() {
        return pinCodeR;
    }

    public void setPinCodeR(String pinCodeR) {
        this.pinCodeR = pinCodeR;
    }

    public String getIsE() {
        return isE;
    }

    public void setIsE(String isE) {
        this.isE = isE;
    }

    public String getIsPE() {
        return isPE;
    }

    public void setIsPE(String isPE) {
        this.isPE = isPE;
    }

}
