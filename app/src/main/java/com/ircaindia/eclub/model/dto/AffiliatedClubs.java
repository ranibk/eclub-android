package com.ircaindia.eclub.model.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class AffiliatedClubs implements Serializable {

    @SerializedName("AffiliationClubCode")
    @Expose
    private String affiliationClubCode;
    @SerializedName("AffiliationClubName")
    @Expose
    private String affiliationClubName;
    @SerializedName("ClubAddress")
    @Expose
    private String clubAddress;
    @SerializedName("City")
    @Expose
    private String city;
    @SerializedName("PinCode")
    @Expose
    private String pinCode;
    @SerializedName("State")
    @Expose
    private String state;
    @SerializedName("Country")
    @Expose
    private String country;
    @SerializedName("PhoneNumber")
    @Expose
    private String phoneNumber;
    @SerializedName("Lattitude")
    @Expose
    private String lattitude;
    @SerializedName("Longitude")
    @Expose
    private String longitude;
    @SerializedName("alpha")
    @Expose
    private String alpha;
    @SerializedName("list")
    @Expose
    private List<AffiliatedClubs> affiliatedClubsList = new ArrayList<>();

    public String getAlpha() {
        return alpha;
    }

    public void setAlpha(String alpha) {
        this.alpha = alpha;
    }

    public List<AffiliatedClubs> getAffiliatedClubsList() {
        return affiliatedClubsList;
    }

    public void setAffiliatedClubsList(List<AffiliatedClubs> affiliatedClubsList) {
        this.affiliatedClubsList = affiliatedClubsList;
    }

    public String getAffiliationClubCode() {
        return affiliationClubCode;
    }

    public void setAffiliationClubCode(String affiliationClubCode) {
        this.affiliationClubCode = affiliationClubCode;
    }

    public String getAffiliationClubName() {
        return affiliationClubName;
    }

    public void setAffiliationClubName(String affiliationClubName) {
        this.affiliationClubName = affiliationClubName;
    }

    public String getClubAddress() {
        return clubAddress;
    }

    public void setClubAddress(String clubAddress) {
        this.clubAddress = clubAddress;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPinCode() {
        return pinCode;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getLattitude() {
        return lattitude;
    }

    public void setLattitude(String lattitude) {
        this.lattitude = lattitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }


}
