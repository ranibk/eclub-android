package com.ircaindia.eclub.model.apis;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class PostData implements Serializable {

        @SerializedName("interface")
        @Expose
        private String interfaces;
        @SerializedName("method")
        @Expose
        private String method;
        @SerializedName("parameters")
        @Expose
        private Object parameters;

        public String getInterface() {
            return interfaces;
        }

        public void setInterface(String _interface) {
            this.interfaces = _interface;
        }

        public String getMethod() {
            return method;
        }

        public void setMethod(String method) {
            this.method = method;
        }

        public Object getParameters() {
            return parameters;
        }

        public void setParameters(Object parameters) {
            this.parameters = parameters;
        }
    }


