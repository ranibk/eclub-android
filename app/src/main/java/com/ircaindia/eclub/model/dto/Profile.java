package com.ircaindia.eclub.model.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Profile implements Serializable {

    @SerializedName("AccountNumber")
    @Expose
    private String accountNumber;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("membername")
    @Expose
    private String membername;
    @SerializedName("MobileNo")
    @Expose
    private String mobileNo;
    @SerializedName("ResiAddress")
    @Expose
    private String resiAddress;
    @SerializedName("CityR")
    @Expose
    private String cityR;
    @SerializedName("PinCodeR")
    @Expose
    private String pinCodeR;
    @SerializedName("MemberID")
    @Expose
    private String memberID;
    @SerializedName("OfficeAddress")
    @Expose
    private String officeAddress;
    @SerializedName("CityO")
    @Expose
    private String cityO;
    @SerializedName("PinCodeO")
    @Expose
    private String pinCodeO;
    @SerializedName("DateOfBirth")
    @Expose
    private String dateOfBirth;
    @SerializedName("ConfirmationDate")
    @Expose
    private String confirmationDate;
    @SerializedName("MemberType")
    @Expose
    private String memberType;
    @SerializedName("Photo")
    @Expose
    private String photo;
    @SerializedName("IsDirectEdit")
    @Expose
    private String isDirectEdit;
    @SerializedName("isE")
    @Expose
    private String isE;
    @SerializedName("isPE")
    @Expose
    private String isPE;

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMembername() {
        return membername;
    }

    public void setMembername(String membername) {
        this.membername = membername;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getResiAddress() {
        return resiAddress;
    }

    public void setResiAddress(String resiAddress) {
        this.resiAddress = resiAddress;
    }

    public String getCityR() {
        return cityR;
    }

    public void setCityR(String cityR) {
        this.cityR = cityR;
    }

    public String getPinCodeR() {
        return pinCodeR;
    }

    public void setPinCodeR(String pinCodeR) {
        this.pinCodeR = pinCodeR;
    }

    public String getMemberID() {
        return memberID;
    }

    public void setMemberID(String memberID) {
        this.memberID = memberID;
    }

    public String getOfficeAddress() {
        return officeAddress;
    }

    public void setOfficeAddress(String officeAddress) {
        this.officeAddress = officeAddress;
    }

    public String getCityO() {
        return cityO;
    }

    public void setCityO(String cityO) {
        this.cityO = cityO;
    }

    public String getPinCodeO() {
        return pinCodeO;
    }

    public void setPinCodeO(String pinCodeO) {
        this.pinCodeO = pinCodeO;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getConfirmationDate() {
        return confirmationDate;
    }

    public void setConfirmationDate(String confirmationDate) {
        this.confirmationDate = confirmationDate;
    }

    public String getMemberType() {
        return memberType;
    }

    public void setMemberType(String memberType) {
        this.memberType = memberType;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getIsDirectEdit() {
        return isDirectEdit;
    }

    public void setIsDirectEdit(String isDirectEdit) {
        this.isDirectEdit = isDirectEdit;
    }

    public String getIsE() {
        return isE;
    }

    public void setIsE(String isE) {
        this.isE = isE;
    }

    public String getIsPE() {
        return isPE;
    }

    public void setIsPE(String isPE) {
        this.isPE = isPE;
    }


}
