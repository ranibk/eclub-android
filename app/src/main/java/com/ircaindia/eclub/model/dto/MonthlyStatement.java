package com.ircaindia.eclub.model.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class MonthlyStatement implements Serializable {

    @SerializedName("tdate")
    @Expose
    private String tdate;
    @SerializedName("baramount")
    @Expose
    private String baramount;
    @SerializedName("kamount")
    @Expose
    private String kamount;
    @SerializedName("ramount")
    @Expose
    private String ramount;
    @SerializedName("Particulars")
    @Expose
    private String particulars;
    @SerializedName("Amount")
    @Expose
    private String amount;
    @SerializedName("Type")
    @Expose
    private String type;
    @SerializedName("MemberName")
    @Expose
    private String memberName;
    @SerializedName("MemebrType")
    @Expose
    private String memebrType;
    @SerializedName("ACCno")
    @Expose
    private String aCCno;
    @SerializedName("BillNo")
    @Expose
    private String billNo;
    @SerializedName("itemcatname")
    @Expose
    private String itemcatname;

    public String getTdate() {
        return tdate;
    }

    public void setTdate(String tdate) {
        this.tdate = tdate;
    }

    public String getBaramount() {
        return baramount;
    }

    public void setBaramount(String baramount) {
        this.baramount = baramount;
    }

    public String getKamount() {
        return kamount;
    }

    public void setKamount(String kamount) {
        this.kamount = kamount;
    }

    public String getRamount() {
        return ramount;
    }

    public void setRamount(String ramount) {
        this.ramount = ramount;
    }

    public String getParticulars() {
        return particulars;
    }

    public void setParticulars(String particulars) {
        this.particulars = particulars;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getMemebrType() {
        return memebrType;
    }

    public void setMemebrType(String memebrType) {
        this.memebrType = memebrType;
    }

    public String getACCno() {
        return aCCno;
    }

    public void setACCno(String aCCno) {
        this.aCCno = aCCno;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getItemcatname() {
        return itemcatname;
    }

    public void setItemcatname(String itemcatname) {
        this.itemcatname = itemcatname;
    }


}
