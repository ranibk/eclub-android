package com.ircaindia.eclub.model.core;

import android.content.Context;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

import com.ircaindia.eclub.model.sqlite.DataManagers;

public class MyApplication extends MultiDexApplication {

    private static MyApplication context;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        context = this;
        FontsOverride.setDefaultFont(this, "DEFAULT", "fonts/SourceSansPro-Semibold.ttf");
        FontsOverride.setDefaultFont(this, "MONOSPACE", "fonts/SourceSansPro-Semibold.ttf");
        FontsOverride.setDefaultFont(this, "SERIF", "fonts/SourceSansPro-Semibold.ttf");
        FontsOverride.setDefaultFont(this, "SANS_SERIF", "fonts/SourceSansPro-Semibold.ttf");
//      FontsOverride.setDefaultFont(this, "DEFAULT", "fonts/ElMessiri-SemiBold.ttf");
//        FontsOverride.setDefaultFont(this, "MONOSPACE", "fonts/ElMessiri-SemiBold.ttf");
//        FontsOverride.setDefaultFont(this, "SERIF", "fonts/ElMessiri-SemiBold.ttf");
//        FontsOverride.setDefaultFont(this, "SANS_SERIF", "fonts/ElMessiri-SemiBold.ttf");
    }
    public static void setFCMToken(String fcmToken) {
        DataManagers.saveToPrefs(context, "FCM_TOKEN", fcmToken);
    }

    public static String getFCMToken() {
        return DataManagers.getFromPrefs(context, "FCM_TOKEN", "");
    }
}
