package com.ircaindia.eclub.model.fcm;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Notification {
    @SerializedName("notification_type")
    @Expose
    private String notificationType;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("user_name")
    @Expose
    private String userName;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("mobile")
    @Expose
    private String mobile;
    @SerializedName("profile_picture")
    @Expose
    private String profilePicture;
    @SerializedName("duels_id")
    @Expose
    private String duelsId;
    @SerializedName("duel_contribution")
    @Expose
    private String duelContribution;
    @SerializedName("match_id")
    @Expose
    private String matchId;
    @SerializedName("cricket_team_one")
    @Expose
    private String cricketTeamOne;
    @SerializedName("cricket_team_two")
    @Expose
    private String cricketTeamTwo;
    @SerializedName("cricket_team_one_short_name")
    @Expose
    private String cricketTeamOneShortName;
    @SerializedName("cricket_team_two_short_name")
    @Expose
    private String cricketTeamTwoShortName;
    @SerializedName("match_time")
    @Expose
    private String matchTime;
    @SerializedName("match_format")
    @Expose
    private String matchFormat;
    @SerializedName("types")
    @Expose
    private List<String> types = null;
    @SerializedName("group_contribution")
    @Expose
    private String groupContribution;
    @SerializedName("group_name")
    @Expose
    private String groupName;
    @SerializedName("group_id")
    @Expose
    private String groupId;
    @SerializedName("credits_type")
    @Expose
    private String creditsType;

    public String getNotificationType() {
        return notificationType;
    }

    public void setNotificationType(String notificationType) {
        this.notificationType = notificationType;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }

    public String getDuelsId() {
        return duelsId;
    }

    public void setDuelsId(String duelsId) {
        this.duelsId = duelsId;
    }

    public String getDuelContribution() {
        return duelContribution;
    }

    public void setDuelContribution(String duelContribution) {
        this.duelContribution = duelContribution;
    }

    public String getMatchId() {
        return matchId;
    }

    public void setMatchId(String matchId) {
        this.matchId = matchId;
    }

    public String getCricketTeamOne() {
        return cricketTeamOne;
    }

    public void setCricketTeamOne(String cricketTeamOne) {
        this.cricketTeamOne = cricketTeamOne;
    }

    public String getCricketTeamTwo() {
        return cricketTeamTwo;
    }

    public void setCricketTeamTwo(String cricketTeamTwo) {
        this.cricketTeamTwo = cricketTeamTwo;
    }

    public String getMatchTime() {
        return matchTime;
    }

    public void setMatchTime(String matchTime) {
        this.matchTime = matchTime;
    }

    public String getMatchFormat() {
        return matchFormat;
    }

    public void setMatchFormat(String matchFormat) {
        this.matchFormat = matchFormat;
    }

    public List<String> getTypes() {
        return types;
    }

    public void setTypes(List<String> types) {
        this.types = types;
    }

    public String getGroupContribution() {
        return groupContribution;
    }

    public void setGroupContribution(String groupContribution) {
        this.groupContribution = groupContribution;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }


    public String getCreditsType() {
        return creditsType;
    }

    public void setCreditsType(String creditsType) {
        this.creditsType = creditsType;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getCricketTeamOneShortName() {
        return cricketTeamOneShortName;
    }

    public void setCricketTeamOneShortName(String cricketTeamOneShortName) {
        this.cricketTeamOneShortName = cricketTeamOneShortName;
    }

    public String getCricketTeamTwoShortName() {
        return cricketTeamTwoShortName;
    }

    public void setCricketTeamTwoShortName(String cricketTeamTwoShortName) {
        this.cricketTeamTwoShortName = cricketTeamTwoShortName;
    }
}
