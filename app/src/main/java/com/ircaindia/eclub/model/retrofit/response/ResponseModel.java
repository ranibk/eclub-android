package com.ircaindia.eclub.model.retrofit.response;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResponseModel {
    @SerializedName("Value")
    @Expose
    private String data = null;
    @SerializedName("Successful")
    @Expose
    private boolean status;


    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}
