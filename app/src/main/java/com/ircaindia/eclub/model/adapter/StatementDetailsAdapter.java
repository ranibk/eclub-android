package com.ircaindia.eclub.model.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ircaindia.eclub.R;
import com.ircaindia.eclub.model.dto.MonthlyStatement;
import com.ircaindia.eclub.model.utils.IMessages;
import com.ircaindia.eclub.model.utils.Messages;
import com.ircaindia.eclub.model.utils.OnRecyclerViewItemClickListener;

import java.util.List;

public class StatementDetailsAdapter extends RecyclerView.Adapter<StatementDetailsAdapter.MyViewHolder> {

    private Context context;
    private List<MonthlyStatement> monthlyStatementList;
    private IMessages messages;
    private String type;
    private OnRecyclerViewItemClickListener<StatementDetailsAdapter.MyViewHolder, MonthlyStatement> onRecyclerViewItemClickListener;

    public StatementDetailsAdapter(List<MonthlyStatement> stringList, Context context, String type) {
        this.monthlyStatementList = stringList;
        this.context = context;
        this.type = type;
        messages = new Messages(context);
    }

    @NonNull
    @Override
    public StatementDetailsAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = null;
        try {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_items_statement_item, parent, false);
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return new StatementDetailsAdapter.MyViewHolder(view);
    }

    public void setOnRecyclerViewItemClickListener(@NonNull OnRecyclerViewItemClickListener<StatementDetailsAdapter.MyViewHolder, MonthlyStatement> onRecyclerViewItemClickListener) {
        this.onRecyclerViewItemClickListener = onRecyclerViewItemClickListener;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull StatementDetailsAdapter.MyViewHolder holder, int position) {
        try {
            MonthlyStatement statementMain = monthlyStatementList.get(position);
            if (type.toLowerCase().equals("type")) {
                holder.dateTextView.setText(statementMain.getParticulars());
                holder.amountTextView.setText(messages.getStringRupee(statementMain.getAmount()));
            } else {
                holder.dateTextView.setText(messages.getDateChangeBillingInfo(statementMain.getTdate()));
                holder.amountTextView.setText(messages.getStringRupee(statementMain.getAmount()));
            }

        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        try {
            return monthlyStatementList.size();
        } catch (Throwable e) {
            e.printStackTrace();
            return 0;
        }
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView dateTextView, amountTextView;

        MyViewHolder(View itemView) {
            super(itemView);
            try {
                amountTextView = itemView.findViewById(R.id.text_viewAmount);
                dateTextView = itemView.findViewById(R.id.text_viewDate);
            } catch (Throwable t) {
                t.printStackTrace();
            }
        }

        @Override
        public void onClick(View view) {
            try {
                if (onRecyclerViewItemClickListener != null)
                    onRecyclerViewItemClickListener.onRecyclerViewItemClick(this, view, monthlyStatementList.get(getAdapterPosition()), getAdapterPosition());
            } catch (Throwable e) {
                e.printStackTrace();
            }
        }
    }
}



