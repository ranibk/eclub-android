package com.ircaindia.eclub.model.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class BillingInfo implements Serializable {

    @SerializedName("billdate")
    @Expose
    private String billdate;
    @SerializedName("billnumber")
    @Expose
    private String billnumber;
    @SerializedName("billId")
    @Expose
    private String billId;
    @SerializedName("amount")
    @Expose
    private String amount;
    @SerializedName("ItemCode")
    @Expose
    private String itemCode;
    @SerializedName("ItemName")
    @Expose
    private String itemName;
    @SerializedName("Quantity")
    @Expose
    private String quantity;
    @SerializedName("Rate")
    @Expose
    private String rate;
    @SerializedName("BillAmount")
    @Expose
    private String billAmount;
    @SerializedName("Tax")
    @Expose
    private String tax;
    @SerializedName("List")
    @Expose
    private List<BillingInfo> billingInfoList = new ArrayList<>();

    public List<BillingInfo> getBillingInfoList() {
        return billingInfoList;
    }

    public void setBillingInfoList(List<BillingInfo> billingInfoList) {
        this.billingInfoList = billingInfoList;
    }

    public String getBilldate() {
        return billdate;
    }

    public void setBilldate(String billdate) {
        this.billdate = billdate;
    }

    public String getBillnumber() {
        return billnumber;
    }

    public void setBillnumber(String billnumber) {
        this.billnumber = billnumber;
    }

    public String getBillId() {
        return billId;
    }

    public void setBillId(String billId) {
        this.billId = billId;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getBillAmount() {
        return billAmount;
    }

    public void setBillAmount(String billAmount) {
        this.billAmount = billAmount;
    }

    public String getTax() {
        return tax;
    }

    public void setTax(String tax) {
        this.tax = tax;
    }


}
