package com.ircaindia.eclub.model.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ircaindia.eclub.R;
import com.ircaindia.eclub.model.dto.DashboardDto;
import com.ircaindia.eclub.model.utils.OnRecyclerViewItemClickListener;

import java.util.List;

public class DashBoardAdapter extends RecyclerView.Adapter<DashBoardAdapter.MyViewHolder> {

    private Context context;
    private List<DashboardDto> stringList;
    private OnRecyclerViewItemClickListener<MyViewHolder, DashboardDto> onRecyclerViewItemClickListener;

    public DashBoardAdapter(List<DashboardDto> stringList, Context context) {
        this.stringList = stringList;
        this.context = context;
    }

    @NonNull
    @Override
    public DashBoardAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = null;
        try {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_dashboard_view_item, parent, false);
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return new DashBoardAdapter.MyViewHolder(view);
    }
    public void setOnRecyclerViewItemClickListener(@NonNull OnRecyclerViewItemClickListener<DashBoardAdapter.MyViewHolder, DashboardDto> onRecyclerViewItemClickListener) {
        this.onRecyclerViewItemClickListener = onRecyclerViewItemClickListener;
    }
    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull DashBoardAdapter.MyViewHolder holder, int position) {
        try {
            DashboardDto dashboardDto = stringList.get(position);
            holder.titleTextView.setText(dashboardDto.getTitle());
            holder.iconImageView.setImageResource(dashboardDto.getImage());

        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        try {
            return stringList.size();
        } catch (Throwable e) {
            e.printStackTrace();
            return 0;
        }
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView titleTextView;
        ImageView iconImageView;

        MyViewHolder(View itemView) {
            super(itemView);
            try {
                iconImageView = itemView.findViewById(R.id.image_viewIcon);
                titleTextView = itemView.findViewById(R.id.text_viewTitle);
                itemView.setOnClickListener(this);
            } catch (Throwable t) {
                t.printStackTrace();
            }
        }

        //        @Override
//        public void onClick(View view) {
//            try {
//                DashboardDto dashboardDto = stringList.get(getAdapterPosition());
//                switch (dashboardDto.getId()) {
//                    case "1":
//                        Intent i1 = new Intent(context, DashboardBillingInfo.class);
//                        i1.putExtra("accNO", context.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE).getString("ac_no", "0"));
//                        i1.putExtra("Item", 0);
//                        context.startActivity(i1);
//                        break;
//                    case "2":
//                        break;
//
//                }
//            } catch (Throwable e) {
//                e.printStackTrace();
//            }
//        }
        @Override
        public void onClick(View view) {
            try {
                if (onRecyclerViewItemClickListener != null)
                    onRecyclerViewItemClickListener.onRecyclerViewItemClick(this, view, stringList.get(getAdapterPosition()), getAdapterPosition());
            } catch (Throwable e) {
                e.printStackTrace();
            }
        }
    }
}


