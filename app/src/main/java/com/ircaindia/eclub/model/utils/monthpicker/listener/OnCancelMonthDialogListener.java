package com.ircaindia.eclub.model.utils.monthpicker.listener;

import android.support.v7.app.AlertDialog;

/**
 * Created by kristiawan on 31/12/16.
 */

public interface OnCancelMonthDialogListener {
    public void onCancel(AlertDialog dialog);
}
