package com.ircaindia.eclub.model.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ircaindia.eclub.R;
import com.ircaindia.eclub.model.dto.StatementMain;
import com.ircaindia.eclub.model.utils.IMessages;
import com.ircaindia.eclub.model.utils.Messages;
import com.ircaindia.eclub.model.utils.OnRecyclerViewItemClickListener;

import java.util.List;

public class StatementAdapter extends RecyclerView.Adapter<StatementAdapter.MyViewHolder> {

    private Context context;
    private List<StatementMain> monthlyStatementList;
    private IMessages messages;
    private OnRecyclerViewItemClickListener<StatementAdapter.MyViewHolder, StatementMain> onRecyclerViewItemClickListener;

    public StatementAdapter(List<StatementMain> stringList, Context context) {
        this.monthlyStatementList = stringList;
        this.context = context;
        messages = new Messages(context);
    }

    @NonNull
    @Override
    public StatementAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = null;
        try {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_monthly_statement_item, parent, false);
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return new StatementAdapter.MyViewHolder(view);
    }

    public void setOnRecyclerViewItemClickListener(@NonNull OnRecyclerViewItemClickListener<StatementAdapter.MyViewHolder, StatementMain> onRecyclerViewItemClickListener) {
        this.onRecyclerViewItemClickListener = onRecyclerViewItemClickListener;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull StatementAdapter.MyViewHolder holder, int position) {
        try {

            StatementMain statementMain = monthlyStatementList.get(position);

            holder.nameTextView.setText(statementMain.getName());
            holder.amountTextView.setText(messages.getStringRupee(statementMain.getAmount()));
            if (statementMain.getName().toLowerCase().trim().equals("bar")) {
                holder.imageView.setImageDrawable(context.getResources().getDrawable(R.drawable.bar));
            } else if (statementMain.getName().toLowerCase().trim().equals("kitchen")) {
                holder.imageView.setImageDrawable(context.getResources().getDrawable(R.drawable.kitchen));
            } else if (statementMain.getName().toLowerCase().trim().contains("others")) {
                holder.imageView.setImageDrawable(context.getResources().getDrawable(R.drawable.others));
            } else if (statementMain.getName().toLowerCase().trim().contains("net due")) {
                holder.imageView.setImageDrawable(context.getResources().getDrawable(R.drawable.payment));
            }else if (statementMain.getName().toLowerCase().trim().contains("golf")) {
                holder.imageView.setImageDrawable(context.getResources().getDrawable(R.drawable.teetime_play_quick));
            }

        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        try {
            return monthlyStatementList.size();
        } catch (Throwable e) {
            e.printStackTrace();
            return 0;
        }
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView nameTextView, amountTextView;
        ImageView imageView, infoImageView;

        MyViewHolder(View itemView) {
            super(itemView);
            try {
                infoImageView = itemView.findViewById(R.id.image_viewInfo);
                imageView = itemView.findViewById(R.id.image_viewImage);
                nameTextView = itemView.findViewById(R.id.text_viewName);
                amountTextView = itemView.findViewById(R.id.text_viewAmount);
                infoImageView.setOnClickListener(this);
            } catch (Throwable t) {
                t.printStackTrace();
            }
        }


        @Override
        public void onClick(View view) {
            try {
                if (onRecyclerViewItemClickListener != null)
                    onRecyclerViewItemClickListener.onRecyclerViewItemClick(this, view, monthlyStatementList.get(getAdapterPosition()), getAdapterPosition());
            } catch (Throwable e) {
                e.printStackTrace();
            }
        }
    }
}




