package com.ircaindia.eclub.model.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SendDto implements Serializable {

    @SerializedName("memAccNo")
    @Expose
    private String memAccNo;
    @SerializedName("accno")
    @Expose
    private String accno;
    @SerializedName("memberId")
    @Expose
    private String memberId;
    @SerializedName("accountno")
    @Expose
    private String accountno;
    @SerializedName("newpwd")
    @Expose
    private String newpwd;
    @SerializedName("month")
    @Expose
    private String month;
    @SerializedName("year")
    @Expose
    private String year;
    @SerializedName("fdate")
    @Expose
    private String fdate;
    @SerializedName("todate")
    @Expose
    private String todate;
    @SerializedName("memberid")
    @Expose
    private String memberid;
    @SerializedName("a1")
    @Expose
    private String a1;
    @SerializedName("a2")
    @Expose
    private String a2;
    @SerializedName("d1")
    @Expose
    private String d1;
    @SerializedName("d2")
    @Expose
    private String d2;
    @SerializedName("f")
    @Expose
    private String f;

    public String getNewpwd() {
        return newpwd;
    }

    public void setNewpwd(String newpwd) {
        this.newpwd = newpwd;
    }

    public String getAccountno() {
        return accountno;
    }

    public void setAccountno(String accountno) {
        this.accountno = accountno;
    }

    public String getA1() {
        return a1;
    }

    public void setA1(String a1) {
        this.a1 = a1;
    }

    public String getA2() {
        return a2;
    }

    public void setA2(String a2) {
        this.a2 = a2;
    }

    public String getD1() {
        return d1;
    }

    public void setD1(String d1) {
        this.d1 = d1;
    }

    public String getD2() {
        return d2;
    }

    public void setD2(String d2) {
        this.d2 = d2;
    }

    public String getF() {
        return f;
    }

    public void setF(String f) {
        this.f = f;
    }

    public String getMemberid() {
        return memberid;
    }

    public void setMemberid(String memberid) {
        this.memberid = memberid;
    }

    public String getFdate() {
        return fdate;
    }

    public void setFdate(String fdate) {
        this.fdate = fdate;
    }

    public String getTodate() {
        return todate;
    }

    public void setTodate(String todate) {
        this.todate = todate;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getMemAccNo() {
        return memAccNo;
    }

    public void setMemAccNo(String memAccNo) {
        this.memAccNo = memAccNo;
    }

    public String getAccno() {
        return accno;
    }

    public void setAccno(String accno) {
        this.accno = accno;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }
}
