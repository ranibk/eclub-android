package com.ircaindia.eclub.model.retrofit.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class ResponseModelObject<T> {

    @SerializedName("Value")
    @Expose
    private T data = null;
    @SerializedName("Successful")
    @Expose
    private boolean status;


    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }


}
