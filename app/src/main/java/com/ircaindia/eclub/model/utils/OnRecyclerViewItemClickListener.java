package com.ircaindia.eclub.model.utils;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public interface OnRecyclerViewItemClickListener<V extends RecyclerView.ViewHolder, D> {
    void onRecyclerViewItemClick(@NonNull V v, @NonNull View view, @NonNull D d, int position);
}
