package com.ircaindia.eclub.model.sqlite;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


import com.ircaindia.eclub.model.dto.AffiliatedClubs;
import com.ircaindia.eclub.model.dto.BillingInfo;
import com.ircaindia.eclub.model.dto.ReceiptsInfo;

import java.util.ArrayList;
import java.util.List;

public class DataBaseLite extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "eClub";


    public DataBaseLite(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(
                "Create Table BILLINGINFO " +
                        "(billDate text,billNumber text,billId text, amount text,itemCode text,itemName text, qty text,rate text,billAmount text,tax text)"
        );

        db.execSQL(
                "Create Table RECEIPTINFO " +
                        "(towards text,amount text,paymentDate text, paymentType text,paymentMode text,receiptNo text)"
        );
        db.execSQL(
                "Create Table AFFILIATEDCLUBS" +
                        "(clubCode text,clubName text,address text, city text, firstChar text,state text,country text,pinCode text,phone text,latLong text)"
        );

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("DROP TABLE IF EXISTS BILLINGINFO");
        db.execSQL("DROP TABLE IF EXISTS RECEIPTINFO");
        db.execSQL("DROP TABLE IF EXISTS AFFILIATEDCLUBS");
        onCreate(db);
    }

    //*****************************************I  N  S  E  R  T  D  A  T  A***********************************************//

    public void insertBillingInfo(BillingInfo billingInfo) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("billDate", billingInfo.getBilldate());
        contentValues.put("billNumber", billingInfo.getBillnumber());
        contentValues.put("billId", billingInfo.getBillId());
        contentValues.put("amount", billingInfo.getAmount());
        contentValues.put("itemCode", billingInfo.getItemCode());
        contentValues.put("itemName", billingInfo.getItemName());
        contentValues.put("qty", billingInfo.getQuantity());
        contentValues.put("rate", billingInfo.getRate());
        contentValues.put("billAmount", billingInfo.getBillAmount());
        contentValues.put("tax", billingInfo.getTax());

        long iid = db.insert("BILLINGINFO", null, contentValues);
        db.close();
    }

    public void insertReceiptInfo(ReceiptsInfo receiptsInfo) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("towards", receiptsInfo.getTowards());
        contentValues.put("amount", receiptsInfo.getAmount());
        contentValues.put("paymentDate", receiptsInfo.getPaymentDate());
        contentValues.put("paymentType", receiptsInfo.getPaymentType());
        contentValues.put("paymentMode", receiptsInfo.getPaymentMode());
        contentValues.put("receiptNo", receiptsInfo.getReceiptNo());
        long iid = db.insert("RECEIPTINFO", null, contentValues);
        db.close();
    }

    public void insertAffiliatedClubs(AffiliatedClubs affiliatedClubs) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put("clubCode", affiliatedClubs.getAffiliationClubCode());
            contentValues.put("clubName", affiliatedClubs.getAffiliationClubName());
            contentValues.put("address", affiliatedClubs.getClubAddress());
            contentValues.put("city", affiliatedClubs.getCity());
            contentValues.put("state", affiliatedClubs.getState());
            contentValues.put("country", affiliatedClubs.getCountry());
            contentValues.put("pinCode", affiliatedClubs.getPinCode());
            contentValues.put("phone", affiliatedClubs.getPhoneNumber());
            contentValues.put("firstChar", affiliatedClubs.getAffiliationClubName().charAt(0) + "");
            contentValues.put("latLong", affiliatedClubs.getLattitude() + "," + affiliatedClubs.getLongitude());
            long iid = db.insert("AFFILIATEDCLUBS", null, contentValues);
            db.close();
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }


    //*****************************************G  E  T  C  O  U  N  T***********************************************//

    public int getBillingInfoCount() {
        String selectQuery = "SELECT *  FROM  BILLINGINFO";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        int count = cursor.getCount();
        db.close();
        return count;

    }

    public int getReceiptInfoCount() {
        String selectQuery = "SELECT *  FROM  RECEIPTINFO";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        int count = cursor.getCount();
        db.close();
        return count;

    }


    //*****************************************T  R  U  N  C  A  T  E***********************************************//

    public void truncateBillingInfo() {
        SQLiteDatabase db = this.getWritableDatabase();
        long delete = db.delete("BILLINGINFO", null, null);
        db.close();

    }

    public void truncateReceiptInfo() {
        SQLiteDatabase db = this.getWritableDatabase();
        long delete = db.delete("RECEIPTINFO", null, null);
        db.close();

    }

    //*****************************************G   E   T    D   A   T   A***********************************************//

    //************************************************BILLING INFO********************************************//
    public List<String> getBillingDates() {
        List<String> stringList = new ArrayList<>();
        try {
            SQLiteDatabase db = this.getReadableDatabase();
            String selectQuery = "Select distinct billDate from BILLINGINFO";
            @SuppressLint("Recycle")
            Cursor cursor = db.rawQuery(selectQuery, null);

            if (cursor.getCount() != 0) {
                if (cursor.moveToFirst()) {
                    do {
                        stringList.add(cursor.getString(0));
                    } while (cursor.moveToNext());
                }
            }
            db.close();
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return stringList;
    }

    public List<String> getBillingNumbers(String date) {
        List<String> stringList = new ArrayList<>();
        try {
            String selectQuery = "";

            selectQuery = "SELECT distinct billNumber  FROM BILLINGINFO  where billDate =" + "'" + date + "'";

            SQLiteDatabase db = this.getReadableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);

            if (cursor.getCount() != 0) {
                if (cursor.moveToFirst()) {
                    do {
                        stringList.add(cursor.getString(0));
                    } while (cursor.moveToNext());
                }
            }
            // closing connection
            cursor.close();
            db.close();
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return stringList;
    }

    public List<BillingInfo> getBillingInfoList(String billNumber) {
        List<BillingInfo> billingInfoList = new ArrayList<>();
        try {
            String selectQuery = "";

            selectQuery = "SELECT billDate,billNumber,billId,amount,itemCode,itemName,qty,rate,billAmount,tax  FROM BILLINGINFO  where billNumber =" + "'" + billNumber + "'";

            SQLiteDatabase db = this.getReadableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);

            if (cursor.getCount() != 0) {
                if (cursor.moveToFirst()) {
                    do {
                        BillingInfo billingInfo = new BillingInfo();
                        billingInfo.setBilldate(cursor.getString(0));
                        billingInfo.setBillnumber(cursor.getString(1));
                        billingInfo.setBillId(cursor.getString(2));
                        billingInfo.setAmount(cursor.getString(3));
                        billingInfo.setItemCode(cursor.getString(4));
                        billingInfo.setItemName(cursor.getString(5));
                        billingInfo.setQuantity(cursor.getString(6));
                        billingInfo.setRate(cursor.getString(7));
                        billingInfo.setBillAmount(cursor.getString(8));
                        billingInfo.setTax(cursor.getString(9));
                        billingInfoList.add(billingInfo);
                    } while (cursor.moveToNext());
                }
            }
            // closing connection
            cursor.close();
            db.close();
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return billingInfoList;
    }

    public List<ReceiptsInfo> getReceiptInfoList() {
        List<ReceiptsInfo> receiptsInfoList = new ArrayList<>();
        try {
            String selectQuery = "";

            selectQuery = "SELECT towards,amount,paymentDate,paymentType,paymentMode,receiptNo  FROM RECEIPTINFO ";

            SQLiteDatabase db = this.getReadableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);

            if (cursor.getCount() != 0) {
                if (cursor.moveToFirst()) {
                    do {
                        ReceiptsInfo receiptsInfo = new ReceiptsInfo();
                        receiptsInfo.setTowards(cursor.getString(0));
                        receiptsInfo.setAmount(cursor.getString(1));
                        receiptsInfo.setPaymentDate(cursor.getString(2));
                        receiptsInfo.setPaymentType(cursor.getString(3));
                        receiptsInfo.setPaymentMode(cursor.getString(4));
                        receiptsInfo.setReceiptNo(cursor.getString(5));

                        receiptsInfoList.add(receiptsInfo);
                    } while (cursor.moveToNext());
                }
            }
            // closing connection
            cursor.close();
            db.close();
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return receiptsInfoList;
    }


//    public List<ItemCategory> getItemCategory() {
//        List<ItemCategory> itemCategoryList = new ArrayList<>();
//        SQLiteDatabase db = this.getReadableDatabase();
//        String selectQuery = "Select Id,GroupName,GroupCode from ITEMCATEGORY ";
//        @SuppressLint("Recycle")
//        Cursor cursor = db.rawQuery(selectQuery, null);
//        ItemCategory item;
//        if (cursor.getCount() != 0) {
//            if (cursor.moveToFirst()) {
//                do {
//                    item = new ItemCategory();
//                    item.setGroupid(cursor.getString(0));
//                    item.setGroupcode(cursor.getString(2));
//                    item.setGroupname(cursor.getString(1));
//                    itemCategoryList.add(item);
//                } while (cursor.moveToNext());
//            }
//        }
//        db.close();
//        return itemCategoryList;
//    }


//    public ArrayList<ItemsNew> getCounters() {
//        ArrayList<ItemsNew> itemlist = new ArrayList<>();
//        SQLiteDatabase db = this.getReadableDatabase();
//        String selectQuery = "Select distinct CounterName,CounterImage from ITEMSNEW GROUP BY CounterImage";
//        @SuppressLint("Recycle")
//        Cursor cursor = db.rawQuery(selectQuery, null);
//        ItemsNew item;
//        if (cursor.getCount() != 0) {
//            if (cursor.moveToFirst()) {
//                do {
//                    item = new ItemsNew();
//                    item.setCounterName(cursor.getString(0));
//                    item.setCounterImage(cursor.getString(1));
//                    itemlist.add(item);
//                } while (cursor.moveToNext());
//            }
//        }
//        db.close();
//        return itemlist;
//    }


    ///EXTRAAAAAAAAAAAA

    public List<String> getAllItemSearch(String param, String product) {
        List<String> labels = new ArrayList<String>();
        //  String p = param + "%";
        String p = param + "%";
        String selectQuery = "", selectQuery1 = "";
        // Select All Query

        if (product.equals("na")) {
            selectQuery = "SELECT Id,ItemName  FROM ITEMS  where ItemName like" + "'" + p + "'";
            selectQuery1 = "SELECT Id,ItemName  FROM ITEMS  where ItemCode like" + "'" + p + "'";
        } else {
            selectQuery = "SELECT Id,ItemName  FROM ITEMS  where ItemName like" + "'" + p + "' and ItemGroupID ='" + product + "'";
            selectQuery1 = "SELECT Id,ItemName  FROM ITEMS  where ItemCode like" + "'" + p + "' and ItemGroupID ='" + product + "'";
            ;
        }
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        Cursor cursor2 = db.rawQuery(selectQuery1, null);

        if (cursor.getCount() != 0) {
            if (cursor.moveToFirst()) {
                do {
                    labels.add(cursor.getString(1) + "#" + cursor.getString(0));
                } while (cursor.moveToNext());
            }
        } else {
            if (cursor2.moveToFirst()) {
                do {
                    labels.add(cursor2.getString(1) + "#" + cursor2.getString(0));
                } while (cursor2.moveToNext());
            }
        }
        // closing connection
        cursor.close();
        cursor2.close();
        db.close();

        return labels;
    }


    public String getItemCode(String id) {
        String labels = "";
        String selectQuery = "";

        selectQuery = "SELECT ItemCode  FROM ITEMS  where Id like" + "'" + id + "'";

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.getCount() != 0) {
            if (cursor.moveToFirst()) {
                do {
                    labels = cursor.getString(0);
                } while (cursor.moveToNext());
            }
        }
        // closing connection
        cursor.close();
        db.close();

        return labels;
    }

    public String getCategoryId(String categoryName) {
        String labels = "";
        String selectQuery = "";

        selectQuery = "SELECT Id  FROM ITEMCATEGORY  where GroupName like" + "'" + categoryName + "'";

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.getCount() != 0) {
            if (cursor.moveToFirst()) {
                do {
                    labels = cursor.getString(0);
                } while (cursor.moveToNext());
            }
        }
        // closing connection
        cursor.close();
        db.close();

        return labels;
    }

    public String getCategoryName(String id) {
        String labels = "";
        String selectQuery = "";

        selectQuery = "SELECT GroupName  FROM ITEMCATEGORY  where  Id like" + "'" + id + "'";

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.getCount() != 0) {
            if (cursor.moveToFirst()) {
                do {
                    labels = cursor.getString(0);
                } while (cursor.moveToNext());
            }
        }
        // closing connection
        cursor.close();
        db.close();

        return labels;
    }


}
