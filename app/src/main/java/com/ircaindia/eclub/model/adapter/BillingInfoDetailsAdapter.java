package com.ircaindia.eclub.model.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ircaindia.eclub.R;
import com.ircaindia.eclub.model.dto.BillingInfo;
import com.ircaindia.eclub.model.utils.OnRecyclerViewItemClickListener;

import java.util.List;

public class BillingInfoDetailsAdapter extends RecyclerView.Adapter<BillingInfoDetailsAdapter.MyViewHolder> {

    private Context context;
    private List<BillingInfo> stringList;
    private OnRecyclerViewItemClickListener<BillingInfoDetailsAdapter.MyViewHolder, BillingInfo> onRecyclerViewItemClickListener;

    public BillingInfoDetailsAdapter(List<BillingInfo> stringList, Context context) {
        this.stringList = stringList;
        this.context = context;
    }

    @NonNull
    @Override
    public BillingInfoDetailsAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = null;
        try {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_billing_info_details_item, parent, false);
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return new BillingInfoDetailsAdapter.MyViewHolder(view);
    }

    public void setOnRecyclerViewItemClickListener(@NonNull OnRecyclerViewItemClickListener<BillingInfoDetailsAdapter.MyViewHolder, BillingInfo> onRecyclerViewItemClickListener) {
        this.onRecyclerViewItemClickListener = onRecyclerViewItemClickListener;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull BillingInfoDetailsAdapter.MyViewHolder holder, int position) {
        try {
            BillingInfo billingInfo = stringList.get(position);
            holder.rateTextView.setText(billingInfo.getRate());
            holder.amountTextView.setText(billingInfo.getBillAmount());
            holder.qtyTextView.setText(billingInfo.getQuantity());
            holder.itemNameTextView.setText(billingInfo.getItemName());
            if (!billingInfo.getItemName().trim().toLowerCase().equals("total")) {
                holder.itemNameTextView.setTextColor(context.getResources().getColor(R.color.blackLight));
                holder.rateTextView.setTextColor(context.getResources().getColor(R.color.blackLight));
                holder.qtyTextView.setTextColor(context.getResources().getColor(R.color.blackLight));
                holder.amountTextView.setTextColor(context.getResources().getColor(R.color.blackLight));
                holder.itemNameTextView.setTextSize(13);
                holder.rateTextView.setTextSize(13);
                holder.amountTextView.setTextSize(13);
                holder.qtyTextView.setTextSize(13);
            } else {
                holder.itemNameTextView.setTextColor(context.getResources().getColor(R.color.colorPrimary));
                holder.rateTextView.setTextColor(context.getResources().getColor(R.color.colorPrimary));
                holder.qtyTextView.setTextColor(context.getResources().getColor(R.color.colorPrimary));
                holder.amountTextView.setTextColor(context.getResources().getColor(R.color.colorPrimary));
                holder.itemNameTextView.setTextSize(17);
                holder.rateTextView.setTextSize(17);
                holder.qtyTextView.setTextSize(17);
                holder.amountTextView.setTextSize(17);
            }

        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        try {
            return stringList.size();
        } catch (Throwable e) {
            e.printStackTrace();
            return 0;
        }
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView itemNameTextView, qtyTextView, rateTextView, amountTextView;

        MyViewHolder(View itemView) {
            super(itemView);
            try {
                itemNameTextView = itemView.findViewById(R.id.text_viewItemName);
                qtyTextView = itemView.findViewById(R.id.text_viewQty);
                rateTextView = itemView.findViewById(R.id.text_viewRate);
                amountTextView = itemView.findViewById(R.id.text_viewAmount);
//                itemView.setOnClickListener(this);
            } catch (Throwable t) {
                t.printStackTrace();
            }
        }

        //        @Override
//        public void onClick(View view) {
//            try {
//                DashboardDto dashboardDto = stringList.get(getAdapterPosition());
//                switch (dashboardDto.getId()) {
//                    case "1":
//                        Intent i1 = new Intent(context, DashboardBillingInfo.class);
//                        i1.putExtra("accNO", context.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE).getString("ac_no", "0"));
//                        i1.putExtra("Item", 0);
//                        context.startActivity(i1);
//                        break;
//                    case "2":
//                        break;
//
//                }
//            } catch (Throwable e) {
//                e.printStackTrace();
//            }
//        }
        @Override
        public void onClick(View view) {
            try {
                if (onRecyclerViewItemClickListener != null)
                    onRecyclerViewItemClickListener.onRecyclerViewItemClick(this, view, stringList.get(getAdapterPosition()), getAdapterPosition());
            } catch (Throwable e) {
                e.printStackTrace();
            }
        }
    }
}


