package com.ircaindia.eclub.model.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ChangePassword implements Serializable {

    @SerializedName("change")
    @Expose
    private String change;

    public String getChange() {
        return change;
    }

    public void setChange(String change) {
        this.change = change;
    }
}
