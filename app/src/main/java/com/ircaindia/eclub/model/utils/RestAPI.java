package com.ircaindia.eclub.model.utils;

import android.content.Context;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class RestAPI {


    Context c;


    public String urlString = "http://122.166.154.101/mobile_tata_billing/handler1.ashx";//Live   240

    public RestAPI(Context context) {
        c = context;

    }

    private String load(String contents) throws IOException {
        URL url = new URL(urlString);
        Log.d("URL", urlString);
        Log.d("PostData", contents);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("POST");
        conn.setConnectTimeout(60000);
        conn.setDoOutput(true);
        conn.setDoInput(true);
        OutputStreamWriter w = new OutputStreamWriter(conn.getOutputStream());
        w.write(contents);
        w.flush();
        InputStream istream = conn.getInputStream();
        String result = convertStreamToUTF8String(istream);
        Log.d("Response", result);
        return result;
    }

    private static String convertStreamToUTF8String(InputStream stream) throws IOException {
        String result = "";
        StringBuilder sb = new StringBuilder();
        try {
            InputStreamReader reader = new InputStreamReader(stream, "UTF-8");
            char[] buffer = new char[4096];
            int readedChars = 0;
            while (readedChars != -1) {
                readedChars = reader.read(buffer);
                if (readedChars > 0)
                    sb.append(buffer, 0, readedChars);
            }
            result = sb.toString();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return result;
    }

    private Object mapObject(Object o) {
        Object finalValue = null;
        if (o.getClass() == String.class) {
            finalValue = o;
        } else if (Number.class.isInstance(o)) {
            finalValue = String.valueOf(o);
        } else if (Date.class.isInstance(o)) {
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss", new Locale("en", "USA"));
            finalValue = sdf.format((Date) o);
        } else if (Collection.class.isInstance(o)) {
            Collection<?> col = (Collection<?>) o;
            JSONArray jarray = new JSONArray();
            for (Object item : col) {
                jarray.put(mapObject(item));
            }
            finalValue = jarray;
        } else {
            Map<String, Object> map = new HashMap<String, Object>();
            Method[] methods = o.getClass().getMethods();
            for (Method method : methods) {
                if (method.getDeclaringClass() == o.getClass()
                        && method.getModifiers() == Modifier.PUBLIC
                        && method.getName().startsWith("get")) {
                    String key = method.getName().substring(3);
                    try {
                        Object obj = method.invoke(o, null);
                        Object value = mapObject(obj);
                        map.put(key, value);
                        finalValue = new JSONObject(map);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

        }
        return finalValue;
    }


    public JSONObject CheckUpdate() throws Exception {
        JSONObject result = null;
        JSONObject o = new JSONObject();
        JSONObject p = new JSONObject();
        o.put("interface", "RestAPI");
        o.put("method", "CheckUpdate");
        o.put("parameters", p);
        String s = o.toString();
        String r = load(s);
        result = new JSONObject(r);
        return result;
    }
    public JSONObject GetOTPNoForgot(String Phonenumber,String isresendotp,String source) throws Exception {
        JSONObject result = null;
        JSONObject o = new JSONObject();
        JSONObject p = new JSONObject();
        o.put("interface","RestAPI");
        o.put("method", "GetOTPNoForgot");
        p.put("Phonenumber",mapObject(Phonenumber));
        p.put("isresendotp",mapObject(isresendotp));
        p.put("source",mapObject(source));
        o.put("parameters", p);
        String s = o.toString();
        String r = load(s);
        result = new JSONObject(r);
        return result;
    }

    public JSONObject addcustomers(String Customername, String Phonenumber, String Emailid, String Address, String City, String State, String pincode, String source, String password) throws Exception {
        JSONObject result = null;
        JSONObject o = new JSONObject();
        JSONObject p = new JSONObject();
        o.put("interface", "RestAPI");
        o.put("method", "addcustomers");
        p.put("Customername", mapObject(Customername));
        p.put("Phonenumber", mapObject(Phonenumber));
        p.put("Emailid", mapObject(Emailid));
        p.put("Address", mapObject(Address));
        p.put("City", mapObject(City));
        p.put("State", mapObject(State));
        p.put("pincode", mapObject(pincode));
        p.put("source", mapObject(source));
        p.put("password", mapObject(password));
        o.put("parameters", p);
        String s = o.toString();
        String r = load(s);
        result = new JSONObject(r);
        return result;
    }

    public JSONObject GetOTPNo(String Phonenumber, String isresendotp, String source) throws Exception {
        JSONObject result = null;
        JSONObject o = new JSONObject();
        JSONObject p = new JSONObject();
        o.put("interface", "RestAPI");
        o.put("method", "GetOTPNo");
        p.put("Phonenumber", mapObject(Phonenumber));
        p.put("isresendotp", mapObject(isresendotp));
        p.put("source", mapObject(source));
        o.put("parameters", p);
        String s = o.toString();
        String r = load(s);
        result = new JSONObject(r);
        return result;
    }

    public JSONObject changepassword(String mobileno, String newpwd) throws Exception {
        JSONObject result = null;
        JSONObject o = new JSONObject();
        JSONObject p = new JSONObject();
        o.put("interface", "RestAPI");
        o.put("method", "changepassword");
        p.put("mobileno", mapObject(mobileno));
        p.put("newpwd", mapObject(newpwd));
        o.put("parameters", p);
        String s = o.toString();
        String r = load(s);
        result = new JSONObject(r);
        return result;
    }

    public JSONObject customerAuthentication(String userName, String passsword, String devicemodel, String source) throws Exception {
        JSONObject result = null;
        JSONObject o = new JSONObject();
        JSONObject p = new JSONObject();
        o.put("interface", "RestAPI");
        o.put("method", "customerAuthentication");
        p.put("userName", mapObject(userName));
        p.put("passsword", mapObject(passsword));
        p.put("devicemodel", mapObject(devicemodel));
        p.put("source", mapObject(source));
        o.put("parameters", p);
        String s = o.toString();
        String r = load(s);
        result = new JSONObject(r);
        return result;
    }

    public JSONObject getItems(String storeid) throws Exception {
        JSONObject result = null;
        JSONObject o = new JSONObject();
        JSONObject p = new JSONObject();
        o.put("interface", "RestAPI");
        o.put("method", "getItems");
        p.put("storeid", mapObject(storeid));
        o.put("parameters", p);
        String s = o.toString();
        String r = load(s);
        result = new JSONObject(r);
        return result;
    }

    public JSONObject getUOM() throws Exception {
        JSONObject result = null;
        JSONObject o = new JSONObject();
        JSONObject p = new JSONObject();
        o.put("interface", "RestAPI");
        o.put("method", "getUOM");
        o.put("parameters", p);
        String s = o.toString();
        String r = load(s);
        result = new JSONObject(r);
        return result;
    }


    public JSONObject getItemGroup(String itemcategoryid) throws Exception {
        JSONObject result = null;
        JSONObject o = new JSONObject();
        JSONObject p = new JSONObject();
        o.put("interface", "RestAPI");
        o.put("method", "BMS_getItemGroup");
        p.put("itemcategoryid", mapObject(itemcategoryid));
        o.put("parameters", p);
        String s = o.toString();
        String r = load(s);
        result = new JSONObject(r);
        return result;
    }

    public JSONObject getCustomerdetails_profile(String customerid) throws Exception {
        JSONObject result = null;
        JSONObject o = new JSONObject();
        JSONObject p = new JSONObject();
        o.put("interface", "RestAPI");
        o.put("method", "getCustomerdetails_profile");
        p.put("customerid", mapObject(customerid));
        o.put("parameters", p);
        String s = o.toString();
        String r = load(s);
        result = new JSONObject(r);
        return result;
    }

    public JSONObject placeOrder(Object param, ArrayList<Object> itemlist, String deviceid, String cardtype, String loginId, String posId, String deviceNodel, String referenceNo, String personId, String BillType, String PaymentMode, String OTtype, int adAmount, String customsize, ArrayList<Object> paymentlist, String icid, Object customeraddress) throws Exception {
        JSONObject result = null;
        JSONObject o = new JSONObject();
        JSONObject p = new JSONObject();
        o.put("interface", "RestAPI");
        o.put("method", "BMS_placeOrder");
        p.put("param", mapObject(param));
        p.put("itemlist", mapObject(itemlist));
        p.put("deviceid", mapObject(deviceid));
        p.put("cardtype", mapObject(cardtype));
        p.put("loginId", mapObject(loginId));
        p.put("posId", mapObject(posId));
        p.put("deviceNodel", mapObject(deviceNodel));
        p.put("referenceNo", mapObject(referenceNo));
        p.put("personId", mapObject(personId));
        p.put("BillType", mapObject(BillType));
        p.put("PaymentMode", mapObject(PaymentMode));
        p.put("OTtype", mapObject(OTtype));
        p.put("adAmount", mapObject(adAmount));
        p.put("customsize", mapObject(customsize));
        p.put("paymentlist", mapObject(paymentlist));
        p.put("icid", mapObject(icid));
        p.put("customeraddress", mapObject(customeraddress));
        o.put("parameters", p);
        String s = o.toString();
        String r = load(s);
        result = new JSONObject(r);
        return result;
    }

    public JSONObject getStores(String personid) throws Exception {
        JSONObject result = null;
        JSONObject o = new JSONObject();
        JSONObject p = new JSONObject();
        o.put("interface", "RestAPI");
        o.put("method", "getStores");
        p.put("personid", mapObject(personid));
        o.put("parameters", p);
        String s = o.toString();
        String r = load(s);
        result = new JSONObject(r);
        return result;
    }


    public JSONObject getBillDetails(String loginid, String billnumber, String storeid) throws Exception {
        JSONObject result = null;
        JSONObject o = new JSONObject();
        JSONObject p = new JSONObject();
        o.put("interface", "RestAPI");
        o.put("method", "getBilldetails");
        p.put("loginid", mapObject(loginid));
        p.put("billnumber", mapObject(billnumber));
        p.put("storeid", mapObject(storeid));
        o.put("parameters", p);
        String s = o.toString();
        String r = load(s);
        result = new JSONObject(r);
        return result;
    }


    public JSONObject getStoreSales(String fromdate,String todate,String storeid,String customerid) throws Exception {
        JSONObject result = null;
        JSONObject o = new JSONObject();
        JSONObject p = new JSONObject();
        o.put("interface","RestAPI");
        o.put("method", "getStoreSales");
        p.put("fromdate",mapObject(fromdate));
        p.put("todate",mapObject(todate));
        p.put("storeid",mapObject(storeid));
        p.put("customerid",mapObject(customerid));
        o.put("parameters", p);
        String s = o.toString();
        String r = load(s);
        result = new JSONObject(r);
        return result;
    }


}
