package com.ircaindia.eclub.model.sqlite;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;

public class DataManagers {
	/**
	 * Called to save supplied value in shared preferences against given key.
	 * @param context Context of caller activity
	 * @param key Key of value to save against
	 * @param value Value to save
	 */
	public static void saveToPrefs(Context context, String key, String value) {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		final SharedPreferences.Editor editor = prefs.edit();
		editor.putString(key,value);
		editor.apply();
	}

	public static void saveBooleanToPrefs(Context context, String key, Boolean value) {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		final SharedPreferences.Editor editor = prefs.edit();
		editor.putBoolean(key,value);
		editor.apply();
	}

	/**
	 * Called to retrieve required value from shared preferences, identified by given key.
	 * Default value will be returned of no value found or error occurred.
	 * @param context Context of caller activity
	 * @param key Key to find value against
	 * @param defaultValue Value to return if no data found against given key
	 * @return Return the value found against given key, default if not found or any error occurs
	 */
	public static String getFromPrefs(Context context, String key, String defaultValue) {
		SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
		try {
			return sharedPrefs.getString(key, defaultValue);
		} catch (Exception e) {
			e.printStackTrace();
			return defaultValue;
		}
	}

	public static Boolean getBooleanFromPrefs(Context context, String key, Boolean value) {
		SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
		try {
			return sharedPrefs.getBoolean(key, value);
		} catch (Exception e) {
			e.printStackTrace();
			return value;
		}
	}

	public  static void saveToPrefs(Context context, String key, Object value){
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		final SharedPreferences.Editor editor = prefs.edit();
		Gson gson = new Gson();
		String json = gson.toJson(value);
		editor.putString(key, json);
		editor.apply();
	}

	public static Object getFromPrefs(Context context, String key , Class<?> name) {
		SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
		try {
			Gson gson = new Gson();
			String json = sharedPrefs.getString(key, "");
			return gson.fromJson(json,name);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	/**
	 *
	 * @param context Context of caller activity
	 * @param key Key to delete from SharedPreferences
	 */

	public static void removeFromPrefs(Context context, String key) {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		final SharedPreferences.Editor editor = prefs.edit();
		editor.remove(key);
		editor.commit();
	}
}
