package com.ircaindia.eclub.model.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SendClubDetails implements Serializable {

    @SerializedName("model")
    @Expose
    private String model;
    @SerializedName("imei")
    @Expose
    private String imei;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("accno")
    @Expose
    private String accountNo;
    @SerializedName("clubname")
    @Expose
    private String clubName;
    @SerializedName("appid")
    @Expose
    private String appId;
    @SerializedName("appname")
    @Expose
    private String appName;
    @SerializedName("os")
    @Expose
    private String os;

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public String getClubName() {
        return clubName;
    }

    public void setClubName(String clubName) {
        this.clubName = clubName;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getOs() {
        return os;
    }

    public void setOs(String os) {
        this.os = os;
    }
}
