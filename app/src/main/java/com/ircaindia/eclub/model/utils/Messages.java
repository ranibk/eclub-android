package com.ircaindia.eclub.model.utils;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.telephony.TelephonyManager;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Base64;
import android.util.Log;
import android.util.Patterns;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.ircaindia.eclub.BuildConfig;
import com.ircaindia.eclub.R;
import com.ircaindia.eclub.model.apis.RestAPICall;
import com.ircaindia.eclub.model.dto.ClubDetails;
import com.ircaindia.eclub.model.dto.Login;
import com.ircaindia.eclub.model.dto.Profile;
import com.ircaindia.eclub.model.dto.RememberMe;
import com.ircaindia.eclub.model.retrofit.server.RetrofitClient;
import com.ircaindia.eclub.view.activity.SplashActivity;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Objects;


/**
 * Created by BhargavaNatha Reddy on 29/03/2018.
 */

public class Messages implements IMessages {

    private AlertOnClick alertOnClick;
    private Context context;
    Dialog progressDialog;
    private SharedPreferences sharedPreferences;

    public Messages(AlertOnClick alertOnClick) {
        this.alertOnClick = alertOnClick;
    }

    public Messages(Context context) {
        this.context = context;
        sharedPreferences = context.getSharedPreferences("CLUBDETAILS", Context.MODE_PRIVATE);
    }

    @Override
    public void showProgressDialog(Context context, String title, String message) {
        try {
            progressDialog = new Dialog(context);
            progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            progressDialog.setContentView(R.layout.dialog_progressbar);
            TextView progressTv = progressDialog.findViewById(R.id.progress_tv);
            progressTv.setText(message);
            if (progressDialog.getWindow() != null)
                progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            progressDialog.setCancelable(false);
            progressDialog.show();
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }


    @Override
    public void hideDialog() {
        try {
            progressDialog.dismiss();
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    @SuppressLint("HardwareIds")
    @Override
    public String getDeviceId(Activity activity) {
        String device = "";
        try {
            TelephonyManager tel = (TelephonyManager) activity.getSystemService(Context.TELEPHONY_SERVICE);

            if (Build.VERSION.SDK_INT >= 29) {
                device = Settings.Secure.getString(activity.getContentResolver(), Settings.Secure.ANDROID_ID) + "";
            } else {
                if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                    return "";
                }
                device = Objects.requireNonNull(tel).getDeviceId() + "";
            }
        } catch (Throwable e) {
            e.printStackTrace();
            try {
                device = Build.SERIAL + "";
            } catch (Throwable throwable) {
                throwable.printStackTrace();
                device = "";
            }
        }
        return device;
    }

    @Override
    public String getDeviceModel(Activity activity) {
        return Build.MODEL + "-" + Build.DEVICE;

    }


    @Override
    public void showAlertDialog(Context context, String message) {
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setMessage(message);
        alertDialogBuilder.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        arg0.dismiss();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }


    @Override
    public void showAlert(final Context context) {
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setMessage("Authentication failed, Please login again");
        alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int s) {
                Intent i = new Intent(context, SplashActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                context.startActivity(i);
            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();
        alertDialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
                if (i == KeyEvent.KEYCODE_BACK) {
                    Log.d("TAG", "--------- Do Something -----------");
                    return true;
                }
                return false;
            }
        });

    }

    @Override
    public void showAlertPassword(final Context context) {
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setMessage("Password changed successfully, Please login again");
        alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int s) {
                Intent i = new Intent(context, SplashActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                context.startActivity(i);
            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();
        alertDialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
                if (i == KeyEvent.KEYCODE_BACK) {
                    Log.d("TAG", "--------- Do Something -----------");
                    return true;
                }
                return false;
            }
        });

    }


    @Override
    public void toast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }


    @Override
    public void toastSnackBar(Context context, String message, View view) {
        Snackbar snackbar = Snackbar.make(view, message, Snackbar.LENGTH_LONG);
        snackbar.show();
    }

    @Override
    public void toastInternet(Context context, View view) {
        Toast.makeText(context, "Opps... Check Internet Connection", Toast.LENGTH_SHORT).show();
//        Snackbar snackbar = Snackbar
//                .make(view, "Opps... Check Internet Connection", Snackbar.LENGTH_LONG);
////                .setAction("Retry", new View.OnClickListener() {
////                    @Override
////                    public void onClick(View view) {
////
////                    }
////                });
////        snackbar.setActionTextColor(Color.RED);
//        View sbView = snackbar.getView();
//        sbView.setBackgroundColor(Color.RED);
//        TextView textView = sbView.findViewById(android.support.design.R.id.snackbar_text);
//        textView.setTextColor(Color.WHITE);
//        snackbar.show();
    }

    @Override
    public boolean isValidMail(String email) {
        return Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    @Override
    public boolean isValidNumber(String number) {
        String INDIAN_MOBILE_NUMBER_PATTERN = "^[6789]\\d{9}$";
        return number.matches(INDIAN_MOBILE_NUMBER_PATTERN);
    }

    @Override
    public void saveCurrentLogin(Activity activity, Login login) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(activity);
        final SharedPreferences.Editor editor = prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(login);
        editor.putString("login", json);
        editor.apply();
    }

    @Override
    public Login getCurrentLogin(Activity activity) {
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(activity);
        try {
            Gson gson = new Gson();
            String json = sharedPrefs.getString("login", "");
            return gson.fromJson(json, Login.class);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void rememberMe(Activity activity, RememberMe rememberMe) {
        SharedPreferences sharedPreferences = activity.getSharedPreferences("rememberMe", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor1 = sharedPreferences.edit();
        editor1.putString("appUser", rememberMe.getUserName());
        editor1.putString("appPass", rememberMe.getPassWord());
        editor1.putBoolean("check", rememberMe.isSave());
        editor1.apply();

    }

    @Override
    public RememberMe getRememberMe(Activity activity) {
        RememberMe rememberMe = new RememberMe();
        SharedPreferences sharedPreferences = activity.getSharedPreferences("rememberMe", Context.MODE_PRIVATE);
        rememberMe.setUserName(sharedPreferences.getString("appUser", ""));
        rememberMe.setPassWord(sharedPreferences.getString("appPass", ""));
        rememberMe.setSave(sharedPreferences.getBoolean("check", false));

        return rememberMe;
    }

    @Override
    public void logOut(final Activity activity) {
        new AlertDialog.Builder(activity)
                .setTitle("Logout")
                .setMessage("Do you want to Logout?")
                .setIcon(R.mipmap.ic_launcher_round)
                .setPositiveButton("Logout",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent back = new Intent(activity, SplashActivity.class);
                                back.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                activity.startActivity(back);
                            }
                        })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                }).show();


    }

    @Override
    public String getDeviceOS() {
        return "Android";
    }

    @Override
    public String getLastStringURL(String url) {
        String[] bits = url.split("/");
        return bits[bits.length - 1];
    }

    @Override
    public String getFirstStringURL(String url, String lastOne) {
        return url.replace(lastOne, "");
    }


    @Override
    public void hideKeyBoard(Activity activity, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        assert imm != null;
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

    }

    @Override
    public int lastDayOfMonth(int month, int year) {
        int lastDay = 1;
        switch (month) {
            case 1:                 // January
                lastDay = 31;
                break;
            case 3:                 // March
                lastDay = 31;
                break;
            case 5:                 // May
                lastDay = 31;
                break;
            case 7:                 // July
                lastDay = 31;
                break;
            case 8:                  // August
                lastDay = 31;
                break;
            case 10:                // October
                lastDay = 31;
                break;
            case 12:                // December
                lastDay = 31;
                break;
            case 4:                 // April
                lastDay = 30;
                break;
            case 6:                  // june
                lastDay = 30;
                break;
            case 9:                 // september
                lastDay = 30;
                break;
            case 11:                // November
                lastDay = 30;
                break;
            case 2:                 // February
                if (0 == year % 4 && 0 != year % 100 || 0 == year % 400) {
                    lastDay = 29;
                } else {
                    lastDay = 28;
                }
                break;
        }
        return lastDay;


    }

    @Override
    public void dateForHistory(Activity activity, final EditText view, String title) {

        try {
            DatePickerDialog.OnDateSetListener myDateSetListener = new DatePickerDialog.OnDateSetListener() {
                @SuppressLint("SetTextI18n")
                @Override
                public void onDateSet(DatePicker datePicker, int i, int j, int k) {
                    j = j + 1;
                    view.setText(i + "-" + getDateView(j) + "-" + getDateView(k));
                }
            };

            Calendar c = Calendar.getInstance();
            @SuppressLint("SimpleDateFormat")
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            String[] date_split = df.format(c.getTime()).split("-");
            DatePickerDialog datePickerDialog = new DatePickerDialog(activity, myDateSetListener, Integer.parseInt(date_split[0]), Integer.parseInt(date_split[1]) - 1, Integer.parseInt(date_split[2]));
            datePickerDialog.setTitle(title);
            datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
            datePickerDialog.show();
        } catch (Throwable e) {
            e.printStackTrace();
            Calendar c = Calendar.getInstance();
            @SuppressLint("SimpleDateFormat")
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            view.setText(df.format(c.getTime()));
        }
    }

    @Override
    public RestAPICall getAPIService(String url) {
        return RetrofitClient.getClient(url).create(RestAPICall.class);
    }

    private String getDateView(int date) {
        if (date > 9) {
            return date + "";
        } else {
            return "0" + date;
        }

    }

    @Override
    public void saveClubDetails(List<ClubDetails> clubDetails) {
        SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(clubDetails);
        prefsEditor.putString("ClubDetailsList", json);
        prefsEditor.apply();
    }

    @Override
    public boolean isInternetAvailable() {
        ConnectionDetector cd = new ConnectionDetector(context);
        return cd.isConnectingToInternet();
    }

    @Override
    public SpannableString getToolbarTitle(String title) {
        SpannableString s = new SpannableString(title);
        s.setSpan(new TypeFaceSpan(context, "SourceSansPro-Semibold.ttf"), 0, s.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        return s;
    }

    @Override
    public SpannableString getCustomFontString(String text, String fontName) {
        SpannableString s = new SpannableString(text);
        s.setSpan(new TypeFaceSpan(context, fontName), 0, s.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        return s;
    }

    @SuppressLint("SdCardPath")
    public void shareOption() {
        try {
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
//            sendIntent.putExtra(Intent.EXTRA_SUBJECT, context.getResources().getString(R.string.app_name));
            sendIntent.putExtra(Intent.EXTRA_TEXT,
                    context.getResources().getString(R.string.app_name).toUpperCase()+" APP \nHey, download this app to manage the things at your Finger Tips \nLink: https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID);
            sendIntent.setType("text/plain");
            context.startActivity(Intent.createChooser(sendIntent, "Share app with"));
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getCurrentMonthYear() {
        String month = "";
        try {
            @SuppressLint("SimpleDateFormat")
            DateFormat dateFormat = new SimpleDateFormat("MMM");
            @SuppressLint("SimpleDateFormat")
            DateFormat dateFormat1 = new SimpleDateFormat("yyyy");
            Date date = new Date();
            Log.d("Month", dateFormat.format(date));
            month = dateFormat.format(date) + "," + dateFormat1.format(date);
        } catch (Throwable e) {
            e.printStackTrace();
            month = "Sep-2020";
        }

        return month;
    }

    @Override
    public String getDateChangeBillingInfo(String date1) {
        String dateReturn = "";
        try {
            @SuppressLint("SimpleDateFormat")
            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            @SuppressLint("SimpleDateFormat")
            DateFormat dateFormat1 = new SimpleDateFormat("dd-MMM");
            Date date = dateFormat.parse(date1);
            assert date != null;
            dateReturn = dateFormat1.format(date);
        } catch (Throwable e) {
            e.printStackTrace();
            dateReturn = date1;
        }
        return dateReturn;
    }

    @Override
    public String getStringRupee(String string) {
        String string1 = "";
        string1 = "₹ " + string;
        return string1;
    }

    @Override
    public boolean isFutureDate(String current, String selected) {
        try {
            IMessages messages = new Messages(context);
            int currentYear = Integer.parseInt(current.trim().split(",")[1].trim());
            int selectedYear = Integer.parseInt(selected.trim().split(",")[1].trim());
            int currentMonth = messages.getMonthNumberFromName(current.trim().split(",")[0].trim());
            int selectedMonth = messages.getMonthNumberFromName(selected.trim().split(",")[0].trim());
            if (selectedYear > currentYear) {
                return true;
            } else if (selectedYear == currentYear) {
                if (selectedMonth > currentMonth) {
                    return true;
                }
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return false;
    }


    @SuppressLint("SimpleDateFormat")
    @Override
    public String getDateChange3Digit(String date1, int key) {
        String dateReturn = "";
        try {
            DateFormat dateFormat = null;
            if (key == 1) {
                dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            } else if (key == 2) {
                dateFormat = new SimpleDateFormat("MM/dd/yyyy");
            }
            @SuppressLint("SimpleDateFormat")
            DateFormat dateFormat1 = new SimpleDateFormat("dd MMM yyyy");
            assert dateFormat != null;
            Date date = dateFormat.parse(date1);
            assert date != null;
            dateReturn = dateFormat1.format(date);
        } catch (Throwable e) {
            e.printStackTrace();
            dateReturn = date1;
        }
        return dateReturn;
    }

    @Override
    public int getMonthNumberFromName(String monthName) {
        int monthNumber = 0;
        try {
            @SuppressLint("SimpleDateFormat")
            Date date = new SimpleDateFormat("MMM").parse(monthName);//put your month name here
            Calendar cal = Calendar.getInstance();
            assert date != null;
            cal.setTime(date);
            monthNumber = cal.get(Calendar.MONTH);
        } catch (Throwable e) {
            e.printStackTrace();
        }

        return monthNumber + 1;
    }

    @Override
    public double round(double value, int places) {
        try {
            if (places < 0) throw new IllegalArgumentException();
            long factor = (long) Math.pow(10, places);
            value = value * factor;
            long tmp = Math.round(value);
            return (double) tmp / factor;
        } catch (Throwable e) {
            e.printStackTrace();
            return value;
        }
    }

    @Override
    public List<ClubDetails> getClubDetails() {
        Gson gson = new Gson();
        String json = sharedPreferences.getString("ClubDetailsList", "");
        return gson.fromJson(String.valueOf(json), new TypeToken<List<ClubDetails>>() {
        }.getType());
    }

    @Override
    public void saveDashBoardView(ClubDetails clubDetails) {
        SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(clubDetails);
        prefsEditor.putString("Dashboard", json);
        prefsEditor.apply();
    }

    @Override
    public ClubDetails getDashBoardView() {
        Gson gson = new Gson();
        String json = sharedPreferences.getString("Dashboard", "");
        return gson.fromJson(json, ClubDetails.class);
    }

    @Override
    public void saveProfile(Profile profile) {
        SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(profile);
        prefsEditor.putString("Profile", json);
        prefsEditor.apply();
    }

    @Override
    public Profile getProfile() {
        Gson gson = new Gson();
        String json = sharedPreferences.getString("Profile", "");
        return gson.fromJson(json, Profile.class);
    }

    @Override
    public void loadImagePicasso(final ImageView imageView, String url, final Drawable drawable) {
        Picasso.get().load(url)
                .into(imageView, new Callback() {
                    @Override
                    public void onError(Exception e) {
                        imageView.setImageDrawable(drawable);
                    }

                    @Override
                    public void onSuccess() {
                    }
                });
    }

    @Override
    public Bitmap getBitmapFromB64(String base64) {
        byte[] decodedString = Base64.decode(base64, Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        return decodedByte;
    }


    @Override
    public void internet(final Context context, View v) {
        Snackbar snackbar = Snackbar
                .make(v, "Opps... Check Internet Connection", Snackbar.LENGTH_LONG)
                .setAction("Retry", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                    }
                });
        snackbar.setActionTextColor(Color.RED);
        View sbView = snackbar.getView();
        TextView textView = sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        snackbar.show();
    }
}

