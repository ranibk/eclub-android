package com.ircaindia.eclub.model.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Login implements Serializable {

    @SerializedName("Auth")
    @Expose
    private String auth;
    @SerializedName("Active")
    @Expose
    private String active;
    @SerializedName("Memberid")
    @Expose
    private String memberid;
    @SerializedName("MemberName")
    @Expose
    private String memberName;
    @SerializedName("AccountNumber")
    @Expose
    private String accountNumber;
    @SerializedName("LoginType")
    @Expose
    private String loginType;

    public String getAuth() {
        return auth;
    }

    public void setAuth(String auth) {
        this.auth = auth;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getMemberid() {
        return memberid;
    }

    public void setMemberid(String memberid) {
        this.memberid = memberid;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getLoginType() {
        return loginType;
    }

    public void setLoginType(String loginType) {
        this.loginType = loginType;
    }
}
