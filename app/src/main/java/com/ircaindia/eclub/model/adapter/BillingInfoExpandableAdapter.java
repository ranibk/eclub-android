package com.ircaindia.eclub.model.adapter;

import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import com.ircaindia.eclub.R;
import com.ircaindia.eclub.model.dto.BillingInfo;
import com.ircaindia.eclub.model.utils.IMessages;
import com.ircaindia.eclub.model.utils.Messages;

public class BillingInfoExpandableAdapter extends BaseExpandableListAdapter {

    private Context context;
    private List<String> dateList;
    private HashMap<String, List<BillingInfo>> billingList;
    private IMessages messages;

    public BillingInfoExpandableAdapter(Context context, List<String> listDataHeader, HashMap<String, List<BillingInfo>> listChildData) {
        this.context = context;
        this.dateList = listDataHeader;
        this.billingList = listChildData;
        messages = new Messages(context);
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return Objects.requireNonNull(this.billingList.get(this.dateList.get(groupPosition)))
                .get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @SuppressLint({"InflateParams", "SetTextI18n"})
    @Override
    public View getChildView(int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        try {
            if (convertView == null) {
                LayoutInflater infalInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                assert infalInflater != null;
                convertView = infalInflater.inflate(R.layout.layout_billing_info_item, null);
            }
            final BillingInfo billingInfo = (BillingInfo) getChild(groupPosition, childPosition);

            ImageView infoImageView = convertView.findViewById(R.id.image_viewInfo);
            TextView billNoTextView = convertView.findViewById(R.id.text_viewBillNo);
            TextView amountTextView = convertView.findViewById(R.id.text_viewAmount);

            billNoTextView.setText(billingInfo.getBillnumber());
            amountTextView.setText("₹" + billingInfo.getAmount());

            infoImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showBillingInfoDetails(billingInfo);
                }
            });

        } catch (Throwable e) {
            e.printStackTrace();
        }
        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return Objects.requireNonNull(this.billingList.get(this.dateList.get(groupPosition))).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this.dateList.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this.dateList.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        String headerTitle = (String) getGroup(groupPosition);
        try {

            if (convertView == null) {
                LayoutInflater infalInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                assert infalInflater != null;
                convertView = infalInflater.inflate(R.layout.layout_billing_date_item, null);
            }

            TextView dateTextView = convertView.findViewById(R.id.text_viewDate);
//		lblListHeader.setTypeface(null, Typeface.BOLD);
            dateTextView.setText(messages.getDateChangeBillingInfo(headerTitle));
            ExpandableListView mExpandableListView = (ExpandableListView) parent;
            mExpandableListView.expandGroup(groupPosition);

        } catch (Throwable e) {
            e.printStackTrace();
        }
        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    @SuppressLint("SetTextI18n")
    private void showBillingInfoDetails(BillingInfo billingInfo) {
        try {
            final Dialog dialog = new Dialog(context);
            dialog.getWindow();
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.dialog_billing_info_details);
            Objects.requireNonNull(dialog.getWindow()).setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

            TextView gTotalTextVew = dialog.findViewById(R.id.text_viewGTotal);
            TextView totalWithTaxTextVew = dialog.findViewById(R.id.text_viewTotalWithTax);
            TextView gTotalMainTextVew = dialog.findViewById(R.id.text_viewGTotalMain);
            RecyclerView itemsRecyclerView = dialog.findViewById(R.id.recycler_viewItems);
            TextView billNoTextView = dialog.findViewById(R.id.text_viewBillNo);
            TextView dateTextView = dialog.findViewById(R.id.text_viewDate);
            ImageView closeImageView = dialog.findViewById(R.id.image_viewClose);

            gTotalMainTextVew.setText("₹" +billingInfo.getAmount());
            gTotalTextVew.setText("₹" +billingInfo.getAmount());
            billNoTextView.setText(billingInfo.getBillnumber());
            dateTextView.setText(messages.getDateChange3Digit(billingInfo.getBilldate(),1));
            totalWithTaxTextVew.setText("₹ " + billingInfo.getBillAmount() + "\n₹ " + billingInfo.getTax());


            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
            itemsRecyclerView.setLayoutManager(linearLayoutManager);
            BillingInfoDetailsAdapter billingInfoDetailsAdapter = new BillingInfoDetailsAdapter(billingInfo.getBillingInfoList(), context);
            itemsRecyclerView.setAdapter(billingInfoDetailsAdapter);
            billingInfoDetailsAdapter.notifyDataSetChanged();
            dialog.show();
            closeImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });

        } catch (Throwable e) {
            e.printStackTrace();
        }
    }


}
