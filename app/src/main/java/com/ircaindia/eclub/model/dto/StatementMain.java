package com.ircaindia.eclub.model.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class StatementMain implements Serializable {


    @SerializedName("color")
    @Expose
    private int color;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("amount")
    @Expose
    private String amount;
    @SerializedName("list")
    @Expose
    private List<MonthlyStatement> monthlyStatementList=new ArrayList<>();

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public List<MonthlyStatement> getMonthlyStatementList() {
        return monthlyStatementList;
    }

    public void setMonthlyStatementList(List<MonthlyStatement> monthlyStatementList) {
        this.monthlyStatementList = monthlyStatementList;
    }
}
