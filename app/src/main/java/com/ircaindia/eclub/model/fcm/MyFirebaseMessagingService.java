package com.ircaindia.eclub.model.fcm;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.ircaindia.eclub.model.core.MyApplication;
import com.ircaindia.eclub.view.activity.SplashActivity;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;


public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";
    Bitmap bitmap;
    public NotificationUtils notificationUtils;
//    public static Runs runs;


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        try {

            Log.d(TAG, "From: " + remoteMessage.getFrom());
            notificationUtils = new NotificationUtils(getApplicationContext());

            // Check if message contains a data payload.

            // Check if message contains a notification payload.
            if (remoteMessage.getNotification() != null) {
                Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
            }
            if (remoteMessage.getData().size() > 0) {
                Log.d(TAG, "Message data payload: " + remoteMessage.getData());
            }
//            String title = remoteMessage.getData().get("title");
//            String message = remoteMessage.getData().get("body");
//            String image = remoteMessage.getData().get("image");
//            String runs = remoteMessage.getData().get("runs");
//            Log.d(Constants.TAG, "title" + title + "\nmessage" + message + "\nimage" + image);
//            sendNotification(message, title, image);
            if (notificationUtils.isAppIsInBackground(getApplicationContext())) {
                RemoteMessage.Notification notification = remoteMessage.getNotification();
                Map<String, String> data = remoteMessage.getData();
                sendNotification(notification, data);
                NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
                notificationUtils.playNotificationSound();
            } else {
                RemoteMessage.Notification notification = remoteMessage.getNotification();
                Map<String, String> data = remoteMessage.getData();
                sendNotification(notification, data);
                NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
                notificationUtils.playNotificationSound();
            }

        } catch (Throwable e) {
            e.printStackTrace();
        }

    }


    @Override
    public void onNewToken(String token) {
        Log.d(TAG, "Refreshed token: " + token);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        MyApplication.setFCMToken(token);
    }

    private void sendNotification(RemoteMessage.Notification notification, Map<String, String> data) {
//        Bitmap icon = BitmapFactory.decodeResource(getResources(), R.drawable.cms_icon);

        Intent intent = new Intent(this, SplashActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);

//        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, "channel_id")
//                .setContentTitle(notification.getTitle())
//                .setContentText(notification.getBody())
//                .setAutoCancel(true)
//                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
//                .setContentIntent(pendingIntent)
//                .setContentInfo(data.get("abc"))
//                .setLargeIcon(icon)
//                .setColor(Color.RED)
//                .setLights(Color.RED, 1000, 300)
//                .setSmallIcon(R.drawable.cms_icon);

        try {
            String picture_url = data.get("picture_url");
            if (picture_url != null && !"".equals(picture_url)) {
                URL url = new URL(picture_url);
                Bitmap bigPicture = BitmapFactory.decodeStream(url.openConnection().getInputStream());
//                notificationBuilder.setStyle(
//                        new NotificationCompat.BigPictureStyle().bigPicture(bigPicture).setSummaryText(notification.getBody())
//                );
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // Notification Channel is required for Android O and above
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(
                    "channel_id", "channel_name", NotificationManager.IMPORTANCE_DEFAULT
            );
            channel.setDescription("channel description");
            channel.setShowBadge(true);
            channel.canShowBadge();
            channel.enableLights(true);
            channel.setLightColor(Color.RED);
            channel.enableVibration(true);
            channel.setVibrationPattern(new long[]{100, 200, 300, 400, 500});
            notificationManager.createNotificationChannel(channel);
        }

//        notificationManager.notify(0, notificationBuilder.build());
    }

    // [END receive_message]
    public void playNotificationSound() {
        try {
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
            r.play();
        } catch (Exception e) {

            e.printStackTrace();
        }
    }


//    @Override
//    public void zzm(Intent intent) {
//
//        Log.i(Constants.TAG, intent.toString());
//        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
//
//        Log.d(Constants.TAG, "messageBody" + message);
//        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
//                PendingIntent.FLAG_ONE_SHOT);
//        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
//                .setLargeIcon(getBitmapfromUrl("https://lh3.googleusercontent.com/irO7oIGAlh0HCACoG7mUncRwx4jREOAZrDRB7j3yjrWULCZpndifDPdqAM5xzBaGEfak=w300-rw"))/*Notification icon image*/
//                .setSmallIcon(R.drawable.app_icon)
//                .setContentTitle("CriChamp Notification")
//                .setSubText(message)
//                .setContentText(message)
//                .setStyle(new NotificationCompat.BigPictureStyle()
//                        .bigPicture(getBitmapfromUrl("https://i.pinimg.com/564x/8a/d5/8a/8ad58a6c3180ec445a0495d80852d027.jpg"))/*Notification icon image*/
//                )/*Notification with Image*/
//                .setAutoCancel(true)
//                .setSound(defaultSoundUri)
//                .setContentIntent(pendingIntent);
//        NotificationManager notificationManager =
//                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
//
//        // play notification sound
//        NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
//        notificationUtils.playNotificationSound();
//    }

    /*
     *To get a Bitmap image from the URL received
     * */
    public Bitmap getBitmapFromUrl(String imageUrl) {
        try {
            URL url = new URL(imageUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap bitmap = BitmapFactory.decodeStream(input);
            return bitmap;
        } catch (Exception e) {

            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;

        }

    }
}


