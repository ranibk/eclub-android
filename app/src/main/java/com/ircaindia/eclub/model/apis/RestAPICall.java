package com.ircaindia.eclub.model.apis;

import com.ircaindia.eclub.model.dto.AffiliatedClubs;
import com.ircaindia.eclub.model.dto.BillingInfo;
import com.ircaindia.eclub.model.dto.ChangePassword;
import com.ircaindia.eclub.model.dto.ClubDetails;
import com.ircaindia.eclub.model.dto.Dependent;
import com.ircaindia.eclub.model.dto.Login;
import com.ircaindia.eclub.model.dto.MonthlyStatement;
import com.ircaindia.eclub.model.dto.Profile;
import com.ircaindia.eclub.model.dto.ReceiptsInfo;
import com.ircaindia.eclub.model.dto.SignUp;
import com.ircaindia.eclub.model.retrofit.response.ResponseModel;
import com.ircaindia.eclub.model.retrofit.response.ResponseModelArray;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface RestAPICall {

    String url = "club.ashx";  // live link
//        String url = "mobile_new_ars/handler1.ashx";  // test link

    @POST("{url}")
    Call<ResponseModelArray<ClubDetails>> getClubDetails(@Path(value = "url", encoded = true) String url, @Body PostData postData);

    @POST("{url}")
    Call<ResponseModelArray<Login>> loginWithAccNumber(@Path(value = "url", encoded = true) String url, @Body PostData postData);

    @POST("{url}")
    Call<ResponseModelArray<Profile>> getProfileDetails(@Path(value = "url", encoded = true) String url, @Body PostData postData);

    @POST("{url}")
    Call<ResponseModelArray<BillingInfo>> getBillingInfo(@Path(value = "url", encoded = true) String url, @Body PostData postData);

    @POST("{url}")
    Call<ResponseModelArray<ReceiptsInfo>> getReceiptInfo(@Path(value = "url", encoded = true) String url, @Body PostData postData);

    @POST("{url}")
    Call<ResponseModelArray<AffiliatedClubs>> getAffiliatedClubs(@Path(value = "url", encoded = true) String url, @Body PostData postData);

    @POST("{url}")
    Call<ResponseModelArray<Dependent>> getDependents(@Path(value = "url", encoded = true) String url, @Body PostData postData);

    @POST("{url}")
    Call<ResponseModelArray<MonthlyStatement>> getMonthlyStatement(@Path(value = "url", encoded = true) String url, @Body PostData postData);

    @POST("{url}")
    Call<ResponseModel> getOutstanding(@Path(value = "url", encoded = true) String url, @Body PostData postData);

    @POST("{url}")
    Call<ResponseModelArray<ChangePassword>> changePassword(@Path(value = "url", encoded = true) String url, @Body PostData postData);

    @POST("{url}")
    Call<ResponseModelArray<SignUp>> getSignUpOTP(@Path(value = "url", encoded = true) String url, @Body PostData postData);

    @POST(url)
    Call<ResponseModelArray<ClubDetails>> loginWithMobileNumber(@Body PostData postData);

}
