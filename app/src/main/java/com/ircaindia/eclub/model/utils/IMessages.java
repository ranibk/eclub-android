package com.ircaindia.eclub.model.utils;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.text.SpannableString;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.ircaindia.eclub.model.apis.RestAPICall;
import com.ircaindia.eclub.model.dto.ClubDetails;
import com.ircaindia.eclub.model.dto.Login;
import com.ircaindia.eclub.model.dto.Profile;
import com.ircaindia.eclub.model.dto.RememberMe;

import java.util.List;


public interface IMessages {
    void showProgressDialog(Context context, String Title, String message);

    void hideDialog();

    String getDeviceId(Activity activity);

    String getDeviceModel(Activity activity);

    void showAlertDialog(Context context, String message);

    void showAlert(Context context);

    void showAlertPassword(Context context);

    void internet(Context context, View view);

    void toast(Context context, String message);

    void toastSnackBar(Context context, String message, View view);

    void toastInternet(Context context, View view);

    boolean isValidMail(String email);

    boolean isValidNumber(String number);

    void saveCurrentLogin(Activity activity, Login login);

    Login getCurrentLogin(Activity activity);

    void rememberMe(Activity activity, RememberMe rememberMe);

    RememberMe getRememberMe(Activity activity);

    void logOut(Activity activity);

    String getDeviceOS();

    String getLastStringURL(String url);

    String getFirstStringURL(String url, String lastOne);

    void hideKeyBoard(Activity activity, View view);

    int lastDayOfMonth(int month, int year);

    void dateForHistory(Activity activity, final EditText view, String title);

    RestAPICall getAPIService(String url);

    List<ClubDetails> getClubDetails();

    void saveDashBoardView(ClubDetails clubDetails);

    ClubDetails getDashBoardView();

    void saveProfile(Profile profile);

    Profile getProfile();

    void loadImagePicasso(ImageView imageView, String url, Drawable drawable);

    void saveClubDetails(List<ClubDetails> clubDetails);

    Bitmap getBitmapFromB64(String base64);

    boolean isInternetAvailable();

    void shareOption();

    SpannableString getToolbarTitle(String title);

    SpannableString getCustomFontString(String text, String fontName);

    String getCurrentMonthYear();

    int getMonthNumberFromName(String monthName);

    String getDateChangeBillingInfo(String date1);

    double round(double value, int places);

    String getDateChange3Digit(String date1, int key);

    String getStringRupee(String string);

    boolean isFutureDate(String current, String selected);

}
